import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

import quicksort from '../resources/quicksort.png';

const trirapide =
`triRapide(tab)
    n = taille de tab
    triRapideAuxiliere(tab, 0, n-1)`;

const pivot =
`choisirPivot(tab, debut, fin)
    pivot = tab[debut]
    pour i = debut + 1 jusqu'à fin
        if tab[i] > pivot
            return i
        else if tab[i] < pivot
            return debut

    return -1`;

const partition =
`partitionner(tab, debut, fin, pivot)
    gauche = debut
    droite = fin
    tant que gauche < droite
        tant que tab[gauche] < pivot
            gauche++
            
        tant que tab[droite] ≥ pivot
            droite--
            
        if gauche < droite
            échanger tab[gauche] et tab[droite]
        
    return gauche`;

const auxiliere =
`triRapideAuxiliere(tab, debut, fin)
    indexPivot = choisirPivot(tab, debut, fin)
    
    if(indexPivot ≥ 0)
        i = partitionner(tab, debut, fin, tab[indexPivot])
        triRapideAuxiliere(tab, debut, i-1)
        triRapideAuxiliere(tab, i, fin)`;

export default function TriRapide() {
    return <>
        <section>
            <h2>Fonctionnement</h2>
            <p>
                Le tri rapide, appellé <IC>quicksort</IC> en anglais, est l'un des tri les plus efficace dans la
                plupart des cas. C'est un algorithme récursif qui s'exécute en <IC>O(n log n)</IC>. Il est très
                efficace pour trier les grands tableaux, mais un peu moins pour les tableaux de petite taille. Le tri
                rapide est l'algorithme de tri le plus couramment exploité dans l'industrie.
            </p>
            <p>
                L'algorithme utilise une stratégie <IC>diviser pour régner</IC>. Il regroupe les éléments en deux
                sous-tableau, puis on traite récursivement ces deux sous tableaux. Les invocations récursives cessent
                lorsque tous les éléments du tableau (ou sous-tableau) à trier sont identiques. À la base,
                l'algorithme fonctionne de la façon suivante:
            </p>
            <ol>
                <li>Choisir une valeur du tableau, appelée <IC>pivot</IC>.</li>
                <li>Déplacer tous les éléments inférieurs au pivot au début du tableau.</li>
                <li>
                    Diviser le tableau en deux sous tableaux.
                    <ol>
                        <li>
                            Le premier sous tableau est la partie inférieure du tableau contenant les éléments
                            inférieurs au pivot.
                        </li>
                        <li>
                            Le deuxième sous tableau est la partie supérieure du tableau contenant les éléments
                            supérieurs ou égals au pivot.
                        </li>
                    </ol>
                </li>
                <li>Trier les deux sous tableaux par tri rapide.</li>
            </ol>
            <p>
                Nous séparerons donc l'algorithme en 3 étapes:
            </p>
            <ol>
                <li>Choisir un pivot.</li>
                <li>Partitionner le tableau en deux.</li>
                <li>Trier les deux partitions.</li>
            </ol>
        </section>

        <section>
            <h2>Exemple graphique</h2>
            <p>
                Voici un exemple graphique de l'invoquation du tri rapide sur un tableau en suivant le fonctionnement
                ci-dessus.
            </p>
            <img src={quicksort} alt="Exemple graphique de l'exécution du tri rapide sur un tableau" />
            <ColoredBox heading="À noter">
                Vous noterez que le choix du pivot est toujours la plus grande des deux premières valeurs distinctes
                du tableau
            </ColoredBox>
        </section>

        <section>
            <h2>Récursion avec index</h2>
            <p>
                Pour ne pas avoir à recopier de tableaux, l'algorithme du tri rapide utilise des index représentant le
                début et la fin du tableau ou sous-tableau qu'il traite.
            </p>
            <p>
                Toutefois, pour un utilisateur, il n'est pas très intéressant de spécifier ces index à la première
                invocation du tri rapide puisque l'indice de début sera toujours 0 et l'indice de fin toujours la
                taille tu tableau moins 1. C'est pourquoi nous utiliserons une fonction de tri rapide auxilière pour
                trier notre tableau.
            </p>
            <CodeBlock language="pseudo">{trirapide}</CodeBlock>
            <p>
                Ainsi, notre fonction <IC>triRapide</IC> cache l'implémentation récursive et les paramètres de début
                et de fin de la zone du tableau à traiter. Nous verrons l'implémentation de la
                fonction <IC>triRapideAuxiliere</IC> un peu plus loin.
            </p>
        </section>

        <section>
            <h2>Choix du pivot</h2>
            <p>
                Le choix du pivot est toujours la plus grande des deux premières valeurs distinctes du tableau ou du
                sous-tableau. Le choix du pivot fait en sorte que, si le tableau contient au moins deux valeurs
                distinctes, deux sous tableaux résulteront du partitionnement.
            </p>
            <p>
                Pour facilité la compréhension de l'algorithme, nous programmerons une fonction qui choisit le pivot.
                Cette fonction retournera l'indice du pivot, ou -1 si toutes les valeurs du tableau sont identiques.
            </p>
            <CodeBlock language="pseudo">{pivot}</CodeBlock>
            <div>
                <p>Voici quelques exemples de sélection du pivot:</p>
                <div style={{ display: 'flex', columnGap: '2rem', flexWrap: 'wrap', whiteSpace: 'nowrap' }}>
                    <div style={{marginBottom: '1rem'}}>
                        <div>[ 2, 2, 2, <IC>5</IC>, 4, 1, 5 ]</div>
                        <div>Retourne l'indice 3</div>
                    </div>
                    <div style={{marginBottom: '1rem'}}>
                        <div>[ <IC>2</IC>, 2, 2, 1, 4, 1, 5 ]</div>
                        <div>Retourne l'indice 0</div>
                    </div>
                    <div style={{marginBottom: '1rem'}}>
                        <div>[ 2, 2, 2, 2, 2, 2, 2 ]</div>
                        <div>Retourne l'indice -1</div>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <h2>Partitionner le tableau en deux</h2>
            <p>
                Le but du partitionnement est de réarranger les éléments de telle sorte que ceux de valeurs
                inférieures au pivot occupent le début du tableau. Le principe est assez simple. On échange la
                position des valeurs plus grande que le pivot au début du tableau avec les valeurs plus petites
                du que le pivot à la fin du tableau.
            </p>
            <p>
                Comme pour le choix du pivot, pour facilité la compréhension de l'algorithme, nous programmerons une
                fonction qui fait le partitionnement. Cette fonction modifiera le tableau original pour mettre les
                valeurs inférieures au pivot au début du tableau. Elle retournera aussi le nouvel indice du pivot dans
                le tableau après ces manipulations.
            </p>
            <CodeBlock language="pseudo">{partition}</CodeBlock>
            <ColoredBox heading="À noter">
                La fonction <IC>choisirPivot</IC> fait en sorte qu'il y a au moins deux valeurs différentes dans la
                partition, dont une est inférieure au pivot et l'autre est supérieure ou égale au pivot.
            </ColoredBox>
        </section>

        <section>
            <h2>Fonction auxilière</h2>
            <p>
                Une fois l'algorithme de choix du pivot et de partitionnement faite, il ne nous reste qu'à mettre le
                code ensemble dans la fonction auxilière du tri rapide. Cette fonction va choisir le pivot,
                partitionner en fonction du pivot et relancer le tri rapide sur les sous-tableaux.
            </p>
            <CodeBlock language="pseudo">{auxiliere}</CodeBlock>
            <p>
                En mettant les 4 fonctions ci-dessus ensemble, vous serez en mesure d'exécuter la
                fonction <IC>triRapide</IC> pour trier vos tableaux de façon efficace.
            </p>
        </section>
    </>;
}
