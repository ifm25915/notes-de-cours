import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

import tableaudentele from '../resources/tableaudentele.png';

const tableauDentele = 
`// Avec initialiseur si on connais déjà les données
int[][] tabDentele = new int[][] {
    new int[] {  1 },
    new int[] {  2,  3 },
    new int[] {  4,  5,  6 },
    new int[] {  7,  8,  9, 10 },
    new int[] { 11, 12, 13, 14, 15 },
};

// Sans inititialiseur si on ajoute les données plus tard
int[][] tabDentele2 = new int[3][];
tabDentele2[0] = new int[4];
tabDentele2[1] = new int[6];
tabDentele2[2] = new int[2];

tabDentele[1][4] = 18;`;

const parcourir =
`for (int i = 0; i < tabDentele.Length; i++) {
    for (int j = 0; j < tabDentele[i].Length; j++){
        Console.Write(tabDentele[i][j] + ",");
    }

    Console.WriteLine();
}`;

const parcourirNull =
`int[][] tabDentele = new int[3][];
tabDentele[0] = new int[4];
tabDentele[1] = null;
tabDentele[2] = new int[2];

for (int i = 0; i < tabDentele.Length; i++) {
    // On ignore les tableau "null"
    if(tabDentele[i] != null){
        for (int j = 0; j < tabDentele[i].Length; j++){
            Console.Write(tabDentele[i][j] + ",");
        }

        Console.WriteLine();
    }
}`;


export default function FTTableauDentele() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                Le tableau dentelé existe par le fait qu'on peut généralement mettre un tableau dans un
                autre tableau, mais que ces tableaux n'ont pas besoin d'avoir la même taille. Puisqu'il fonctionne
                avec des tableaux, le tableau dentelé partage donc leurs points forts et leurs faiblesses. Comme le
                tableau multidimensionnel, ce type de tableau peut avoir plus de 2 dimensions.
            </p>
            <p>
                On représentera graphiquement un dentelé à 2 dimensions de la façon suivante:
            </p>
            <img src={ tableaudentele } alt="Représentation d'un tableau" />
            <p>
                En C#, on créera un tableau dentelé de la façon suivante:
            </p>
            <CodeBlock language="csharp">{ tableauDentele }</CodeBlock>
        </section>

        <section>
            <h2>Avantages</h2>
            <ul>
                <li>Simple à utiliser et ne nécessite généralement pas d'importation de librairie</li>
                <li>Très performant pour l'accès aléatoire</li>
            </ul>
        </section>

        <section>
            <h2>Inconvénients</h2>
            <ul>
                <li>La taille est fixe, on ne peut donc pas ajouté ou enlever de l'espace dedans</li>
                <li>Pour faire des recherches, on doit généralement parcourir toutes les données</li>
                <li>
                    Il peut être difficile de créer des tableaux de taille gigantesque puisque les éléments du tableau
                    sont placé un à côté de l'autre dans la mémoire et qu'il que l'ordinateur ne pourra pas
                    nécessairement trouver ce genre d'espace mémoire facilement
                </li>
            </ul>
        </section>

        <section>
            <h2>Opérations</h2>
            <dl>
                <dt>Parcourir le tableau dentelé</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(n)</IC>
                    </p>
                    <p>
                        Les opérations et leur complexité algorithmique sont les même sur un tableau dentelé
                        que sur un tableau régulier, comme le tableau multidimensionnel. La façon dont on va parcourir
                        le tableau dentelé est toutefois un peu différente. En effet, il sera généralement nécessaire
                        d'avoir une boucle pour chaque dimension du tableau dentelé.
                    </p>
                    <CodeBlock language="csharp">{ parcourir }</CodeBlock>
                    <p>
                        Vous noterez ici dans la boucle intérieur que l'on doit bien vérifier la taille de chaque
                        tableau avec <IC>tabDentele[i].Length</IC>. C'est nécessaire puisque les tableaux internes
                        n'ont pas tous la même taille.
                    </p>
                    <ColoredBox heading="Attention">
                        <p>
                            En plus d'avoir des tableaux de tailles différente, un tableau dentelé pourrait contenir des
                            tableaux <IC>null</IC>. Si c'est une possibilité dans votre code, il vous faudra ajouter des
                            conditions supplémentaires.
                        </p>
                        <CodeBlock language="csharp">{ parcourirNull }</CodeBlock>
                    </ColoredBox>
                </dd>
            </dl>
        </section>
    </>
}
