import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import DownloadBlock from '../component/DownloadBlock';

import distribue from '../resources/laboratoire-blackjack-distribué.zip';
import solution from '../resources/laboratoire-blackjack-solution.zip';

const blackjack =
`> dotnet run
0: [ 4♦, K♠ ] : 14
1: [ 4♦, K♠, 2♣ ] : 16    
2: [ 4♦, K♠, 2♣, 2♥ ] : 18
Vous gagnez avec 18 points

> dotnet run
0: [ 6♠, 6♥ ] : 12
1: [ 6♠, 6♥, Q♦ ] : 22
Vous perdez avec 22 points

> dotnet run
0: [ Q♦, A♦ ] : 21
Vous gagnez avec un Blackjack!`;

export default function LabBlackJack() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                On fait une version automatique et très simplifié du jeu de Blackjack. Dans ce jeu, le but est
                d'atteindre un pointage le plus prêt possible de 21, sans toutefois le dépasser. Le jeu se joue avec
                un paquet de carte à jouer chaque carte a une valeur de points. Les cartes de 2 à 10 ont leur valeur
                respective de points, le valet, la dame et le roi valent 10 points et l'as vaut 11 points s'il ne fait
                pas dépasser le pointage au dessus de 21, autrement il vaut juste 1 point.
            </p>
            <p>
                Dans notre version simplifié du jeu, on donnera des cartes automatiquement au joueur tant que son
                pointage est inférieur à 17. Si le joueur atteint un pointage entre 17 et 21 inclusivement, il gagne.
                Si son pointage dépasse le 21, il perd.
            </p>
            <p>
                Compléter le projet distribué ci-dessous:
            </p>
            <ol>
                <li>Télécharger le fichier <IC>distribué.zip</IC>.</li>
                <li>Dézipper le fichier <IC>distribué.zip</IC>.</li>
                <li>
                    Écrire le code de la fonction <IC>CalculerMain</IC> dans le fichier <IC>Blackjack.cs</IC>.
                    <ul>
                        <li>
                            La fonction doit calculer le pointage total de la main en paramètre avec les valeurs de
                            points mentionnées ci-dessus.
                        </li>
                        <li>
                            Le truc pour gérer l'as, c'est de toujours le calculer comme une valeur de 1. Après le
                            calcul de toute les cartes, si vous aviez un as, regarder si vous pouvez ajouter 10 points
                            sans dépasser un pointage de 21. Si oui, faites-le. Sinon, ne faites rien puisque vous
                            avez déjà calculé la valeur de l'as comme un 1.
                        </li>
                        <li>
                            Retourner le pointage calculé.
                        </li>
                    </ul>
                </li>
                <li>
                    Compléter le constructeur <IC>Blackjack</IC> dans le fichier <IC>Blackjack.cs</IC>
                    <ul>
                        <li>
                            La première chose à faire est d'initialiser la main du joueur et de mettre la première
                            carte du paquet dans la main.
                        </li>
                        <li>
                            Tant que la valeur de la main est inférieur à 17 points, transférer la carte sur le dessus
                            du paquet dans la main du joueur et affichez la main et le pointage de ceux-ci.
                        </li>
                        <li>
                            Si la valeur de la main du joueur dépasse 17 points, affichez si l'utilisateur gagne ou
                            perd, bref, si son total de points dépasse 21. Si l'utilisateur gagne avec 21 points et
                            seulement 2 cartes, donc une carte à 10 points et un as, affichez que c'est un Blackjack!
                        </li>
                        <li>
                            Pour afficher la main du joueur, utiliser la méthode <IC>AfficherMain</IC>. Pour calculer
                            la valeur de la main du joueur, utiliser la fonction <IC>CalculerMain</IC> que vous avez
                            écrite.
                        </li>
                        <li>
                            Quelques exécutions du programme pourrait ressembler à ceci:
                            <CodeBlock language="shell">{ blackjack }</CodeBlock>
                        </li>
                    </ul>
                </li>
            </ol>
        </section>

        <section>
            <h2>Distribué et solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ distribue } name="distribué.zip"></DownloadBlock.File>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
