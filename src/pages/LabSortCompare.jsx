import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import DownloadBlock from '../component/DownloadBlock';

import distribue from '../resources/laboratoire-triinefficace-distribué.zip';
import solution from '../resources/laboratoire-triinefficace-solution.zip';

const selectionsort =
`triSelection(tab)
    n = taille de tab
    pour i = 0 jusqu'à n-2
        min = i
        pour j = i+1 jusqu'à n-1
            if tab[j] < tab[min]
                min = j;
            
        if min != i
            echanger tab[i] et tab[min]`;

export default function LabSortCompare() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                Dans ce laboratoire, vous devrez programmer en C# l'algorithme du tri par sélection en C# à partir de
                pseudo-code. Vous devrez ensuite le comparer au tri faire-valoir et au tri à bulles en comptant le
                nombre de comparaisons et d'inversions faites par l'algorithme.
            </p>
            <p>
                Compléter le projet distribué ci-dessous:
            </p>
            <ol>
                <li>Télécharger le fichier <IC>distribué.zip</IC>.</li>
                <li>Dézipper le fichier <IC>distribué.zip</IC>.</li>
                <li>
                    Dans le fichier <IC>Tris.cs</IC>, programmer la fonction <IC>TriSelection</IC> à l'aide du
                    pseudo-code ci-dessous:
                    <CodeBlock language="pseudo">{selectionsort}</CodeBlock>
                    Vous pouvez utiliser la fonction <IC>echanger</IC> pour permuter 2 valeurs, comme pour les 2
                    autres tris dans ce fichier.
                </li>
                <li>
                    Dans le code de votre implémentation du tri sélection, à chaque condition et à chaque permutation,
                    incrémenter la variable <IC>nbTriSelection</IC>.
                </li>
                <li>
                    Regarder les résultats dans la console. Lequel de ces algorithmes performe le moins d'opération?
                </li>
            </ol>
        </section>

        <section>
            <h2>Distribué et solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ distribue } name="distribué.zip"></DownloadBlock.File>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}