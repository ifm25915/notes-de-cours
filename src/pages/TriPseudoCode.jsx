import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const pseudoVar =
`x = 100

y <- 500

tab = []

chaine = "allo"`;

const pseudoIf =
`if x est plus grand que 100
    ...
endif

SI valeur est pair
    ...

IF y = -1
    ...
ELSE IF y = 0
    ...
ELSE
    ...`;

const pseudoLoop =
`Pour chaque element dans tab
    ...
FinPour

Boucler de 0 jusqu'à n
    ...
    
FOR elem in tab
    ...
ROF`;

const pseudoFunc =
`function test(x, y)
    ...
endfunction
test(10, 30)

procedure nomDeProcedure
    ...
    
nomDeProcedure()

SUB viderTruc (truc)
    ...
BUS
tab <- [1, 4, 9]
viderTruc(tab)

F(z) =
    ...

F(48)`;

const pseudoMath =
`Si x ≠ -1 et y ≥ 0
    y <- ⌊y⌋
Sinon
    y <- min(3, 4)
    echanger x et y`;

export default function TriPseudoCode() {
    return <>
        <section>
            <h2>Langage basé sur la compréhension</h2>
            <p>
                Le pseudo-code est une façon d'écrire les algorithmes sans utiliser un langage de programmation en
                particulier. Il est basé sur la compréhension entre les programmeurs et mathématiciens. Il n'a donc
                pas vraiment de règles et chaque personne peut écrire son propre pseudo-code un peu différent, tant
                que les lecteurs du pseudo-code puissent le comprendre.
            </p>
            <p>
                C'est un outil que l'on utilisera beaucoup pour démontrer comment programmer un algorithme sans
                utiliser un langage de programmation spécifique.
            </p>
        </section>

        <section>
            <h2>Opération</h2>
            <p>
                Le pseudo-code va généralement avoir les mêmes opérations qu'un langage de programmation. Voici les
                plus génériques:
            </p>
            <dl>
                <dt>Variable</dt>
                <dd>
                    La variable est le bloc de base de tous les langages de programmation puisque c'est une façon de
                    manipuler des données. Dans les langages de programmation, on crée généralement une variable en
                    utilisant un mot-clé comme <IC>var</IC> ou <IC>let</IC> ou encore en spécifiant son type,
                    comme <IC>string</IC> ou <IC>int</IC>. En pseudo-code, vous pourriez la représenter de la façon
                    suivante:
                    <CodeBlock language="pseudo">{pseudoVar}</CodeBlock>
                </dd>
                <dt>Condition</dt>
                <dd>
                    La condition est le changement du flux d'exécution d'un algorithme en fonction d'une condition. On
                    l'appèle généralement <IC>if/else</IC> dans les langages de programmation. En pseudo-code, vous
                    pourriez le représenter de la façon suivante:
                    <CodeBlock language="pseudo">{pseudoIf}</CodeBlock>
                </dd>
                <dt>Boucle</dt>
                <dd>
                    La boucle est la répétition d'une partie d'un algorithme en fonction d'une condition. On
                    l'appèle généralement <IC>for</IC> ou <IC>while</IC> dans les langages de programmation. En
                    pseudo-code, vous pourriez le représenter de la façon suivante:
                    <CodeBlock language="pseudo">{pseudoLoop}</CodeBlock>
                </dd>
                <dt>Fonction</dt>
                <dd>
                    La fonction est une déclaration de routine pouvant être utilisé à plusieurs endroit dans le code.
                    On l'appèle généralement fonction, méthode ou sous-routine dans les langages de programmation. On
                    utilise aussi les parenthèses pour montrer ses paramètres et son exécution. En pseudo-code, vous
                    pourriez la représenter de la façon suivante:
                    <CodeBlock language="pseudo">{pseudoFunc}</CodeBlock>
                </dd>
                <dt>Autre</dt>
                <dd>
                    Le pseudo-code est souvent basé sur le langage mathématique. Il est donc fréquent d'y voir des
                    symboles spéciaux reliés aux mathématiques dedans. Voici quelques exemples:
                    <CodeBlock language="pseudo">{pseudoMath}</CodeBlock>
                </dd>
            </dl>
        </section>

        <section>
            <h2>Pseudo-code dans le cours</h2>
            <p>
                Dans ce cours, nous utiliserons le pseudo-code pour montrer comment programmer des algorithmes. Vous
                devrez toutefois programmer ces algorithmes dans un langage de programmation pour le tester. Dans le
                cadre du cours, vous devrez généralement écrire le code de vos algorithmes en C#.
            </p>
        </section>
    </>;
}