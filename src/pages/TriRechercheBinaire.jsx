import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

import binarysearch1 from '../resources/binarysearch1.png';
import binarysearch2 from '../resources/binarysearch2.png';

const recherche =
`rechercheDichotomique(tab, valeur)
    n = taille de tab
    debut = 0
    fin = n-1

    tant que debut <= fin
        milieu = (debut + fin) / 2
        if valeur < tab[milieu]
            fin = milieu - 1
        else if valeur > tab[milieu]
            debut = milieu + 1
        else
            return milieu
        
    return -1`;

const rechercheRecursive =
`rechercheDichotomique(tab, valeur)
    n = taille de tab

    return rechercheDichotomiqueRecursive(tab, valeur, 0, n-1)

rechercheDichotomiqueRecursive(tab, valeur, debut, fin)
    milieu = (debut + fin) / 2

    if debut > fin
        return -1
    else if valeur < tab[milieu]
        return rechercheDichotomiqueRecursive(tab, valeur, debut, milieu - 1)
    else if valeur > tab[milieu]
        return rechercheDichotomiqueRecursive(tab, valeur, milieu + 1, fin)
    else
        return milieu`;

export default function TriRechercheBinaire() {
    return <>
        <section>
            <h2>Utilité et fonctionnement</h2>
            <p>
                La recherche dichotomique, aussi appelé recherche binaire, est un algorithme de recherche dans un
                tableau de données. Normalement, la recherche d'un élément dans un tableau de données se fait
                en <IC>O(n)</IC> puisque nous devons passer sur chacun des éléments du tableau. C'est ce qu'on
                appelle la recherche séquentielle. Toutefois, si le tableau est trié, nous pouvons plutôt utilisé
                l'algorithme de la recherche dichotomique pour trouver l'élément en <IC>O(log n)</IC>
            </p>
            <p>
                Le problème, c'est que le tableau doit être trié. Vous devez donc utiliser un algorithme de tri
                préalablement pour trié le tableau. Les algorithmes de tri sont généralement trié
                en <IC>O(n<sup>2</sup>)</IC> ou <IC>O(n log n)</IC>, ce qui est définitivement moins bon que la
                recherche séquentielle. Toutefois, si nous avons plusieurs recherches à faire dans le même tableau,
                il peut être utile de préalablement trié le tableau pour ensuite utiliser une recherche plus efficace.
            </p>
            <p>
                Si vous avez un tableau de 1&nbsp;000&nbsp;000 éléments, la recherche séquentielle peut prendre
                jusqu'à 1&nbsp;000&nbsp;000 comparaisons pour trouver l'élément. Dans le cas de la recherche
                dichotomique, le maximum de comparaisons est 20.
            </p>
        </section>

        <section>
            <h2>Pseudo-code</h2>
            <p>
                La recherche dichotomique fonctionne un peu comme une recherche dans un dictionnaire papier. Si on
                recherche le mot <IC>éléphant</IC>, on commencera par ouvrir le dictionnaire au milieu. On se
                demandera ensuite dans quelle moité du dictionnaire se retrouve le mot <IC>éléphant</IC>. Dans ce
                cas-ci, définitivement dans la première moitié. On recommence ensuite l'algorithme, mais avec la
                première moité du dictionnaire, qu'on séparera en 2. On se posera ensuite encore la question si le mot
                est dans la première ou 2e moitié. On continuera ainsi jusqu'à trouver le mot.
            </p>
            <p>
                Voici le pseudo-code de l'algorithme:
            </p>
            <CodeBlock language="pseudo">{recherche}</CodeBlock>
            <p>
                L'algorithme peut aussi être écrit de façon récursive. Voici so pseudo-code:
            </p>
            <CodeBlock language="pseudo">{rechercheRecursive}</CodeBlock>
        </section>

        <section>
            <h2>Exemple d'exécution</h2>
            <p>
                Si nous recherchons la valeur <IC>4</IC> dans le tableau ci-dessous, nous aurons l'exécution
                suivante:
            </p>
            <img src={binarysearch1} alt="Exemple de recherche dichotomique dans un tableau" />
            <p>
                Si nous recherchons plutôt la valeur <IC>15</IC>, nous aurons l'exécution suivante:
            </p>
            <img src={binarysearch2} alt="Exemple de recherche dichotomique dans un tableau" />
        </section>
    </>;
}
