import React from 'react';
import IC from '../component/InlineCode';
import DownloadBlock from '../component/DownloadBlock';

import solution from '../resources/laboratoire-recursivite-solution.zip';

export default function LabArticle() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                Créer un projet console vide nommé <IC>ExerciceRecursivite</IC> et résoudre les problèmes ci-dessous:
            </p>
            <ol>
                <li>
                    Créer 2 fonctions qui permettent de calculer le factoriel d'un entier positif. Une des fonctions
                    doit être itérative et l'autre doit être récursive.
                </li>
                <li>
                    Créer 2 fonctions qui permettent de calculer de calculer la somme d'une suite de nombre d'entiers
                    positifs. Une des fonctions doit être itérative et l'autre doit être récursive.
                    <div>Exemple: somme = 1 + 2 + 3 + 4 + ... + n</div>
                </li>
                <li>
                    Créer 2 fonctions qui permettent de calculer de calculer la somme d'une suite de nombre d'entiers
                    positifs pairs. Une des fonctions doit être itérative et l'autre doit être récursive.
                    <div>Exemple: somme = 0 + 2 + 4 + 6 + ... + n</div>
                </li>
                <li>
                    Créer 2 fonctions qui permettent de calculer de calculer la somme d'une suite de nombre d'entiers
                    positifs impairs. Une des fonctions doit être itérative et l'autre doit être récursive.
                    <div>Exemple: somme = 1 + 3 + 5 + 7 + ... + n</div>
                </li>
                <li>
                    Créer une fonction récursive permettant de calculer la somme des chiffres d'un
                    entier <IC>n</IC> positif.
                    <div>Exemple: n = 765; Somme = 7 + 6 + 5 = 18</div>
                </li>
                <li>
                    Créer une fonction récursive nommée <IC>Chiffre(n, k)</IC> permettant de retourner le k-ième
                    chiffre à la base zéro d'un entier <IC>n</IC> positif à partir de la droite.
                    <div>Exemple: n = 41765; k = 2; Chiffre = 7</div>
                    <div>Exemple: n = 41765; k = 0; Chiffre = 5</div>
                </li>
                <li>
                    Créer 2 fonctions qui permettent de calculer de calculer la somme d'un tableau d'entiers positifs.
                    Une des fonctions doit être itérative et l'autre doit être récursive.
                </li>
                <li>
                    Créer 2 fonctions qui permettent d'afficher le nombre d'occurrences d'un nombre <IC>n</IC> dans un
                    tableau d'entiers positifs. Une des fonctions doit être itérative et l'autre doit être récursive.
                </li>
            </ol>
        </section>

        <section>
            <h2>Distribué et solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
