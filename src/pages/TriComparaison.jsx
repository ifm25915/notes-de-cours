import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const comparaison =
`private static int NomDeFonctionDeComparaison(Type element1, Type element2)
{
    // ...
}`;

const sort =
`// Pour trier un tableau
Array.Sort(tableau, NomDeFonctionDeComparaison);

// Pour trier un vecteur
vecteur.Sort(NomDeFonctionDeComparaison)`;

const reverse =
`private static int Inverse(int element1, int element2)
{
    if(element1 > element2)
    {
        return -1;
    }
    else if(element1 < element2)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}`;

const reverseSimple =
`private static int Inverse(int element1, int element2)
{
    // Si on fait element1.compareTo(element2), ça
    // retourne l'ordre normal. Nous, on inverse les
    // paramètres.
    return element2.compareTo(element1);
}`;

const reverseString =
`private static int Inverse(string element1, string element2)
{
    return element2.compareTo(element1);
}`;

const oddeven =
`private static int PairPlusGrandQueImpair(int element1, int element2)
{
    if(element1 % 2 == 0 && element2 % 2 == 1)
    {
        return 1;
    }
    else if(element1 % 2 == 1 && element2 % 2 == 0)
    {
        return -1;
    }
    else
    {
        return element1.compareTo(element2);
    }
}`;

export default function TriComparaison() {
    return <>
        <section>
            <h2>Fonction de comparaison</h2>
            <p>
                Parfois, l'ordre naturel de triage des éléments ne nous satisfait pas. En effet, il peut parfois être
                voulu de trier selon des règles différentes ou tout simplement à l'envers. En C#, les fonctions de
                triage nous permettent de fournir une fonction de comparaison pour changer la façon dont le triage va
                comparer les éléments. Les fonctions de triage auront le format suivant:
            </p>
            <CodeBlock language="csharp">{comparaison}</CodeBlock>
            <p>
                La fonction de comparaison prends en paramètre 2 éléments à comparer. Ces 2 éléments doivent être du
                même type que ce que vous voulez trier dans votre tableau. Vous devez donc changer le type de ces 2
                paramètres dans la fonction.
            </p>
            <p>
                Cette fonction doit retourner un entier ayant la valeur -1, 0 ou 1 indiquant la comparaison de nos 2
                paramètres:
            </p>
            <ul>
                <li>-1 Si l'élément 1 est plus petit que l'élément 2.</li>
                <li>0 Si l'élément 1 est égal à l'élément 2.</li>
                <li>1 Si l'élément 1 est plus grand que l'élément 2.</li>
            </ul>
            <p>
                Une fois la fonction créé, vous n'avez qu'à la passer en paramètre à la fonction <IC>Sort</IC>.
            </p>
            <CodeBlock language="csharp">{sort}</CodeBlock>
        </section>

        <section>
            <h2>Exemples</h2>
            <p>
                Si vous voulez trier des entiers à l'envers:
            </p>
            <CodeBlock language="csharp">{reverse}</CodeBlock>
            <p>
                La façon ci-dessus fonctionne, mais il y a une façon plus simple de le faire. En effet, tous les types
                de données comparables en C#, entre autres tous les types de bases, ont une fonction <IC>compareTo</IC> qui
                retourne les valeurs -1, 0 ou 1. C'est cette fonction qui est utilisé par défaut lors du triage. Nous
                pouvons donc utiliser cette fonction, mais en inversant les paramètres de la façon suivante:
            </p>
            <CodeBlock language="csharp">{reverseSimple}</CodeBlock>
            <p>
                Vous pouvez faire la même chose avec les chaînes de caractère pour trier en ordre alphabétique
                inverse:
            </p>
            <CodeBlock language="csharp">{reverseString}</CodeBlock>
            <p>
                On peut aussi faire plus compliqué. Par exemple, si vous voulez comparer pour que les nombres pairs
                soient considéré plus grand que les nombres impairs, mais que pour le reste, l'ordre naturel soit
                respecté, vous pourrez le faire ainsi:
            </p>
            <CodeBlock language="csharp">{oddeven}</CodeBlock>
        </section>
    </>
}
