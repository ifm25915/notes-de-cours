import React from 'react';
import IC from '../component/InlineCode';
import DownloadBlock from '../component/DownloadBlock';

import distribue from '../resources/laboratoire-recherche-distribué.zip';
import solution from '../resources/laboratoire-recherche-solution.zip';

export default function LabSortCompare() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                Dans ce laboratoire, vous devrez programmer en C# l'algorithme de la recherche dichotomique récursive
                pour pouvoir faire des recherches dans des tableaux triés.
            </p>
            <p>
                Compléter le projet distribué ci-dessous:
            </p>
            <ol>
                <li>Télécharger le fichier <IC>distribué.zip</IC>.</li>
                <li>Dézipper le fichier <IC>distribué.zip</IC>.</li>
                <li>
                    Dans le fichier <IC>Program.cs</IC>, programmer les fonctions nécessaires pour pouvoir lancer des
                    recherches dichotomiques récursive sur les tableaux d'entiers.
                </li>
                <li>
                    Dans le <IC>main</IC> du programme, ajouter le code pour demander à l'utilisateur d'entrer une
                    valeur entière qui sera ensuite recherché dans les tableaux d'entier à l'aide de la recherche
                    dichotomique récursive.
                </li>
                <li>
                    Créer un compteur de comparaison pour la recherche dichotomique. Dans l'algorithme de la recherche
                    dichotomique, incrémenter la valeur de ce compteur à chaque fois qu'il y a une comparaison. Donc
                    vous faites l'incrémentation juste avant le <IC>if</IC>. À la fin du <IC>main</IC>, afficher le
                    nombre de comparaison exécuté par l'algorithme.
                </li>
            </ol>
        </section>

        <section>
            <h2>Distribué et solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ distribue } name="distribué.zip"></DownloadBlock.File>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}