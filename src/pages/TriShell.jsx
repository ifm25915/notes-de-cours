import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const shell =
`triShell(tab)
    n = taille de tab
    intervals = [1750, 701, 301, 132, 57, 23, 10, 4, 1]
    pour chaque interval dans intervals
        triInsertionInterval(tab, interval)
        
triInsertionInterval(tab, interval)
    n = taille de tab
    pour i = interval jusqu'à n-1
        elementCle = tab[i]
        int j = i - interval
        tant que j >= 0 et tab[j] > elementCle
            tab[j + interval] = tab[j]
            j -= interval
            
        tab[j + interval] = elementCle`;
        

export default function TriShell() {
    return <>
        <section>
            <h2>Utilité</h2>
            <p>
                Le nom du tri Shell provient de son auteur, Donald Shell. Sa complexité en pire cas
                est <IC>O(n<sup>2</sup>)</IC>. Toutefois, sa complexité en cas moyen est probablement près
                de <IC>O(n log n)</IC>. On dit probablement puisqu'on a pas encore réussi mathématiquement à le
                prouver.
            </p>
            <p>
                Cet algorithme vise à améliorer les performances du tri par insertion pour de grandes quantités de
                données. Puisque le tri Shell est basé sur le tri par insertion, il n'est pas récursif et n'utilise
                pas de mémoire supplémentaire, ce qui en fait un tri très utile pour les systèmes n'ayant pas beaucoup
                d'espace mémoire. Il était d'ailleurs utilisé dans le passé dans le coeur de Linux pour ces raisons.
            </p>
        </section>

        <section>
            <h2>Pseudo-code</h2>
            <p>
                Le tri Shell fonctionne de façon très similaire au tri par insertion, mais contrairement à celui-ci,
                au lieu de toujours comparer avec les éléments précédants un par un, il les compare avec un interval
                plus grand, qui sera graduellement rétrécie pour finalement placer l'élément au bon endroit. Le
                concept est un peu difficile à comprendre, mais cet algorithme fonctionne très bien pour trier des
                tableaux de grande dimension. Voici son pseudo-code:
            </p>
            <CodeBlock language="pseudo">{shell}</CodeBlock>
            <p>
                Vous noterez le tableau d'intervals. Les valeurs qui s'y retrouvent ont été calculé mathématiquement
                pour être le plus optimal possible. 
            </p>
        </section>
    </>;
}
