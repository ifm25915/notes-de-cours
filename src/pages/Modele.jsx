import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

const modeleVecteurListe =
`// Vecteur contenant des entier
List<int> vecteur = new List<int>();

// Liste contenant des chaînes de caractères
LinkedList<string> liste1 = new LinkedList<string>();`;

const modeleCustom =
`// Vecteur contenant des objets Voiture
List<Voiture> vecteur = new List<Voiture>();`;

export default function Modele() {
    return <>
        <section>
            <h2>Différents types</h2>
            <p>
                Beaucoup de langages de programmation nécessite un typage explicite. Un langage à typage explicite
                indique que chaque variable doit avoir un type prédéfini à sa déclaration. C'est le cas du Java, C++,
                C# et plusieurs autres. Dans ces langages, il est donc facile de savoir quel type de données se
                retrouve dans une variable, mais cela nécessite plusieurs restrictions. 
            </p>
            <p>
                Une des restrictions est que chaque tableau doit contenir un type spécifique de données. C'est
                pourquoi en C#, on spécifie toujours le type du tableau lors de sa déclaration.
            </p>
            <CodeBlock language="csharp">{ 'int[] tab = new int[32];' }</CodeBlock>
            <p>
                Par conséquant, les structures utilisant les tableaux doivent aussi contenir un type spécifique de
                données. Il faut toutefois trouver une façon d'indiquer à la structure quel type de données nous
                mettrons dans celle-ci. Les modèles seront la solution.
            </p>
            <ColoredBox heading="À noter">
                Il existe aussi des langages au typage implicite, comme le Javascript ou Python. Ces langages n'oblige
                pas le programmeur à spécifier le type des variables. Le C# autorise aussi le typage implicite, mais
                nous ne l'utiliserons pas dans ce cours.
            </ColoredBox>
        </section>

        <section>
            <h2>Passer un type à une classe</h2>
            <p>
                Les modèles, ou templates, sont une façon de spécifier un type de donnée à une classe pour qu'elle
                l'utilise pour certaines de ses variables. Vous verrez que toutes les classes de structure de données
                en C# vont les utiliser.
            </p>
            <p>
                Pour utiliser une classe avec modèles en C#, vous utiliserez les symboles <IC>&lt;</IC> et <IC>&gt;</IC> avec
                le nom de la classe et le nom du type de données de la façon suivante:
            </p>
            <CodeBlock language="csharp">{'NomClasse<Type> nomVariable = new NomClasse<Type>();'}</CodeBlock>
            <p>
                Si vous regardez le code d'initialisation d'un vecteur ou d'une liste en C#, vous verrez qu'ils
                utilisent toujours un modèle pour spécifier le type de données qu'ils contiennent.
            </p>
            <CodeBlock language="csharp">{modeleVecteurListe}</CodeBlock>
            <p>
                Le type de données n'est pas obligatoirement un type de base du langage. Rien ne vous empêche de créer
                votre propre classe et de l'utiliser comme modèle dans vos structures de données.
            </p>
            <CodeBlock language="csharp">{modeleCustom}</CodeBlock>
        </section>

        <section>
            <h2>Créer une classe qui utilise des modèles</h2>
            <p>
                Dans ce cours, nous ne verrons pas comment créer une classe utilisant des modèles. Si vous êtes
                toutefois intéressé de voir comment cela peut se faire, je vous recommande de voir la documentation
                du langage C#.
            </p>
            <p>
                <a target="_blank" rel="noopener noreferrer" href="https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/generics/generic-classes">
                    C# Generic Classes
                </a>
            </p>
        </section>
    </>;
}