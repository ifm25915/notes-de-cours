import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

const tri =
`// Pour trier un tableau
int[] tableau = new int[]{ 3, 5, 9, 1, 2, 4, 8, 6, 7, 0 };

Array.Sort(tableau);

// Pour trier un vecteur
List<int> vecteur = new List<int>(
    new int[]{ 3, 5, 9, 1, 2, 4, 8, 6, 7, 0 }
);

vecteur.Sort();`;

const binSearch =
`// Recherche dans un tableau trié
int[] tableau = new int[]{ 0, 1, 2, 7, 8, 9, 15, 16, 17, 23, 24, 25 };

// Retourne 6
Array.BinarySearch(tableau, 15);

// Retourne -4
Array.BinarySearch(tableau, 5);

// Recherche dans un vecteur trié
List<int> vecteur = new List<int>(
    new int[]{ 0, 1, 2, 7, 8, 9, 15, 16, 17, 23, 24, 25 }
);

// Retourne 10
vecteur.BinarySearch(24);

// Retourne -7
vecteur.BinarySearch(10);`

export default function TriFonction() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                Nous avons vu plusieurs façon de trier des tableaux jusqu'à présent. Il vous parait peut-être
                redondant d'avoir à coder une fonction de triage à chaque fois que vous voulez trier un tableau dans
                un nouveau projet. Vous avez tout à fait raison. Le triage est effectivement un problème récurrent en
                programmation. Pour ne pas avoir à réécrire nos algorithmes de tri dans tous nos projets, les langages
                de programmation nous offrent généralement leur propre fonction de tri.
            </p>
            <ColoredBox heading="À noter">
                En C#, ces fonctions s'utilise uniquement sur des tableaux et des vecteurs (<IC>List</IC>). Si vous
                voulez trier des données dans une pile, une file, ou une liste chaînée, vous aurez à programmer vos
                propres algorithmes ou à transférer les données dans un tableau ou vecteur pour faire le tri.
            </ColoredBox>
        </section>

        <section>
            <h2>Triage des données</h2>
            <p>
                En C#, l'algorithme de tri utilisé est un dérivé du tri instrospectif (introsort). Pour l'utiliser,
                vous n'avez qu'à avoir un tableau ou un vecteur d'éléments qui peuvent être comparé et à lancer la
                fonction de tri de la façon suivante:
            </p>
            <CodeBlock language="csharp">{tri}</CodeBlock>
        </section>

        <section>
            <h2>Recherche binaire</h2>
            <p>
                L'algorithme de la recherche binaire existe déjà aussi en C#. Pour l'utiliser, vous n'avez qu'à avoir
                un tableau ou un vecteur d'éléments qui ont été préalablement triés et à lancer la fonction de
                recherche dichotomique. La recherche dichotomique retourne l'index de l'élément recherché. Si l'index
                retourné est négatif, c'est que l'élément ne se retrouve pas dans votre tableau ou vecteur.
            </p>
            <CodeBlock language="csharp">{binSearch}</CodeBlock>
        </section>
    </>
}
