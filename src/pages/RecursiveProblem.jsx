import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

import fibonacciRec from '../resources/fibonacci.png';

const fibonacci =
`static int Fibonacci(int n)
{
    if(n < 2)
    {
        return n;
    }
    else
    {
        return Fibonacci(n - 1) + Fibonacci(n - 2);
    }
}`;

const fibonacciIter =
`static int Fibonacci(int n)
{
    int nombre1 = 0;
    int nombre2 = 1;
    if(n < 2)
    {
        return n;
    }
    else
    {
        int valeur = 0;
        for (int i = 2; i <= n; i++)
        {
            valeur = nombre1 + nombre2;
            nombre1 = nombre2;
            nombre2 = valeur;
        }

        return valeur;
    }
}`; 

export default function RecursiveProblem() {
    return <>
        <section>
            <h2>Problèmes impropre à la récursivité</h2>
            <p>
                En règle générale, si la définition d’un problème est récursive et que l’utilisation excessive des
                ressources informatiques n’est pas une contrainte majeure, exploitez la récursivité.
            </p>
            <p>
                Il faut toutefois faire attention, certains problèmes sont particulièrement impropres à une
                implantation récursive, et ce malgré une définition purement récursive. C'est le cas de la fonction
                de Fibonacci, qui est un exemple classique. Cette fonction nous permet de connaître quel est le
                n<sup>ème</sup> nombre dans la suite de Fibonacci.
            </p>
            <div style={{display: 'flex', padding: '.5rem', margin: '0 0 1rem', fontFamily: 'monospace', border: '2px solid var(--border-interactive-color)'}}>
                <div>
                    <div>
                        f(n) = n
                    </div>
                    <div>
                        f(n) = f(n-1) + f(n-2)
                    </div>
                </div>
                <div style={{marginLeft: '2rem'}}>
                    <div>
                        Si n = 0 ou n = 1
                    </div>
                    <div>
                        Si n &gt; 1
                    </div>
                </div>
            </div>
            <p>
                L'implémentation récursive évidente est la suivante:
            </p>
            <CodeBlock language="csharp">{fibonacci}</CodeBlock>
        </section>

        <section>
            <h2>Répétition d'appels</h2>
            <p>
                L'implémentation de la fonction de Fibonacci ci-dessus fonctionne mais est très inefficace. Regardons
                les appels récursifs effectués pour calculer <IC>Fibonacci(5)</IC>:
            </p>
            <img src={fibonacciRec} alt="Affichage des appels récursifs de la fonction Fibonacci" />
            <p>
                On appèle donc:
            </p>
            <ul>
                <li>3 fois <IC>Fibonacci(0)</IC></li>
                <li>5 fois <IC>Fibonacci(1)</IC></li>
                <li>3 fois <IC>Fibonacci(2)</IC></li>
                <li>2 fois <IC>Fibonacci(3)</IC></li>
                <li>1 fois <IC>Fibonacci(4)</IC></li>
            </ul>
            <p>
                On résout donc les mêmes sous-problèmes à répétition! C’est ce qu’on nomme une explosion combinatoire.
                Par exemple, <IC>Fibonacci(32)</IC> requiert 4 294 967 296 invocations de <IC>Fibonacci</IC>. La
                complexité de cette algorithme est O(2<sup>n</sup>), ce qui est vraiment mauvais.
            </p>
            <p>
                Si vous comparez ce type de récursion à d'autres problèmes que nous avons déjà vu, vous remarquerez
                que le problème de la tour de Hanoï fonctionne de façon très similaire. La tour de Hanoï est
                effectivement un problème récursif inefficace.
            </p>
        </section>

        <section>
            <h2>Version non récursive</h2>
            <p>
                Contrairement à l’algorithme pour résoudre les tours de Hanoï, la séquence de Fibonacci peut être
                calculée efficacement en O(n) de la façon itérative suivante:
            </p>
            <CodeBlock language="csharp">{fibonacciIter}</CodeBlock>
            <p>
                Comme vous pouvez le constater, les solutions itératives sont en général toujours préférables à celles
                récursives. Les solutions récursives sont toutefois souvent plus facile à lire ou à implémenter.
                Utilisez-donc intelligemment la récursivité. 
            </p>
        </section>
    </>
}
