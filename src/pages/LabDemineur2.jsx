import React from 'react';
import IC from '../component/InlineCode';
import DownloadBlock from '../component/DownloadBlock';

import distribue from '../resources/laboratoire-demineur2-distribué.zip';
import solution from '../resources/laboratoire-demineur2-solution.zip';

export default function LabDemineur() {
    return <>
        <section>
            <h2>Mise en situation</h2>
            <p>
                La majorité des utilisateurs de Windows<sup>TM</sup> sont familiers avec le jeu
                MineSweep<sup>TM</sup>, inclus avec le système d’exploitation lors de l’achat. Ce laboratoire consiste
                à construire une fonction qui détecte toutes les cases <IC>minées</IC> qui sont directement ou
                indirectement adjacentes à une case choisie par l’utilisateur.
            </p>
        </section>

        <section>
            <h2>Marche à suivre</h2>
            <p>
                Le programme affiche une grille minée, puis demande à l’utilisateur de sélectionner une case minée. Le
                programme <IC>démine</IC> alors toutes les cases minées directement ou indirectement adjacentes à celle
                choisie (un groupe de cases minées adjacentes est appelé blob). Ce processus se répète jusqu’à ce que
                la grille soit complètement déminée.
            </p>
            <p>
                Le code source accompagnant ce laboratoire est incomplet. Vous devez écrire la fonction de déminage de
                blobs. Cette fonction récursive est basée sur les observations ci-dessous.
            </p>
            <p>
                Pour chaque case (rangée, colonne) il y a trois possibilités:
            </p>
            <ul>
                <li>La case (rangée, colonne) n’est pas sur la grille.</li>
                <li>La case (rangée, colonne) est libre.</li>
                <li>La case (rangée, colonne) est minée.</li>
            </ul>
            <p>
                Dans les deux premiers cas il n’y a rien à faire (arrêt des appels récursifs), alors que le dernier
                cas requiert des appels récursifs.
            </p>
            <p>
                Si la case (rangée, colonne) est minée, on peut redéfinir le problème du déminage en termes de
                plusieurs problèmes de déminage de tailles inférieures. Si on marque la case (rangée, colonne) comme
                libre (i.e. déminée), on peut redéfinir alors le problème comme suit:
            </p>
            <ul>
                <li>
                    La taille du blob contenant la case (rangée, colonne) est 1 + la somme de la taille des blobs
                    directement adjacents à la case (rangée, colonne).
                </li>
                <li>
                    Un blob est adjacent à la case (rangée, colonne) si et seulement si il contient une case
                    directement adjacente à la case (rangée, colonne).
                </li>
            </ul>
            <p>
                On peut clarifier ces observations comme suit:
            </p>
            <ul>
                <li>
                    La taille du blob contenant la case (rangée, colonne) est 1 + la somme de la taille des blobs dont
                    une des cases est directement adjacentes à la case (rangée, colonne).
                </li>
                <li>
                    Puisque la case (rangée, colonne) peut avoir jusqu’à 8 cases directement adjacentes, déminer le
                    blob contenant la case (rangée, colonne) peut être réduit à déminer 8 blobs de tailles inférieures
                    une fois la case (rangée, colonne) déminée.
                </li>
            </ul>
            <p>
                À l’aide des observations ci-dessus, complétez la fonction <IC>Blob()</IC> dans le code source fourni.
            </p>
        </section>

        <section>
            <h2>Distribué et solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ distribue } name="distribué.zip"></DownloadBlock.File>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
