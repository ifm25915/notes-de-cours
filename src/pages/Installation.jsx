import React from 'react';
import IC from '../component/InlineCode';

export default function Installation() {
    return <>
        <section>
            <h2>Langage de programmation</h2>
            <p>
                Les structures de données montrées dans ce cours existent dans presque tous les langages de 
                programmation. Dans ce cours, nous utiliserons toutefois le langage C# pour nous exercer à utiliser 
                les structures de données. Si vous voulez utiliser une structure de données dans un autre langage, 
                utilisez un engin de recherche (Google) pour en apprendre d'avantage. Vous verrez que le code est 
                très similaire d'un langage à l'autre.
            </p>
            <p>
                Puisque nous utiliseront le langage C#, nous aurons besoin d'installer quelques programmes pour nous 
                aider. Si vous n'avez pas déjà les logiciels ci-dessous, il est important que vous les téléchargiez!
            </p>
        </section>

        <section>
            <h2>Installation avec Visual Studio Community</h2>
            <p>
                Si vous êtes sur Windows, je vous recommande fortement cette installation puisqu'elle est facile à 
                faire et demande peu de configuration. Visual Studio Community est un éditeur de code très puissant 
                qui est entre autre spécialisé dans l'utilisation du C#. Vous pouvez télécharger à l'adresse suivante:
            </p>
            <p>
                <a target="_blank" rel="noopener noreferrer" href="https://visualstudio.microsoft.com/vs/community/">
                    Visual Studio Community
                </a>
            </p>
            <p>
                Lors de l'installation, le programme vous demanderas de choisir les modules que vous voulez installer.
                Pour ce cours, un seul module est nécessaire: <IC>.NET desktop development</IC>. N'hésitez 
                toutefois pas à en installer d'autres si cela vous convient.
            </p>
        </section>

        <section>
            <h2>Mise à jour de Visual Studio Community</h2>
            <p>
                Si vous avez déjà Visual Studio Community sur votre ordinateur et que vous n'êtes pas certain d'avoir 
                le bon module ou que vous croyez que votre version n'est pas à jour, il est possible de remédier à ce 
                problème sans avoir à refaire une installation complète. Vous n'avez qu'à faire une recherche sur 
                votre ordinateur du <IC>Visual Studio Installer</IC> et de lancer le programme.
            </p>
            <p>
                Si vous voulez mettre à jour Visual Studio Community, vous pourrez le faire en cliquant sur le 
                bouton <IC>Update</IC>.
            </p>
            <p>
                Pour modifier les modules installés, vous pouvez cliquer sur le bouton <IC>Modify</IC> et changer 
                les modules à installer.
            </p>
        </section>

        <section>
            <h2>Installation avec Visual Studio Code</h2>
            <p>
                Visual Studio Code est un éditeur de code très léger et très modulaire. Ce n'est pas le même éditeur 
                que Visual Studio Community. Il est très populaire en ce moment. On peut aussi l'utiliser pour 
                programmer en C#, mais cela demande quelques configuration. L'installation avec Visual Studio Code 
                est donc un peu plus laborieuse. Même si Visual Studio Community est maintenant disponible pour 
                MacOS, leurs utilisateurs vont souvent utilisé Visual Studio Code. Si vous préférez l'utiliser, vous 
                pouvez le télécharger à l'adresse suivante:
            </p>
            <p>
                <a target="_blank" rel="noopener noreferrer" href="https://code.visualstudio.com/">
                    Visual Studio Code
                </a>
            </p>
            <p>
                Vous aurez aussi besoin d'installer la plateforme .NET manuellement. Nous avons besoin de la 
                version 5.0, donc faite attention à celle que vous installer. Vous pouvez la télécharger à l'adresse 
                suivante:
            </p>
            <p>
                <a target="_blank" rel="noopener noreferrer" href="https://dotnet.microsoft.com/download">
                    .NET
                </a>
            </p>
            <p>
                Finalement, dans Visual Studio Code, vous devrez télécharger l'extension <IC>C#</IC>. Faites une 
                recherche dans l'onglet des extensions et installez-le.
            </p>
            <p>
                Finalement, dans Visual Studio Code, une bonne partie des opérations se lance dans la console. Vous 
                aurez besoin de la console pour:
            </p>
            <ul>
                <li>Créer un nouveau projet</li>
                <li>Compiler un projet</li>
                <li>Lancer un projet</li>
            </ul>
            <p>
                Plus d'information sur les commandes nécessaire sont disponible sur cette page:
            </p>
            <p>
                <a target="_blank" rel="noopener noreferrer" href="https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet#examples">
                    .NET CLI
                </a>
            </p>
        </section>
    </>
}
