import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

import tribulle from '../resources/tribulle.png';

const bogosort =
`estTriee(tab)
    n = taille de tab
    pour i = 1 jusqu'à n - 1
        if tab[i - 1] > tab[i]
            return false

    return true

melanger(tab)
    n = taille de tab
    pour i = 0 jusqu'à n-2
        j = index aléatoire entre i et n-1 inclusivement
        échanger tab[i] et tab[j]
        
triStupide(tab)
    tant que !estTriee(tab)
        melanger(tab)`;

const stoogesort =
`triFaireValoir(tab, i, j)
    si tab[i] > tab[j]
        échanger tab[i] et tab[j]
    si j - i + 1 > 2
        t = floor((j - i + 1) / 3)
        triFaireValoir(tab, i, j - t)
        triFaireValoir(tab, i + t, j)
        triFaireValoir(tab, i, j - t)`;

const bubblesort =
`triBulle(tab)
    n = taille de tab
    pour i = n-1 jusqu'à 1
        echange = false
        pour j = 1 jusqu'à i
            if tab[j-1] > tab[j]
                echanger tab[j-1] et tab[j]
                echange = true
            
        if echange est false
            break`;

export default function TriInefficace() {
    return <>
        <section>
            <h2>Triage de données</h2>
            <p>
                Le triage de données est un problème mathématique et informatique de longue date. Il consiste à trier
                un ensemble de données de la façon la plus efficace possible. Le triage de données a plusieurs
                utilités. Il est entre autre très pratique pour l'exécution de plusieurs autres algorithmes. Ce
                problème est aujourd'hui majoritairement mathématique résolue. C'est-à-dire que nous avons des preuves
                mathématiques illustrant que nous avons les meilleures performances possibles avec nos algorithmes de
                triage des données.
            </p>
            <p>
                Dans ce cours, nous explorerons différents algorithmes de triage de données dans le but de voir
                comment ils fonctionnent, lesquels sont les plus efficaces et aussi tout simplement de se pratiquer à
                écrire des algorithmes. Nous commençons ici avec les algorithmes de triage inefficace.
            </p>
        </section>

        <section>
            <h2>Tri stupide</h2>
            <p>
                Le tri stupide, aussi appelé <IC>Bogosort</IC> en anglais, est un tri qui, comme son nom l'indique,
                est vraiment stupide. Il consiste simplement à mélanger les éléments dans un tableau en espérant
                qu'éventuellement, il sera trié. Cet algorithme de tri peut donc réussir à trier des données en seul
                mélange, mais les probabilités sont très faible. En meilleur cas, il s'exécute en <IC>O(n)</IC>, mais en
                moyenne environ en <IC>O(n!)</IC>.
            </p>
            <p>
                Ce tri est en fait une blague et une façon pour les professeur de montrer aux étudiants comment
                comparer les algorithmes de triage. Voici son pseudo-code:
            </p>
            <CodeBlock language="pseudo">{bogosort}</CodeBlock>
        </section>

        <section>
            <h2>Tri faire-valoir</h2>
            <p>
                Le tri faire-valoir est un algorithme de triage récursif qui est reconnu pour être particulièrement
                inefficace. Bien qu'il soit probablement plus efficace que le tri stupide, il est tout de même dans le
                palmarès des pires algorithmes de triage. En effet, il s'exécute environ en <IC>O(n<sup>2.71</sup>)</IC>.
                En anglais, on l'appèle le <IC>Stooge sort</IC>.
            </p>
            <p>
                Cet algorithme consiste donc à faire ceci:
            </p>
            <ol>
                <li>
                    Si la valeur du début est plus grande que la valeur à la fin
                    <ol>
                        <li>On échange les 2 valeurs</li>
                    </ol>
                </li>
                <li>
                    S'il y a plus de 3 éléments
                    <ol>
                        <li>On lance le tri faire-valoir sur le premier 2/3 du tableau</li>
                        <li>On lance le tri faire-valoir sur le dernier 2/3 du tableau</li>
                        <li>On relance le tri faire-valoir sur le premier 2/3 du tableau</li>
                    </ol>
                </li>
            </ol>
            <p>
                Ce tri est aussi purement pédagogique. En aucun cas vous devriez utiliser cet algorithme pour trier
                des données dans un programme pour votre travail. Voici son pseudo-code:
            </p>
            <CodeBlock language="pseudo">{stoogesort}</CodeBlock>
            <p>
                Vous noterez que cet algorithme nécessite 2 paramètres en plus du tableau. Ces paramètres sont l'index
                de départ du triage et l'index de fin du triage. Bref, à sa première exécution, le
                paramètre <IC>i</IC> aura la valeur 0 et le paramètre <IC>j</IC> aura la valeur de la taille du
                tableau moins 1.
            </p>
            <p>
                De plus, pour que cet algorithme fonctionne bien, il faut s'assurer d'arrondir les divisions à
                l'entier supérieur. Dans le pseudo-code ci-dessus, c'est pour cela que l'on fait un <IC>floor()</IC> de
                la valeur <IC>+ 1</IC> pour trouver la valeur de <IC>t</IC>.
            </p>
        </section>

        <section>
            <h2>Tri à bulles</h2>
            <p>
                Le tri à bulle est un algorithme de tri très simple à programmer, mais dont les performances laissent
                à désirer. En effet, il s'exécute environ en <IC>O(n<sup>2</sup>)</IC>. C'est généralement l'un des
                premier algorithme de triage que les programmeurs apprennent à coder vu sa simplicité.
            </p>
            <p>
                Cet algorithme consiste à faire plusieurs passe sur un tableau en ramenant toujours la plus grande
                valeur vers la fin du tableau. À chaque passe, la plus grande valeur restante du tableau sera au bout
                de celui-ci. Le nom de l'algorithme vient donc d'une analogie aux bulles de champagne qui remonte à la
                surface. Voici un exemple plus visuel:
            </p>
            <img src={tribulle} alt="1ère passe d'un tri par bulle" />
            <p>
                Dans l'exemple ci-dessus, à la fin d'une passe, nous savons que la valeur la plus grande est à la fin
                (en haut) de notre tableau. Pour ce faire, l'algorithme fait des permutations si une valeur est plus
                grande que la valeur suivante. Voici le pseudo-code complet:
            </p>
            <CodeBlock language="pseudo">{bubblesort}</CodeBlock>
            <p>
                Cette version du tri par bulle est un peu optimisé. En effet, si le tri fait une passe sans faire
                aucun échange de valeur, il détecte alors qu'il est déjà trié. Il arrête donc le triage plus
                rapidement.
            </p>
        </section>
    </>;
}