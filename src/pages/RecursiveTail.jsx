import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const nontail =
`static int SommeN(int n)
{
    if(n == 1)
    {
        return 1;
    }
    else
    {
        return n + SommeN(n - 1);
    }
}`;

const execution =
`SommeN(5)
5 + SommeN(4)
5 + (4 + SommeN(3))
5 + (4 + (3 + SommeN(2)))
5 + (4 + (3 + (2 + SommeN(1))))
5 + (4 + (3 + (2 + 1)))
5 + (4 + (3 + 3))
5 + (4 + 6)
5 + 10
15`;

const tail =
`static int SommeN(int n, int total = 0)
{
    if(n == 1)
    {
        return total + 1;
    }
    else
    {
        return SommeN(n - 1, total + n);
    }
}`;

const executionTail =
`SommeN(5)
SommeN(4, 5)
SommeN(3, 9)
SommeN(2, 12)
SommeN(1, 14)
15`;

export default function RecursiveTail() {
    return <>
        <section>
            <h2>Optimisation de la récursion</h2>
            <p>
                Dans certains langages de programmation fonctionnel, la récursivité a une part tellement importante
                que leur compilateur optimise automatiquement les récursions pour qu'elles fonctionnent comme une
                boucle, sans avoir à remplir la pile d'appel. C'est le cas des langages de programmation F#, Scala,
                Haskell et plusieurs autres. Théoriquement, Javascript le supporte aussi, mais il n'est pas vraiment
                encore implémenté par les navigateurs et Node.js. La plupart des compilateurs de C++,
                comme <IC>gpp</IC> et <IC>Microsoft Visual C++</IC> supporte aussi ces optisations
            </p>
            <p>
                En C#, ces optimisations n'existe pas encore. Elles sont toutefois envisagées dans le futur et par
                conséquant, il serait intéressant de vous montrer comment vous assurer qu'une récursion puisse être
                optimisée.
            </p>
            <p>
                <a href="https://github.com/dotnet/csharplang/discussions/2544" target="_blank" rel="noopener noreferrer" >Proposition d'optimisation dans le langage C#</a>
            </p>
            <p>
                <a href="https://github.com/dotnet/csharplang/issues/2304" target="_blank" rel="noopener noreferrer" >Suivi de l'implémentation de l'optimisation dans le langage C#</a>
            </p>
        </section>

        <section>
            <h2>Récursion terminale</h2>
            <p>
                Pour que ces optisations puissent être faite, il faut toutefois écrire nos récursions d'une façon un
                peu spéciale. Cette écriture particulière se nomme la <IC>récursion terminale</IC> ou <IC>tail recursion</IC> en
                anglais. Essentiellement, pour pouvoir faire l'optimisation, l'appel récursif doit absolument être la
                dernière instruction exécutée dans votre fonction récursive.
            </p>
            <p>
                Le concept est plus facile à comprendre avec un exemple. Supposons que nous avons une fonction
                récursive simple qui calcule la somme des nombres nombres entier entre 0 et <IC>n</IC>:
            </p>
            <CodeBlock language="csharp">{nontail}</CodeBlock>
            <p>
                Dans cette fonction, on semble voir que l'appel récursif est la dernière opération exécuté, mais ce
                n'est pas le cas. Voici comment le compilateur exécute cette fonction pour <IC>SommeN(5)</IC>:
            </p>
            <CodeBlock language="csharp">{execution}</CodeBlock>
            <p>
                Comme vous pouvez le constater, ce n'est pas l'appel récursif qui est exécuté en dernier, mais plutôt
                l'addition. Nous n'avons donc pas une récursion terminale ici. Il est toutefois possible de réécrire
                la fonction récursive pour qu'elle exécute son appel récursif à la fin.
            </p>
            <CodeBlock language="csharp">{tail}</CodeBlock>
            <p>
                La stratégie ici est d'utilisé un accumulateur pour guarder la somme calculé au fur et à mesure que
                nous exécutons les appels récursifs. Celà nous permet de nous assurer que la dernière opération dans
                notre récursion est l'appel récursif.
            </p>
            <CodeBlock language="csharp">{executionTail}</CodeBlock>
            <p>
                Il n'est pas possible de convertir toutes les fonctions récursives pour qu'elles utilisent la
                récursion terminale, mais plusieurs d'entre elles le permettent. Ceci étant dit, convertir une
                fonction pour qu'elle utilise la récursion terminale n'est pas facile non plus.
            </p>
        </section>

        <section>
            <h2>Optimisation de la récursion terminale</h2>
            <p>
                L'optimisation de la récursion terminale est la capacité d'un compilateur ou d'un compilateur à
                optimiser la récursion terminale pour qu'elle n'utilise pas plus d'espace sur la pile.
                Essentiellement, le compilateur va convertir la fonction récursive en boucle qui n'utilise pas la pile
                d'appel.
            </p>
            <p>
                Comme mentionné précédemment, ce n'est pas tous les langages qui supportent l'optimisation de la
                récursion terminale. Il est toutefois recommandé d'essayer de transformer vos fonctions récursives en
                récursion terminale pour ainsi permettre d'optimiser ses appels pour les compilateurs le supportant.
            </p>
        </section>
    </>
}
