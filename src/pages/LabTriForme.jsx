import React from 'react';
import IC from '../component/InlineCode';
import ColoredBox from '../component/ColoredBox';
import DownloadBlock from '../component/DownloadBlock';

import distribue from '../resources/laboratoire-tri-forme-distribué.zip';
import solution from '../resources/laboratoire-tri-forme-solution.zip';

export default function LabTriForme() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                Le but de ce laboratoire est d'être capable de trier un tableau de rectangles en fonction de leur
                aire. Pour ce faire, vous devrez mettre en oeuvre les techniques vues dans ce module.
            </p>
            <p>
                Compléter le projet distribué ci-dessous:
            </p>
            <ol>
                <li>Télécharger le fichier <IC>distribué.zip</IC>.</li>
                <li>Dézipper le fichier <IC>distribué.zip</IC>.</li>
                <li>
                    Ajouter les éléments nécessaire à la classe <IC>Rectangle</IC> pour que ses objets puissent se
                    comparer entre eux. Les <IC>Rectangle</IC> doivent être comparé par leur aire. Les rectangles
                    ayant une aire plus grande sont considéré plus grand et ce, peu importe la hauteur ou la largeur
                    de celui-ci.
                </li>
                <li>
                    Dans le fichier <IC>main.cs</IC>, trier le <strong>vecteur</strong> de rectangles. Afficher à la
                    console les données du vecteur avant et après son tri.
                </li>
                <li>
                    Dans le fichier <IC>main.cs</IC>, trier le <strong>tableau</strong> de rectangles, mais en ordre
                    inverse. Afficher à la console les données du tableau avant et après son tri.
                </li>
                <li>
                    Dans le fichier <IC>main.cs</IC>, après les triages, demandez à l'utilisateur d'entrer une aire.
                    À l'aide de la recherche binaire, trouver si un rectangle ayant cet aire existe dans
                    le <strong>vecteur</strong> de rectangles.
                    <ColoredBox heading="Attention">
                        On lance la recherche dichotomique dans le vecteur puique le tableau de rectangle est trié en
                        ordre inverse. Il n'est pas possible d'exécuter la recherche binaire dsur le tableau, à moins
                        de changer la méthode de comparaison dans la recherche binaire aussi, ce qui est plus
                        compliqué.
                    </ColoredBox>
                </li>
            </ol>
        </section>

        <section>
            <h2>Distribué et solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ distribue } name="distribué.zip"></DownloadBlock.File>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>
}
