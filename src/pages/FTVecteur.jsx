import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

import vecteur from '../resources/vecteur.png';
import vecteur2 from '../resources/vecteur2.png';

const creation = 
`// Vecteur d'entiers
List<int> vecteur1 = new List<int>();

// Vecteur de chaînes de caractères
List<string> vecteur2 = new List<string>();

// Vecteur avec une taille de départ prédéfinie
List<int> vecteur3 = new List<int>(20);

// Vecteur à partir d'un tableau existant
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
List<int> vecteur4 = new List<int>(tab);`;

const accesAleatoire = 
`// Vecteur
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
List<int> vecteur = new List<int>(tab);

// Accès aléatoire
Console.WriteLine(vecteur[3]);
vecteur[5] = -100;`;

const ajouter =
`// Vecteur
List<int> vecteur = new List<int>()

// Ajouter à la fin
vecteur.Add(1);
vecteur.Add(3);
vecteur.Add(3);
vecteur.Add(7);`;

const inserer =
`// Vecteur
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
List<int> vecteur = new List<int>(tab);

// Ajouter à un certain index
// 1er paramètre est l'index
// 2e paramètre est la valeur à insérer
vecteur.Insert(3, 42);`;

const retirer =
`// Vecteur
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
List<int> vecteur = new List<int>(tab);

// Enlever une valeur spécifique
// Ça enlève uniquement la 1ère valeur trouvé
vecteur.Remove(8);

// Enlever une valeur à un index spécifique
vecteur.RemoveAt(4);

// Enlever une valeur au début ou à la fin
vecteur.RemoveAt(0);
vecteur.RemoveAt(vecteur.Count - 1);`;

const recherche =
`// Vecteur
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
List<int> vecteur = new List<int>(tab);

// Retourne l'index de la valeur qu'on recherche
// Retourne -1 si la valeur n'est pas trouvé
int index = vecteur.IndexOf(7);`;

const parcourir =
`// Vecteur
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
List<int> vecteur = new List<int>(tab);

for (int i = 0; i < vecteur.Count; i++)
{
    Console.Write(vecteur[i] + ", ");
}

foreach (int valeur in vecteur)
{
    Console.Write(valeur + ", ");
}`;

export default function FTVecteur() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                Le vecteur est une structure basé directement sur le tableau. À la base, le tableau interne du vecteur
                contiendra seulement des valeurs <IC>null</IC> pour commencer. Lorsqu'on ajoutera des valeurs dans le
                tableau, on remplacera les valeurs <IC>null</IC> dans l'ordre, par les valeurs que l'on veut ajouter.
                Si nous essayons d'ajouter plus de valeurs que la taille totale du tableau, celui-ci créera un
                nouveau tableau beaucoup plus grand, pour être certain d'avoir de l'espace pour les prochaines
                insertions, et copiera automatiquement les données. Le vecteur est donc un tableau sur stéroïdes. Il
                est réagi comme un tableau, mais dans lequel on peut y ajouter ou retirer des données facilement et
                relativement efficacement.
            </p>
            <p>
                On le représentera souvent graphiquement de la façon suivante:
            </p>
            <img src={ vecteur } alt="Représentation d'un tableau" />
            <p>
                Si on déborde du tableau, le tableau sera automatiquement aggrandi. Par exemple, il pourrait doubler
                son espace mémoire en créant un nouveau tableau pour s'assurer de bien fonctionner de la façon
                suivante:
            </p>
            <img src={vecteur2} alt="Représentation d'un tableau" />
            <p>
                En C#, un vecteur se crée avec la classe <IC>List</IC> de la façon suivante:
            </p>
            <CodeBlock language="csharp">{ creation }</CodeBlock>
        </section>

        <section>
            <h2>Avantages</h2>
            <ul>
                <li>Permet l'ajout et le retrait de données à la fin du tableau de façon efficace</li>
                <li>Très performant pour l'accès aléatoire</li>
            </ul>
        </section>

        <section>
            <h2>Inconvénients</h2>
            <ul>
                <li>Pas optimal pour l'utilisation de la mémoire</li>
                <li>L'ajout et le retrait de données au début ou au milieu du tableau n'est pas efficace</li>
                <li>Pour faire des recherches, on doit généralement parcourir toutes les données</li>
                <li>
                    Puisque c'est essentiellement un tableau avec des fonctions utilitaires, les vecteurs de taille
                    gigantesque peuvent aussi causer des problèmes s'il n'y a pas assez d'espace mémoire un à côté de
                    l'autre de disponible
                </li>
            </ul>
        </section>

        <section>
            <h2>Opérations</h2>
            <dl>
                <dt>Accès aléatoire</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(1)</IC>
                    </p>
                    <p>
                        Puisque le vecteur est directement basé sur un tableau, son efficacité pour l'accès aléatoire
                        est le même. Cette opération est très efficace. En C#, on utilise l'accès aléatoire sur
                        une <IC>List</IC> de la même façon que sur un tableau.
                    </p>
                    <CodeBlock language="csharp">{ accesAleatoire }</CodeBlock>
                </dd>

                <dt>Ajouter des valeurs</dt>
                <dd>
                    <p>
                        Complexité algorithmique si on ajoute à la fin: <IC>O(1)</IC>
                    </p>
                    <p>
                        Complexité algorithmique si on ajoute au début ou au milieu: <IC>O(n)</IC>
                    </p>
                    <p>
                        Le vecteur est très pratique si notre but est simplement d'ajouter des éléments à la fin de
                        celui-ci. En effet, même avec les aggrandissements potentiel de tableau, l'ajout d'un élément
                        se fait en moyenne en <IC>O(1)</IC>, ce qui est très pratique. Pour ajouter un élément à la
                        fin d'un vecteur en C#, on utilise le code suivant:
                    </p>
                    <CodeBlock language="csharp">{ajouter}</CodeBlock>
                    <p>
                        Si vous voulez plutôt insérer au milieu ou au début du vecteur, le code doit décaler
                        plusieurs éléments dans le tableau, ce qui peut être long. C'est la raison pour laquelle il
                        s'exécute en <IC>O(n)</IC>. En C#, on utilise la méthode <IC>Insert</IC> pour ajouter un
                        élément à un endroit précis dans le tableau:
                    </p>
                    <CodeBlock language="csharp">{ inserer }</CodeBlock>
                </dd>

                <dt>Supprimer des valeurs</dt>
                <dd>
                    <p>
                        Complexité algorithmique si on supprime à la fin: <IC>O(1)</IC>
                    </p>
                    <p>
                        Complexité algorithmique si on supprime au début ou au milieu: <IC>O(n)</IC>
                    </p>
                    <p>
                        Dans le code du vecteur, la suppression de valeurs fonctionne de façon similaire à l'ajout de
                        valeur. Lorsque la suppression se fait en fin de tableau, elle est très efficace, mais
                        partout ailleurs, c'est un peu plus lent puisqu'il y a des décalages de valeurs à faire. En
                        C#, on supprimer à l'aide des méthodes <IC>Remove</IC> et <IC>RemoveAt</IC>:
                    </p>
                    <CodeBlock language="csharp">{ retirer }</CodeBlock>
                </dd>

                <dt>Recherche de valeurs</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(n)</IC>
                    </p>
                    <p>
                        Pour trouver une valeur dans le vecteur, le code doit parcourir toutes les valeurs une par une
                        pour y rétourner l'index de la valeur que l'on cherche. Heureusement, dans le vecteur en C#,
                        il existe la méthode <IC>IndexOf</IC> pour nous aider:
                    </p>
                    <CodeBlock language="csharp">{ recherche }</CodeBlock>
                    <p>
                        Il existe des façons plus efficace de faire des recherches dans un tableau, mais celles-ci
                        nécessitent quelques notions supplémentaire que nous verrons plus tard dans le cours
                    </p>
                </dd>

                <dt>Parcourir</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(n)</IC>
                    </p>
                    <p>
                        Si vous devez parcourir vous-même un vecteur, vous pouvez facilement le faire à l'aide d'une
                        boucle régulière ou d'une boucle à itérateur, comme si c'était un tableau.
                    </p>
                    <CodeBlock language="csharp">{parcourir}</CodeBlock>
                    <ColoredBox heading="Attention">
                        Vous noterez que nous n'utilisons pas <IC>.Length</IC> pour avoir la taille du vecteur. En
                        effet, dans un vecteur, c'est la propriété <IC>.Count</IC> qui nous permet de savoir combien
                        de valeurs se retrouvent dans notre structure.
                    </ColoredBox>
                </dd>
            </dl>
        </section>

        <section>
            <h2>Documentation</h2>
            <p>
                Si vous voulez voir plus de fonctionnalité et de possibités avec le vecteur du langage C#, je
                vous recommande fortement d'aller visiter sa page de documentation. Cela vous permettra de voir toutes
                les méthodes disponibles et d'apprendre à mieux les utiliser. Vous pourrez y trouver de nombreuses
                fonctionnalités que nous n'avons pas encore couvert.
            </p>
            <p>
                <a target="_blank" rel="noopener noreferrer" href="https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.list-1?view=net-5.0">
                    C# List&lt;T&gt; Class
                </a>
            </p>
        </section>
    </>
}
