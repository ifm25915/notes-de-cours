import React from 'react';
import IC from '../component/InlineCode';
import DownloadBlock from '../component/DownloadBlock';

import solution from '../resources/laboratoire-article-solution.zip';

export default function LabArticle() {
    return <>
        <section>
            <h2>Mise en situation</h2>
            <p>
                On veut organiser les infomations des cours dans un dictionnaire. Les données sont enregistrées dans 2 
                ensembles comme suit:
            </p>
            <dl>
                <dt>codes</dt>
                <dd>{'{'} 1005, 2133, 1101, 5302, 8104, 5016 {'}'}</dd>

                <dt>noms</dt>
                <dd>
                    {'{'} Structures des données, Mathématiques, Programmation Web, Multimédia et infographie, Réseaux
                    et télématiques, Gestion des bases de données {'}'}
                </dd>
            </dl>
            <p>
                Le résultat demandé doit être affiché dans un dictionnaire comme suit:
            </p>
            <table>
                <thead>
                    <tr><th>Code</th><th>Matière</th></tr>
                </thead>
                <tbody>
                    <tr><td>8104</td><td>Gestion des bases de données</td></tr>
                    <tr><td>5302</td><td>Mathématiques</td></tr>
                    <tr><td>5016</td><td>Multimédia et infographie</td></tr>
                    <tr><td>2133</td><td>Programmation Web</td></tr>
                    <tr><td>1101</td><td>Réseaux et télématiques</td></tr>
                    <tr><td>1005</td><td>Structures des données</td></tr>
                </tbody>
            </table>
            <p>
                Vous noterez que les codes sont en ordre numérique décroissant et que le noms sont en ordre
                alphabétique croissant.
            </p>
        </section>

        <section>
            <h2>Marche à suivre</h2>
            <p>
                Compléter le projet distribué ci-dessous:
            </p>
            <ol>
                <li>Télécharger le fichier <IC>distribué.zip</IC>.</li>
                <li>Dézipper le fichier <IC>distribué.zip</IC>.</li>
                <li>
                    Faire le traitement nécessaire pour mettre le contenu des 2 ensembles dans le dictionnaire. Vous
                    ne pouvez pas mettre le contenu manuellement. Vous devez donc trier les ensembles et boucler
                    dessus pour ajouter leurs valeurs dans le dictionnaire de cours. N'hésitez pas à transférer le
                    contenu des ensembles dans d'autres structures de données si c'est nécessaire.
                </li>
                <li>
                    Afficher le contenu du dictionnaire à la console.
                </li>
                <li>
                    Créer une méthode qui permet la saisie d'un code de cours et de rechercher son nom dans le
                    dictionnaire. Utiliser cette méthode dans le <IC>main</IC> de cotre programme.
                </li>
            </ol>
        </section>

        <section>
            <h2>Distribué et solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
