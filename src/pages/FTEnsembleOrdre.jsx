import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

import ensembleordre from '../resources/ensembleordre.png';
import ensemblearbre from '../resources/ensemblearbre.png';

const creation = 
`// Ensemble ordonné d'entiers
SortedSet<int> ensemble = new SortedSet<int>();

// Ensemble ordonné de chaînes de caractères
SortedSet<string> ensemble2 = new SortedSet<string>();

// Ensemble ordonné à partir de données existante
// S'il y a des répétitions dans le tableau, 
// elles sont retirées dans l'ensemble
// Les données sont automatiquement ordonnées
int[] tab = new int[10] { 9, 0, 8, 1, 8, 2, 0, 3, 8, 4 };
SortedSet<int> ensemble3 = new SortedSet<int>(tab);`;

const add =
`// Ensemble ordonné
SortedSet<int> ensemble = new SortedSet<int>();

// Ajouter des données
ensemble.Add(1);`;

const remove =
`// Ensemble ordonné
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
SortedSet<int> ensemble = new SortedSet<int>(tab);

// Retirer un élément
ensemble.Remove(7);`;

const contain =
`// Ensemble ordonné
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
SortedSet<int> ensemble = new SortedSet<int>(tab);

// Rechercher un élément
if(ensemble.Contains(7))
{
    // ...
}`;

const parcourir =
`// Ensemble
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
SortedSet<int> ensemble = new SortedSet<int>(tab);

// Parcourir l'ensemble
// Affiche toujours 0 1 2 3 4 5 6 7 8 9
foreach(int element in ensemble)
{
    Console.Write(element + " ");
}`;

export default function FTEnsemble() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                L'ensemble ordonné est une structure de données basé sur l'ensemble, mais qui garde un ordre pour nos
                données. Ce concept peut être pratique pour certaines applications, mais vient malheureusement avec
                quelques pertes de performances.
            </p>
            <p>
                On représentera graphiquement l'ensemble de la façon suivante:
            </p>
            <img src={ensembleordre} alt="Représentation d'un ensemble ordonné" />
            <p>
                L'ensemble ordonné, dans la mémoire de votre ordinateur, sera sauvegardé sous la forme d'un arbre pour
                être capable de rester trié. C'est pourquoi vous pouvez aussi représenter un ensemble ordonné de la
                façon ci-dessous. Dans ce cours, nous ne verrons pas comment l'ordinateur fait pour manipuler cet
                arbre en mémoire.
            </p>
            <img src={ensemblearbre} alt="Représentation d'un ensemble sous forme d'arbre" />
            <p>
                Les opérations de base, comme l'ajout, la suppression et la recherche de données fonctionnent de la
                même façon dans un ensemble ordonné que dans un ensemble de base. Ces fonctionnalités seront toutefois
                moins efficace que dans un ensemble de base.
            </p>
            <p>
                L'ensemble ordonné possède aussi les mêmes fonctionnalités supplémentaires que l'ensemble de base. Il
                est donc possible de faire l'union, l'intersection et la différence symétrique. Ces 3 opérations
                sont aussi efficace que dans l'ensemble de base.
            </p>
            <p>
                En C#, un ensemble ordonné s'utilise avec la classe <IC>SortedSet</IC>. L'ordre des données dépends du
                type des données. Les nombres sont en ordre numérique et les chaînes de caractères sont en ordre alphabétique
                unicode. Si les éléments ne sont pas comparable, il faudra leur indiquer une façon de les comparer, ce
                que nous verrons plus tard. Vous pouvez créer un <IC>SortedSet</IC> de la façon suivante:
            </p>
            <CodeBlock language="csharp">{ creation }</CodeBlock>
        </section>

        <section>
            <h2>Avantages</h2>
            <ul>
                <li>Les opérations d'ajout et de retrait sont efficace, mais pas autant que l'ensemble de base.</li>
                <li>La recherche d'un élément est très efficace, mais pas autant que l'ensemble de base.</li>
                <li>Possède des opérations mathématiques spéciales.</li>
                <li>Les données garde un ordre défini par leur type.</li>
            </ul>
        </section>

        <section>
            <h2>Inconvénients</h2>
            <ul>
                <li>Pas d'accès aléatoires.</li>
                <li>Pas de répétition de données.</li>
            </ul>
        </section>

        <section>
            <h2>Opérations</h2>
            <dl>
                <dt>Accès aléatoire</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>N/A</IC>
                    </p>
                    <p>
                        On ne peut pas faire d'accès aléatoire dans un ensemble ordonné. Il faut le convertir dans une autre
                        structure de données pour pouvoir le faire, ce qui n'est pas efficace.
                    </p>
                </dd>

                <dt>Ajouter</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(log n)</IC>
                    </p>
                    <p>
                        Permet d'ajouter un élément dans l'ensemble ordonné. L'ajout dans un ensemble ordonné est toujours assez efficace.
                        Comme pour l'ensemble de base, une valeur booléenne est retourné pour indiquer si la valeur à
                        ajouter existe déjà dans l'ensemble. Cette opération est moins efficace que celle de l'ensemble de base.
                    </p>
                    <CodeBlock language="csharp">{add}</CodeBlock>
                </dd>

                <dt>Retirer</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(log n)</IC>
                    </p>
                    <p>
                        Permet de retirer un élément dans l'ensemble ordonné. Le retrait dans un ensemble ordonné est toujours assez efficace.
                        Comme pour l'ensemble de base, une valeur booléenne est retourné pour indiquer si la valeur à
                        retirer est présente dans l'ensemble. Cette opération est moins efficace que celle de l'ensemble de base.
                    </p>
                    <CodeBlock language="csharp">{remove}</CodeBlock>
                </dd>

                <dt>Recherche</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(log n)</IC>
                    </p>
                    <p>
                        Dans un ensemble ordonné, la recherche correspond à savoir si un élément est dans l'ensemble ou non.
                        La recherche dans un ensemble ordonné est toujours assez efficace. Cette opération est moins
                        efficace que celle de l'ensemble de base.
                    </p>
                    <CodeBlock language="csharp">{contain}</CodeBlock>
                </dd>

                <dt>Parcourir</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(n)</IC>
                    </p>
                    <p>
                        Puisqu'il n'y a pas d'accès aléatoire dans un ensemble ordonné, on peut uniquement le parcourir à l'aide
                        d'une boucle à itérateur. La boucle <IC>foreach</IC> est donc importante ici:
                    </p>
                    <CodeBlock language="csharp">{parcourir}</CodeBlock>
                    <p>
                        À l'exécution du code ci-dessus, vous noterez que l'ordre de parcour est celui des données en ordre
                        croissant puisque cet ensemble est toujours ordonné.
                    </p>
                </dd>
            </dl>
        </section>

        <section>
            <h2>Documentation</h2>
            <p>
                Si vous voulez voir plus de fonctionnalité et de possibités avec l'ensemble ordonné du langage C#,
                je vous recommande fortement d'aller visiter sa page de documentation. Cela vous permettra de voir
                toutes les méthodes disponibles et d'apprendre à mieux les utiliser. Vous pourrez y trouver de
                nombreuses fonctionnalités que nous n'avons pas couvert.
            </p>
            <p>
                <a target="_blank" rel="noopener noreferrer" href="https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.sortedset-1?view=net-5.0">
                    C# SortedSet&lt;T&gt; Class
                </a>
            </p>
        </section>
    </>
}
