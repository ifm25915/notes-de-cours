import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import Situation from '../component/Situation';
import ColoredBox from '../component/ColoredBox';

const algosimple = 
`// Tableau de 10 données
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };

// Algorithme pour afficher le contenu du tableau
foreach(int valeur in tab){
    Console.Write(valeur);
}`;

export default function Complexite() {
    return <>
        <section>
            <h2>Comparer des algorithmes</h2>
            <p>
                Comparer des algorithmes ou des morceaux de code pour voir celui qui est le plus performant n'est pas 
                nécessairement une tâche facile. À première vue, on pourrait simplement penser que chronométrer le 
                code nous donnerais des informations suffisantes. C'est ce qu'on appele l'évaluation empirique d'un 
                algorithme. En bref, on l'évalue en l'exécutant et en regardant ses performances, donc dans ce cas-ci,
                le temps nécessaire à son exécution. L'évaluation empirique comporte toutefois certains problèmes:
            </p>
            <ul>
                <li>
                    Est-ce que le code est exécuté sur un ordinateur récent, plus fort, moins fort ou spécialisé?
                </li>
                <li>
                    Est-ce qu'il y a d'autres applications qui roulent en arrière-plan sur l'ordinateur qui pourrait 
                    ralentir le programme?
                </li>
                <li>
                    Est-ce que l'exécution de l'algorithme peu changer dépendant des données qui y sont passées?
                </li>
            </ul>
            <p>
                Toutes ces questions peuvent effectivement brouiller nos résultats et nos comparatifs. L'évaluation 
                empirique est donc utile pour avoir un résultat facile à comprendre, mais peut être erroné. Nous 
                allons donc plutôt comparer nos algorithmes mathématiquement.
            </p>
        </section>

        <section>
            <h2>Notation Grand O</h2>
            <p>
                Évaluer un algorithme mathématiquement n'est pas toujours facile. Dans ce cours, on ne vous demande 
                pas d'évaluer mathématiquement du code, mais plutôt d'être capable de comparer les résultats. En bref, 
                l'évaluation mathématique sera déjà faite, mais vous devrez être capable de faire la comparaison des 
                résultats. Pour se faire, vous devez quand même avoir une certaine idée de comment cette évaluation 
                fonctionne.
            </p>
            <p>
                Bien que l'évaluation mathématique puisse nous sortir de nombreuses données, nous nous concentrerons 
                sur la notation Grand O. Cette notation est une indication de comment un algorithme performe dans ses 
                pires conditions. En fait, il nous donnera approximativement le nombre d'opération fait par 
                l'algorithme dans le pire des cas. Voici un exemple pour nous aider à comprendre:
            </p>
            <CodeBlock language="csharp">{ algosimple }</CodeBlock>
            <p>
                Nous avons ici un tableau de 10 données et un algorithme très simple qui affiche le contenu de notre 
                tableau. Après l'évaluation mathématique, nous aurions la notation Grand O suivante: <IC>O(n)</IC>.
                Pour mieux comprendre cette notation, voici quelques détails supplémentaires:
            </p>
            <ul>
                <li>Le <IC>O</IC> indique que l'évaluation se fait dans le pire des cas.</li>
                <li>Le <IC>n</IC> indique le nombre de données qui sont utilisé par l'algorithme.</li>
            </ul>
            <p>
                La traduction française de <IC>O(n)</IC> est donc la suivante:
            </p>
            <Situation>
                Dans le pire des cas, notre algorithme va exécuter approximativement autant d'opération que le nombre 
                d'élément dans notre tableau, soit le nombre de données.
            </Situation>
            <p>
                Cette affirmation est exacte. En effet, dans l'exemple ci-dessus, le code exécute exactement 10 fois 
                le <IC>Console.Write(valeur)</IC> puisqu'il y a 10 données dans notre tableau. S'il y a 1337 données 
                dans notre tableau, le <IC>Console.Write(valeur)</IC> est exécuté exactement 1337 fois.
            </p>
            <p>
                L'évaluation mathématique ne semble peut-être pas trop complexe ici, mais pour les algorithmes 
                complexes, elle peut rapidement devenir un cauchemars mathématique rempli d'intégrales et de calculs
                complexes (texte inspiré de faits vécus).
            </p>
        </section>

        <section>
            <h2>Ordre d'efficacité</h2>
            <p>
                Comme vous pouvez probablement le deviner, le <IC>O(n)</IC> n'est pas la seule valeur qui peut être 
                ressortie d'une évaluation mathématique. Voici donc un tableau explicatif des valeurs Grand O les plus
                communes dans leur ordre d'efficacité:
            </p>
            <div className="overflow-protection">
                <table>
                    <thead>
                        <tr>
                            <th>Notation</th>
                            <th>Explication</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><IC>O(1)</IC></td>
                            <td>
                                L'algorithme s'exécute en approximativement 1 opération et ce, peut importe la quantité de 
                                données. C'est la meilleur performance que peut avoir un algorithme.
                            </td>
                        </tr>
                        <tr>
                            <td><IC>O(log&nbsp;n)</IC></td>
                            <td>
                                L'algorithme doit parcourir les données, mais n'a pas besoin de toutes les parcourir. 
                                Cette valeur indique généralement un algorithme très performant.
                            </td>
                        </tr>
                        <tr>
                            <td><IC>O(n)</IC></td>
                            <td>
                                L'algorithme doit parcourir toutes les données, mais seulement une fois. Dépendant de 
                                l'algorithme, on peut considérer cette valeur comme performante. Ceci étant dit, avec 
                                beaucoup de données, l'algorithme va ralentir.
                            </td>
                        </tr>
                        <tr>
                            <td><IC>O(n&nbsp;log&nbsp;n)</IC></td>
                            <td>
                                L'algorithme doit parcourir toutes les données et pour chaque donnée parcouru, il doit 
                                parcourir une partie des données. On commence ici à avoir des valeurs qui sont vraiment 
                                moins indique des algorithmes beaucoup moins efficaces lorsqu'il y a beaucoup de données.
                            </td>
                        </tr>
                        <tr>
                            <td><IC>O(n<sup>2</sup>)</IC></td>
                            <td>
                                L'algorithme doit parcourir toutes les données et pour chaque donnée parcouru, il doit 
                                reparcourir toutes les données.
                            </td>
                        </tr>
                        <tr>
                            <td><IC>O(c<sup>n</sup>)</IC></td>
                            <td>
                                L'algorithme doit parcourir toutes les données de façon exponentielle. Cette valeur 
                                indique un algorithme qui n'est vraiment pas efficace.
                            </td>
                        </tr>
                        <tr>
                            <td><IC>O(n!)</IC></td>
                            <td>
                                L'algorithme doit parcourir toutes les données de façon factorielle. Cette valeur est 
                                l'une des pires, indiquant qu'un algorithme n'est vraiment pas efficace, surtout avec 
                                beaucoup de données.
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <p>
                Dans le cours, nous utiliserons la notation Grand O pour comparer l'efficacité de certains algorithmes
                et de certaines opérations sur les structures de données. Il est donc important d'être capable de se 
                référer au tableau ci-dessus pour comparer les valeurs Grand O.
            </p>
            <ColoredBox heading="Attention">
                Un algorithme s'exécutant en <IC>O(n<sup>2</sup>)</IC> ne semble peut-être pas efficace, mais dans 
                certains cas, il pourrait être prouvé mathématiquement que c'est la meilleure valeur possible. Faite 
                donc attention lorque vous jugez un algorithme par sa valeur Grand O. Même avec une valeur médiocre,
                c'est peut-être le mieux qu'on puisse faire avec certains algorithmes.
            </ColoredBox>
        </section>
    </>
}
