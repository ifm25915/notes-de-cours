import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

const enumString =
`Console.WriteLine("Entrez une saison: ");
string saison = Console.ReadLine();
if(saison == "printemps") { ... }
else if(saison == "été") { ... }
else if(saison == "automne") { ... }
else if(saison == "hiver") { ... }
else { ... }`;

const enumInt =
`Console.WriteLine("0: printemps | 1: été");
Console.WriteLine("2: automne   | 3: hiver");
Console.WriteLine("Entrez une saison:");

int saison = int.Parse(Console.ReadLine());
if(saison == 0) { ... }
else if(saison == 1) { ... }
else if(saison == 2) { ... }
else if(saison == 3) { ... }
else { ... }`;

const enumConst =
`const int PRINTEMPS = 0;
const int ETE = 1;
const int AUTOMNE = 2;
const int HIVER = 3;

Console.WriteLine("0: printemps | 1: été");
Console.WriteLine("2: automne   | 3: hiver");
Console.WriteLine("Entrez une saison:");

int saison = int.Parse(Console.ReadLine());
if(saison == PRINTEMPS) { ... }
else if(saison == ETE) { ... }
else if(saison == AUTOMNE) { ... }
else if(saison == HIVER) { ... }
else { ... }`;

const enum1 =
`// Création du type de données
enum Saison { PRINTEMPS, ETE, AUTOMNE, HIVER };

static void Main(string[] args)
{
    // Création et modification de variables
    // ayant le type de données "Saison"
    Saison variableDeSaison = Saison.ETE;
    variableDeSaison = Saison.HIVER;
}`;

const enumCast =
`enum Saison { PRINTEMPS, ETE, AUTOMNE, HIVER };

static void Main(string[] args)
{
    // Affiche "ETE"
    int valeur = 1;
    Console.WriteLine((Saison) valeur);
}`;

const enum2 =
`enum Saison { PRINTEMPS, ETE, AUTOMNE, HIVER };

static void Main(string[] args)
{
    Console.WriteLine("0: printemps | 1: été");
    Console.WriteLine("2: automne   | 3: hiver");
    Console.WriteLine("Entrez une saison:");

    Saison saison = (Saison)int.Parse(Console.ReadLine());
    if(saison == Saison.PRINTEMPS) { ... }
    else if(saison == Saison.ETE) { ... }
    else if(saison == Saison.AUTOMNE) { ... }
    else if(saison == Saison.HIVER) { ... }
    else { ... }
}`;

export default function Enum() {
    return <>
        <section>
            <h2>Choix parmi plusieurs options</h2>
            <p>
                Supposons que vous voulez que l'utilisateur de votre application choisisse une saison dans la console.
                Vous pourriez avoir le code suivant:
            </p>
            <CodeBlock language="csharp">{enumString}</CodeBlock>
            <p>
                Le code peut bien fonctionner, mais pour l'utilisateur, c'est définitivement énervant. S'il tape une
                erreur dans sa réponse, le programme ne fonctionne pas. Aussi, notre programme n'est pas extensible.
                Par exemple, si nous changer la langue du programme, nous devrons faire du travail inutile. Voici donc
                une option plus intéressante:
            </p>
            <CodeBlock language="csharp">{enumInt}</CodeBlock>
            <p>
                Dans cette nouvelle version, l'utilisateur entre uniquement un chiffre, ce qui est plus facile pour
                lui. De plus, si nous voulons traduire l'application, nous n'avons qu'à changer les chaînes de
                caractères affichées dans la console. Cette solution ajoute toutefois un nouveau problème. Pour un
                programmeur qui lit rapidement le code, les chiffre 0, 1, 2 et 3 ne veulent rien dire et
                sont <IC>hardcodé</IC> dans le programme. Nous pouvons donc encore faire mieux:
            </p>
            <CodeBlock language="csharp">{enumConst}</CodeBlock>
            <p>
                Cette version finale est beaucoup mieux. L'utilisation de constante est très pratique pour rendre le
                code lisible et facilement maintenable si ces valeurs sont utilisées à plusieurs endroits dans votre
                programme.
            </p>
            <p>
                Pouvons-nous toutefois faire encore mieux?
            </p>
        </section>
        <section>
            <h2>Énumération</h2>
            <p>
                Lorsque nous avons une variable qui peut prendre une valeur parmi plusieurs options, nous dirons
                que c'est une énumération. On l'appèle ainsi puisque la valeur existe parmi une énumération d'option.
                Dans notre situation ci-dessus, la valeur de l'utilisateur doit exister parmi une énumération d'option
                qui sont les 4 saisons.
            </p>
            <p>
                En C#, si vous avez ce genre de situation, vous pourrez utiliser un <IC>enum</IC> au lieu de créer des
                constantes pour chaque valeur. Un <IC>enum</IC> est une façon de créer un nouveau type de données, un
                peu comme une classe. Ce type de données pourra toutefois uniquement prendre les valeurs spécifiées
                dans l'énumération. Même <IC>null</IC> n'est pas accepté ici.
            </p>
            <CodeBlock language="csharp">{enum1}</CodeBlock>
            <ColoredBox heading="Attention">
                Un <IC>enum</IC> doit être créé directement dans un <IC>namespace</IC> ou dans une <IC>class</IC>. Si
                vous essayez de l'utiliser dans une méthode ou fonction, votre programme ne compilera pas.
            </ColoredBox>
            <p>
                En dessous, l'énumération utilise des constantes entières, comme nous avions précédement. Il est donc
                même possible de convertir un nombre entier à une valeur de l'énumération avec un transtypage (cast).
            </p>
            <CodeBlock language="csharp">{enumCast}</CodeBlock>
        </section>
        
        <section>
            <h2>Exemple</h2>
            <p>
                Si nous reprenons l'exemple ci-dessus, mais avec un <IC>enum</IC>, on peut avoir le code ci-dessous:
            </p>
            <CodeBlock language="csharp">{enum2}</CodeBlock>
            <p>
                Le code fait exactement la même chose, mais est plus concis avec l'utilisation de l'énumération. Les
                outils d'autocomplétion du code sont aussi bien optimisé pour utiliser les énumérations, ce qui
                simplifie beaucoup notre tâche.
            </p>
        </section>

        <section>
            <h2>Documentation</h2>
            <p>
                Si vous voulez voir plus de fonctionnalité et de possibités avec les énumérations du langage C#, je
                vous recommande fortement d'aller visiter leur page de documentation. Cela vous permettra de voir toutes
                les méthodes disponibles et d'apprendre à mieux les utiliser. Vous pourrez y trouver de nombreuses
                fonctionnalités que nous n'avons pas couvert.
            </p>
            <p>
                <a target="_blank" rel="noopener noreferrer" href="https://docs.microsoft.com/en-us/dotnet/api/system.enum?view=net-5.0">
                    C# Enum Class
                </a>
            </p>
        </section>
    </>
}
