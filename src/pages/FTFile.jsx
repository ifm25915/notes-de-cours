import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

import file from '../resources/file.png';
import file2 from '../resources/file2.png';

const creation = 
`// File d'entiers
Queue<int> file = new Queue<int>();

// File de chaînes de caractères
Queue<string> file2 = new Queue<string>();

// File avec une taille de départ prédéfinie
Queue<int> file3 = new Queue<int>(20);

// File à partir de données existante
// Les données sont enfilées dans l'ordre du tableau
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
Queue<int> file4 = new Queue<int>(tab);`;

const enqueue =
`// File d'entiers
Queue<int> file = new Queue<int>();

// Enfiler des données
file.Enqueue(1);
file.Enqueue(3);
file.Enqueue(3);
file.Enqueue(7);`;

const dequeue =
`// File
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
Queue<int> file = new Queue<int>(tab);

// Défiler des données
// Enlève l'entier 9
file.Dequeue();                 

// Enlève l'entier 0, mais mets sa valeur dans la variable
int valeur = file.Dequeue();
Console.WriteLine(valeur);`;

const peek =
`// File
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
Queue<int> file = new Queue<int>(tab);

// Regarde l'élément à la fin de la file
// Retourne l'entier 9
int valeur = file.Peek();
Console.WriteLine(valeur);`;

const parcourir =
`// File
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
Queue<int> file = new Queue<int>(tab);

// Parcourir la file
foreach(int valeur in file)
{
    Console.Write(valeur + ", ");
}`;

export default function FTFile() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                La file est une structure de données similaire à la liste, mais avec certaines restrictions. On ne
                peut insérer qu'au début de la file et retirer qu'à la fin. Bref, il n'est pas possible d'ajouter ou
                retirer un élément en plein milieu de la file. C'est une structure pratique pour représenter certains
                concepts réels, comme des files d'attentes, ou pour créer des algorithmes plus complexe.
            </p>
            <p>
                On représentera graphiquement la file de la façon suivante:
            </p>
            <img src={file} alt="Représentation d'un tableau" />
            <p>
                Quand on parle du début de la file, on parle de l'entrée de la file et quand on parle de la fin de la
                file, on parle de la sortie. Je sais qu'il est facile de s'y confondre avec une file d'attente ou en
                général quand on parle du début de la file, on parle de sa sortie. Faites attention d'utiliser le bon
                vocabulaire pour que vos collègues vous comprennent bien.
            </p>
            <img src={file2} alt="Représentation d'un tableau" />
            <p>
                Dans une file, on dira que le premier à y entrer est le premier à en sortir puisque si on enfile
                d'autres éléments ils seront tous derrière le premier. Le premier élément à être enfiler est donc le
                premier à se faire défiler de la structure. Vous verrez donc souvent l'accronyme <IC>F.I.F.O.</IC> en
                ligne. Il signifie <IC>First In First Out</IC>, donc premier entré, premier sorti. Vous entendrez
                aussi souvent premier arrivé, premier servi!
            </p>
            <p>
                En C#, une file se crée avec la classe <IC>Queue</IC> de la façon suivante:
            </p>
            <CodeBlock language="csharp">{creation}</CodeBlock>
            <ColoredBox heading="À noter">
                En général, une file est implémenté avec un tableau circulaire (<IC>Circular Array</IC>). Ce concept
                n'est pas nécessaire pour comprendre comment utiliser une file, mais est pratique si vous voulez créer
                votre propre file. Nous ne le verrons donc pas dans ce cours. N'hésitez toutefois pas à faire une
                recherche sur ce concept si ça vous intéresse.
            </ColoredBox>
        </section>

        <section>
            <h2>Avantages</h2>
            <ul>
                <li>Les opérations d'ajout et de retrait sont très efficace.</li>
                <li>Très pratique pour les situations nécessitant un ordre <IC>F.I.F.O.</IC> d'entrée et sortie des données</li>
            </ul>
        </section>

        <section>
            <h2>Inconvénients</h2>
            <ul>
                <li>Pas d'accès aléatoires</li>
                <li>Inutile pour les situations n'ayant pas besoin d'un ordre <IC>F.I.F.O.</IC></li>
            </ul>
        </section>

        <section>
            <h2>Opérations</h2>
            <dl>
                <dt>Accès aléatoire</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>N/A</IC>
                    </p>
                    <p>
                        On ne peut pas faire d'accès aléatoire dans une file. Il faut la convertir dans une autre
                        structure de données pour pouvoir le faire, ce qui n'est pas efficace.
                    </p>
                </dd>

                <dt>Enfiler - Enqueue</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(1)</IC>
                    </p>
                    <p>
                        Permet d'ajouter un élément dans la file. Essentiellement, un élément sera ajouté au début de
                        la file. L'ajout dans une file est toujours très efficace.
                    </p>
                    <CodeBlock language="csharp">{enqueue}</CodeBlock>
                </dd>

                <dt>Défiler - Dequeue</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(1)</IC>
                    </p>
                    <p>
                        Permet de défiler l'élément à la fin de la file. Essentiellement, l'élément à la fin de
                        la file est retiré. Le retrait dans une file est toujours très efficace.
                    </p>
                    <p>
                        Lorsqu'on défile un élément de la file, cet élément est retourné pour qu'on puisse l'utiliser
                        dans notre traitement.
                    </p>
                    <CodeBlock language="csharp">{dequeue}</CodeBlock>
                </dd>

                <dt>Voir la fin de la file - Peek</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(1)</IC>
                    </p>
                    <p>
                        Le seul élément que l'on peut accéder aléatoirement dans une file est celui sur qui est à la
                        fin de la file, donc celui qui est à la sortie de la file. L'avantage de cette opération est
                        qu'elle nous permet de voir ce qu'il y a à la fin de la file, mais sans avoir à défiler cet
                        élément.
                    </p>
                    <CodeBlock language="csharp">{peek}</CodeBlock>
                </dd>

                <dt>Parcourir</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(n)</IC>
                    </p>
                    <p>
                        Puisqu'il n'y a pas d'accès aléatoire dans une file, on peut uniquement la parcourir à l'aide
                        d'une boucle à itérateur. La boucle <IC>foreach</IC> est donc notre ami ici:
                    </p>
                    <CodeBlock language="csharp">{parcourir}</CodeBlock>
                    <ColoredBox heading="Attention">
                        Si vous testez le parcours d'une file, vous remarquerez que les éléments sont parcouru en
                        ordre inverse, soit de l'élément à la sortie de la file vers l'élément à l'entrée de la file.
                        Cela peut être un peu bizarre, mais c'est l'ordre d'insertion dans la file, donc faites
                        attention!
                    </ColoredBox>
                </dd>
            </dl>
        </section>

        <section>
            <h2>Documentation</h2>
            <p>
                Si vous voulez voir plus de fonctionnalité et de possibités avec la file du langage C#, je
                vous recommande fortement d'aller visiter sa page de documentation. Cela vous permettra de voir toutes
                les méthodes disponibles et d'apprendre à mieux les utiliser. Vous pourrez y trouver de nombreuses
                fonctionnalités que nous n'avons pas encore couvert.
            </p>
            <p>
                <a target="_blank" rel="noopener noreferrer" href="https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.queue-1?view=net-5.0">
                    C# Queue&lt;T&gt; Class
                </a>
            </p>
        </section>
    </>
}
