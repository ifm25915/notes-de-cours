import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const stringCode =
`namespace MesStructures
{
    public class String
    {
        // Tableau de caractère interne
        private char[] data;

        // Méthodes utilitaires
        public String Substring() { ... }
        public String[] Split() { ... }
        public bool StartsWith() { ... }
        public String ToUpperCase() { ... }
    }
}`;

export default function ConstructionStructure() {
    return <>
        <section>
            <h2>Blocs de construction</h2>
            <p>
                Dans le module précédant, nous avons vu comment utiliser les tableaux. Ils sont une structure de
                données qui existe dans tous les langages de programmation (que je sache). Comme vu, ils sont
                pratique, mais manquent légèrement de flexibité. Entre autre, le fait qu'on ne puisse pas facilement
                y ajouter ou retirer des éléments peut nous causer des problèmes. Les langages de programmation
                utilisent donc d'autres structures de données qu'ils construiront généralement dans des classes ou
                des objets.
            </p>
            <p>
                Ces classes/objets/structures de structure de données sont en fait codé à partir de blocs déjà
                existant dans le langage de programmation. Ils utiliseront d'ailleurs souvent des tableaux pour se
                construire. Le tableau est donc aussi utilisé comme bloc de construction pour bâtir des structures
                de données plus complexes. Avec une bonne compréhension de comment ces structures plus complexes
                fonctionnent, nous pourrions d'ailleurs les créer nous-même. Ce n'est toutefois pas le but de ce
                cours.
            </p>
        </section>
        <section>
            <h2>La chaîne de caractère</h2>
            <p>
                Un exemple simple de structure de données utilisant un tableau est la chaîne de caractères, appelée
                souvent <IC>String</IC>. Dans un langage de programmation, la chaîne de caractère est une structure
                de données contenant un ensemble de caractères dans un certain ordre. L'idée de base pour construire
                cette structure de données est donc simplement de faire une classe qui contient un tableau de
                caractères comme variable. Nous ajouterons aussi dans cette classe un paquet de méthodes et fonctions
                utilitaire pratique, telle que des fonctions <IC>Substring()</IC>, <IC>Split()</IC>, <IC>StartsWith()</IC>, <IC>ToUpperCase()</IC>, etc...
            </p>
            <CodeBlock language="csharp">{stringCode}</CodeBlock>
            <p>
                Créer nos propres structure de données est un exercice intéressant pour se pratiquer à programmer.
                Nous ne le ferons toutefois pas dans ce cours, surtout que toutes les structures que nous verrons
                existent déjà dans le langage C#. La classe <IC>String</IC> du langage C# en est un bon exemple. Elle
                existe déjà, donc pas besoin de la coder de nouveau.
            </p>
            <p>
                Les structures venant du langage de programmation sont aussi très optimisées. Vous pouvez
                d'ailleurs aller voir le code de la classe <IC>String</IC> du C# en ligne. Même si le concept est le
                même que ce qui est expliqué ci-dessus, vous verrez que le code optimisé n'est pas très facile à
                comprendre. C'est pourquoi nous nous n'y attarderons pas.
            </p>
            <p>
                <a target="_blank" rel="noopener noreferrer" href="https://github.com/microsoft/referencesource/blob/master/mscorlib/system/string.cs">
                    C# String Class
                </a>
            </p>
        </section>
        <section>
            <h2>Structures en C#</h2>
            <p>
                Dans ce module, nous verrons 2 structures qui sont construites à l'aide de tableaux, soit le vecteur
                et la liste. Comme mentionné ci-dessus, le but n'est pas de créer nous-même ces structures, mais
                plutôt de comprendre un peu comment elles fonctionnent et d'être capable de les utiliser lorsque
                nécessaire.
            </p>
            <p>
                En C#, toutes les structures de base que nous verrons dans le cours sont disponible dans une librairie
                interne différente. Nous devrons donc utiliser un <IC>using</IC> pour aller chercher nos structures de
                données. Bref, dans vos programmes, assurez-vous d'avoir le <IC>using</IC> suivant pour que vous
                puissiez utiliser les structures de données plus complexe:
            </p>
            <CodeBlock language="csharp">{'using System.Collections.Generic;'}</CodeBlock>
        </section>
    </>
}
