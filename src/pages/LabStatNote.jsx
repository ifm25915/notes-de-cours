import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import DownloadBlock from '../component/DownloadBlock';

import distribue from '../resources/laboratoire-statnote-distribué.zip';
import solution from '../resources/laboratoire-statnote-solution.zip';

export default function LabStatNote() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                Vous voulez aider votre professeur à faire des calculs statistiques sur les notes de ses étudiants.
                Vous faites donc un petit programme qui demande à l'utilisateur d'entrer toutes les notes en ordre
                croissant et qui calculera quelques statistiques sur ces données. Pour réussir ce laboratoire, suivez
                les étapes suivantes:
            </p>
            <ol>
                <li>Télécharger le fichier <IC>distribué.zip</IC>.</li>
                <li>Dézipper le fichier <IC>distribué.zip</IC>.</li>
                <li>
                    Dans le constructeur <IC>StatNote</IC>, à l'endroit indiqué, écrire le code permettant d'ajouter
                    la <IC>note</IC> à la fin du vecteur <IC>notesDuGroupe</IC>.
                </li>
                <li>
                    Programmer la méthode <IC>ValiderNote</IC>.
                    <ul>
                        <li>Doit valider que la note en paramètre est entre 0 et 100 inclusivement.</li>
                        <li>Doit valider que la note est plus grande ou égale à la dernière note entrée.</li>
                        <li>Retourne <IC>true</IC> si les 2 validations sont bonne, <IC>false</IC> autrement.</li>
                    </ul>
                </li>
                <li>
                    Programmer la méthode <IC>AfficherVecteur</IC> pour qu'elle affiche le
                    vecteur <IC>notesDuGroupe</IC> sous le format suivant dans la console:
                    <CodeBlock language="shell">{ '[ 4, 13, 54, 78, 89, 97 ]' }</CodeBlock>
                </li>
                <li>
                    Programmer la méthode <IC>Moyenne</IC> qui doit calculer la moyenne du
                    vecteur <IC>notesDuGroupe</IC> et la retourner.
                </li>
                <li>
                    Programmer la méthode <IC>Mediane</IC> qui doit calculer la médiane du
                    vecteur <IC>notesDuGroupe</IC> et la retourner.
                    <ul>
                        <li>
                            La médiane se calcule uniquement sur des données triées en ordre croissant, ce qui
                            devrait déjà être la cas si votre validation fonctionne.
                        </li>
                        <li>
                            Lorsque le vecteur contient un nombre impair de valeurs, la médiane est la valeur du
                            milieu du vecteur.
                        </li>
                        <li>
                            Lorsque le vecteur contient un nombre pair de valeurs, la médiane est la moyenne des 2
                            valeurs du milieu du vecteur.
                        </li>
                    </ul>
                </li>
                <li>
                    Programmer la méthode <IC>Mode</IC> qui doit calculer le mode du vecteur <IC>notesDuGroupe</IC> et
                    le retourner.
                    <ul>
                        <li>Le mode est la valeur qui apparaît le plus souvent dans le vecteur.</li>
                        <li>
                            Pour calculer le mode, vous devez trouver le nombre d'occurences de chaque valeur dans le
                            vecteur.
                        </li>
                        <li>
                            Puisque les données sont triées, il existe une façon plus efficace de trouver le mode. Si
                            vous avez le temps, essayez de la trouver!
                        </li>
                    </ul>
                </li>
            </ol>
        </section>

        <section>
            <h2>Distribué et solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ distribue } name="distribué.zip"></DownloadBlock.File>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
