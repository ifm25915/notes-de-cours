import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

import ensemble from '../resources/ensemble.png';
import union from '../resources/union.png';
import intersection from '../resources/intersection.png';
import diffsym from '../resources/diffsym.png';

const creation = 
`// Ensemble d'entiers
HashSet<int> ensemble = new HashSet<int>();

// Ensemble de chaînes de caractères
HashSet<string> ensemble2 = new HashSet<string>();

// Ensemble avec une taille de départ prédéfinie
HashSet<int> ensemble3 = new HashSet<int>(20);

// Ensemble à partir de données existante
// S'il y a des répétitions dans le tableau, 
// elles sont retirées dans l'ennsemble
int[] tab = new int[10] { 9, 0, 8, 1, 8, 2, 0, 3, 8, 4 };
HashSet<int> ensemble4 = new HashSet<int>(tab);

// Affiche le nombre "7" et non "10" puisque les
// doublons sont retirés
Console.WriteLine(ensemble4.Count); `;

const add =
`// Ensemble
HashSet<int> ensemble = new HashSet<int>();

// Ajouter des données
ensemble.Add(1);
ensemble.Add(3);
ensemble.Add(7);

// Si on ajoute un élément existant 
// dans l'ensemble, il n'est pas ajouté
// et la méthode Add() retourne "false"
bool aReussi = ensemble.Add(3);

// Affiche False
Console.WriteLine(aReussi);`;

const remove =
`// Ensemble
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
HashSet<int> ensemble = new HashSet<int>(tab);

// Retirer un élément
ensemble.Remove(7);

// Si on retire un élément qui n'existe 
// pas dans l'ensemble, la méthode 
// Remove() retourne "false"
bool aReussi = ensemble.Remove(42);

// Affiche False
Console.WriteLine(aReussi);`;

const contain =
`// Ensemble
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
HashSet<int> ensemble = new HashSet<int>(tab);

// Rechercher un élément
// Affiche True
bool estDedans1 = ensemble.Contains(7);
Console.WriteLine(estDedans1);

// Affiche False
bool estDedans2 = ensemble.Contains(-40);
Console.WriteLine(estDedans2);`;

const unionCode =
`// Ensemble 1
int[] tab1 = new int[] { 1, 3, 5, 7, 9 };
HashSet<int> ensemble1 = new HashSet<int>(tab1);

// Ensemble 2
int[] tab2 = new int[] { 0, 2, 4, 6, 8 };
HashSet<int> ensemble2 = new HashSet<int>(tab2);

// Union de l'ensemble 2 dans l'ensemble 1
// Toutes les données de l'ensemble 2 vont
// s'ajouter à l'ensemble 1
// L'ensemble 1 contient { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }
// L'ensemble 2 est inchangé
ensemble1.UnionWith(ensemble2);`;

const intersectionCode =
`// Ensemble 1
int[] tab1 = new int[] { 0, 1, 2, 3, 4, 5, 6 };
HashSet<int> ensemble1 = new HashSet<int>(tab1);

// Ensemble 2
int[] tab2 = new int[] { 3, 4, 5, 6, 7, 8, 9 };
HashSet<int> ensemble2 = new HashSet<int>(tab2);

// Intersection de l'ensemble 1 et 2 
// L'ensemble résultant sera mis dans 
// L'ensemble 1 contient { 3, 4, 5, 6 }
// L'ensemble 2 est inchangé
ensemble1.IntersectWith(ensemble2);`;

const diffsymCode =
`// Ensemble 1
int[] tab1 = new int[] { 0, 1, 2, 3, 4, 5, 6 };
HashSet<int> ensemble1 = new HashSet<int>(tab1);

// Ensemble 2
int[] tab2 = new int[] { 3, 4, 5, 6, 7, 8, 9 };
HashSet<int> ensemble2 = new HashSet<int>(tab2);

// Intersection de l'ensemble 1 et 2 
// L'ensemble résultant sera mis dans 
// L'ensemble 1 contient { 0, 1, 2, 7, 8, 9 }
// L'ensemble 2 est inchangé
ensemble1.SymmetricExceptWith(ensemble2);`;

const parcourir =
`// Ensemble
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
HashSet<int> ensemble = new HashSet<int>(tab);

// Parcourir l'ensemble
foreach(int element in ensemble)
{
    Console.Write(element + " ");
}`;

export default function FTEnsemble() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                L'ensemble est une structure de données basée sur un concept mathématique que l'on appèle la théorie
                des ensembles. La théorie des ensembles est l'une des notions bases et un des fondements des
                mathématiques. Dans celle-ci, on défini un ensemble comme un groupe d'éléments, ce qui sera notre cas
                aussi en programmation. En informatique, la particularité de l'ensemble est que chaque élément qui se
                à l'intérieur doit être unique. Il est donc impossible d'ajouter 2 fois le même élément dans le même
                ensemble.
            </p>
            <p>
                On représentera graphiquement l'ensemble de la façon suivante:
            </p>
            <img src={ensemble} alt="Représentation d'un ensemble" />
            <p>
                Venant des mathématiques, l'ensemble possède quelques fonctionnalités supplémentaires comme l'union et
                l'intersection.
                <dl>
                    <dt>Union</dt>
                    <dd>
                        <p>C'est la fusion de 2 ensembles. S'il ya a des répétitions, celles-ci sont ignoré.</p>
                        <img src={union} alt="Représentation de l'union d'ensemble" />
                    </dd>
                    <dt>Intersection</dt>
                    <dd>
                        <p>C'est l'ensemble des éléments qui se retrouve dans 2 ensembles.</p>
                        <img src={intersection} alt="Représentation de l'intersection d'ensemble" />
                    </dd>
                    <dt>Différence symétrique</dt>
                    <dd>
                        <p>C'est l'ensemble des éléments de 2 ensembles, mais qui ne sont pas dans l'intersection.</p>
                        <img src={diffsym} alt="Représentation de la différence symétrique d'ensemble" />
                    </dd>
                </dl>
            </p>
            <p>
                En C#, un ensemble de base s'utilise avec la classe <IC>HashSet</IC>. En anglais, un <IC>Set</IC> est un
                ensemble. Le mot <IC>Hash</IC>, quant à lui indique que l'implémentation de l'ensemble se fait par
                hachage, concept que nous ne verrons pas dans ce cours. Vous pouvez créer un <IC>HashSet</IC> de la
                façon suivante:
            </p>
            <CodeBlock language="csharp">{ creation }</CodeBlock>
        </section>

        <section>
            <h2>Avantages</h2>
            <ul>
                <li>Les opérations d'ajout et de retrait sont très efficace.</li>
                <li>La recherche d'un élément dans un ensemble est très efficace.</li>
                <li>Possède des opérations mathématiques spéciales.</li>
            </ul>
        </section>

        <section>
            <h2>Inconvénients</h2>
            <ul>
                <li>Pas d'accès aléatoires.</li>
                <li>Pas de répétition de données.</li>
                <li>L'ordre des données n'est pas connu.</li>
            </ul>
        </section>

        <section>
            <h2>Opérations</h2>
            <dl>
                <dt>Accès aléatoire</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>N/A</IC>
                    </p>
                    <p>
                        On ne peut pas faire d'accès aléatoire dans un ensemble. Il faut le convertir dans une autre
                        structure de données pour pouvoir le faire, ce qui n'est pas efficace.
                    </p>
                </dd>

                <dt>Ajouter</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(1)</IC>
                    </p>
                    <p>
                        Permet d'ajouter un élément dans l'ensemble. L'ajout dans un ensemble est toujours très efficace.
                    </p>
                    <p>
                        La méthode <IC>Add</IC> va retourner une valeur booléenne indiquant si la valeur que l'on essait
                        d'ajouter existe déjà dans l'ensemble ou non. Si la valeur n'existe pas dans l'ensemble, la
                        méthode <IC>Add</IC> va retourner <IC>true</IC>. Si la valeur existe déjà dans l'ensemble, la
                        méthode va retourner <IC>false</IC> et n'ajoutera rien dans l'ensemble puisqu'il ne peut pas y
                        avoir de répétition.
                    </p>
                    <CodeBlock language="csharp">{add}</CodeBlock>
                </dd>

                <dt>Retirer</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(1)</IC>
                    </p>
                    <p>
                        Permet de retirer un élément dans l'ensemble. Le retrait dans un ensemble est toujours très efficace.
                    </p>
                    <p>
                        La méthode <IC>Remove</IC> va retourner une valeur booléenne indiquant si la valeur que l'on essait
                        de retirer existe déjà dans l'ensemble ou non. Si la valeur n'existe pas dans l'ensemble, la
                        méthode <IC>Remove</IC> va retourner <IC>false</IC> et ne retirera rien dans l'ensemble. Si la valeur
                        existe déjà dans l'ensemble, la méthode va retourner <IC>true</IC>.
                    </p>
                    <CodeBlock language="csharp">{remove}</CodeBlock>
                </dd>

                <dt>Recherche</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(1)</IC>
                    </p>
                    <p>
                        Dans un ensemble, la recherche correspond à savoir si un élément est dans l'ensemble ou non.
                        La recherche dans un ensemble est toujours très efficace.
                    </p>
                    <CodeBlock language="csharp">{contain}</CodeBlock>
                </dd>

                <dt>Union</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(n)</IC>
                    </p>
                    <p>
                        L'union est la fusion de 2 ensembles. L'efficacité de l'union dépends dans la quantité de
                        données que nous essayons d'ajouter dans notre ensemble.
                    </p>
                    <CodeBlock language="csharp">{unionCode}</CodeBlock>
                </dd>

                <dt>Intersection</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(n)</IC>
                    </p>
                    <p>
                        L'intersection est une façon de faire un nouvel ensemble avec les éléments ayant la même
                        valeur dans les 2 ensembles. L'efficacité de l'intersection dépends dans la quantité de
                        données que nous avons dans le premier ensemble.
                    </p>
                    <CodeBlock language="csharp">{intersectionCode}</CodeBlock>
                </dd>

                <dt>Différence symétrique</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(n)</IC>
                    </p>
                    <p>
                        La différence symétrique est une façon de faire un nouvel ensemble avec les éléments qui sont
                        différents dans les 2 ensembles. L'efficacité de la différence symétrique dépends dans la
                        quantité de données que nous avons dans le deuxième ensemble.
                    </p>
                    <CodeBlock language="csharp">{diffsymCode}</CodeBlock>
                </dd>

                <dt>Parcourir</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(n)</IC>
                    </p>
                    <p>
                        Puisqu'il n'y a pas d'accès aléatoire dans un ensemble, on peut uniquement le parcourir à l'aide
                        d'une boucle à itérateur. La boucle <IC>foreach</IC> est donc importante ici:
                    </p>
                    <CodeBlock language="csharp">{parcourir}</CodeBlock>
                    <ColoredBox heading="Attention">
                        Une des particularités du parcour d'un ensemble de base est qu'il ne préserve pas un ordre des
                        éléments. Bien que l'exécution du code ci-dessus semble montré que les éléments reste dans le
                        même ordre, l'ajout ou le retrait de beaucoup de données variées peut changer l'ordre sans
                        qu'il soit facile de le prédire.
                    </ColoredBox>
                </dd>
            </dl>
        </section>

        <section>
            <h2>Documentation</h2>
            <p>
                Si vous voulez voir plus de fonctionnalité et de possibités avec l'ensemble par hachage du langage C#,
                je vous recommande fortement d'aller visiter sa page de documentation. Cela vous permettra de voir
                toutes les méthodes disponibles et d'apprendre à mieux les utiliser. Vous pourrez y trouver de
                nombreuses fonctionnalités que nous n'avons pas couvert.
            </p>
            <p>
                <a target="_blank" rel="noopener noreferrer" href="https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.hashset-1?view=net-5.0">
                    C# HashSet&lt;T&gt; Class
                </a>
            </p>
        </section>
    </>
}
