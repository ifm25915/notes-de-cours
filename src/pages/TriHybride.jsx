import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const introsort =
`introsort(tab)
    n = taille de tab
    profondeurMax = floor(log(n)) * 2
    introsortAuxiliere(tab, 0, n-1, profondeurMax)
    
introsortAuxiliere(tab, debut, fin, profondeurMax)
    if n < 32
        triInsertion(tab, debut, fin)
    else if profondeurMax = 0
        triParTas(tab, debut, fin)
    else
        indexPivot = choisirPivot(tab, debut, fin)
        if(indexPivot ≥ 0)
            i = partitionner(tab, debut, fin, tab[indexPivot])
            introsortAuxiliere(tab, debut, i-1, profondeurMax-1)
            introsortAuxiliere(tab, i, fin, profondeurMax-1)`;

export default function TriRapide() {
    return <>
        <section>
            <h2>Optimisation des algorithmes</h2>
            <p>
                Comme mentionné à quelques reprises jusqu'à présent, certains algorithmes de tri sont plus efficaces
                dépendant des conditions que d'autres. Le tri rapide est l'un des tris les plus efficace dans tous les
                cas, mais ne serait-il pas possible d'utiliser le tri rapide uniquement lorsqu'il est le plus efficace
                et d'autres tris pour les autres cas? La réponse est oui.
            </p>
            <p>
                Dans ce document, nous verrons quelques algorithmes de tri qui utilisent plusieurs autres algorithmes
                pour fonctionner. Ces algorithmes sont nommés des tris hybrides.
            </p>
        </section>

        <section>
            <h2>Timsort</h2>
            <p>
                Le Timsort est un algorithme de tri hybride nommé au nom de son créateur, Tim Peters. Le but de cet
                algorithme était d'optimiser le triage sur les données réelles. Tim Peters a créé cet algorithme pour
                le langage de programmation Python qui est fréquement utilisé pour la manipulation massive de données.
                Il est toutefois aussi utilisé par le langage de programmation Java. Il s'exécute en pire cas
                en <IC>O(n&nbsp;log&nbsp;n)</IC>.
            </p>
            <p>
                Le Timsort utilise un dérivé du tri fusion (<IC>mergesort</IC>) et le tri par insertion.
                Essentiellement, il utilise le tri fusion jusqu'à ce que les divisions de tableaux soient plus petites
                et utilise ensuite le tri par inserions pour trier les petits sous-tableaux.
            </p>
            <p>
                L'algorithme est assez complexe, surtout puisqu'il utilise une version modifié du tri fusion. Nous ne
                verrons donc pas son pseudo-code. Vous pouvez toutefois trouver l'implémentation du Timsort dans la
                OpenJDK de Java ici:
            </p>
            <p>
                <a target="_blank" rel="noopener noreferrer" href="https://github.com/openjdk/jdk/blob/master/src/java.base/share/classes/java/util/TimSort.java">
                    TimSort.java
                </a>
            </p>
        </section>

        <section>
            <h2>Introsort</h2>
            <p>
                Le Introsort, aussi nommé tri instrospectif, est un algorithme de tri hybride. Il a été créé par David
                Musser dans le but d'offrir un algorithme performant et générique pour le langage C++. Le langage de
                programmation C# utilise aussi cet algorithme de tri par défaut. Il s'exécute en pire cas
                en <IC>O(n&nbsp;log&nbsp;n)</IC>, se qui en fait l'un des meilleurs algorihtmes de tri.
            </p>
            <p>
                Le Introsort utilise 3 algorithmes de tri différents, soit le tri rapide, le tri par insertion et
                finalement le tri par tas (<IC>Heapsort</IC>). L'algorithme fonctionne essentiellement en faisant un
                tri rapide, mais si les sous-tableaux deviennent plus petit qu'une certaine taille, on utilise le tri
                par insertion. De plus pour éviter d'utiliser trop de mémoire avec la récursivité, si la profondeur
                d'exécution devient trop importante, le Introsort utilisera le tri par tas.
            </p>
            <p>
                L'algorithme peut être complexe, mais se résume au pseudo-code suivant:
            </p>
            <CodeBlock language="pseudo">{introsort}</CodeBlock>
            <p>
                Il existe plusieurs variantes de cet algorithme. Par exemple, certaines versions changent la valeur de
                la profondeur maximale de la récursivité ou de la taille minimale pour utiliser le tri par insertion.
                Certaines version utilisent même le tri Shell au lieu du tri par insertion.
            </p>
        </section>
    </>;
}
