import React from 'react';
import IC from '../component/InlineCode';
import DownloadBlock from '../component/DownloadBlock';

import distribue from '../resources/laboratoire-hanoi-distribué.zip';
import solution from '../resources/laboratoire-hanoi-solution.zip';

export default function LabDemineur() {
    return <>
        <section>
            <h2>Mise en situation</h2>
            <p>
                La tour de Hanoï est un problème récursif connu. Bien que ce problème soit un bon exemple de
                récursivité, il n'est vraiment pas efficace. Ce laboratoire consiste modifier un programme résolvant
                le problème de la tour de Hanoï pour calculer certaines statistique et voir pourquoi le problème de
                la tour de Hanoï se résout de façon inefficace.
            </p>
        </section>

        <section>
            <h2>Marche à suivre</h2>
            <p>
                Ce programme contient déjà une version fonctionnelle de l'algorithme de la tour de Hanoï. Dans ce
                programme, chaque tige est une pile d'anneaux qui sont représenté par un nombre entier. Le nombre
                entier indique la taille de l'anneau, 1 étant le plus petit anneau.
            </p>
            <p>
                Vous devez modifier le programme pour calculer 2 statistiques.
            </p>
            <ol>
                <li>Le nombre d'exécutions totales de la fonction récursive <IC>Resoudre</IC>.</li>
                <li>Le nombre d'exécution de chaque opération.</li>
            </ol>
            <p>
                Bien qu'il soit facile de calculer le nombre total d'exécution, il n'en est pas de même pour le
                nombre d'exécution de chaque opération. Voici la marche à suivre pour le faire:
            </p>
            <ol>
                <li>Créez-vous un dictionnaire de chaîne de caractère comme clé et de nombre entier comme valeur.</li>
                <li>
                    Pour chaque exécution de <IC>Resoudre</IC>, déterminez l'opération qui est faite. L'opération se
                    résume au nombre d'anneaux à transférer et de quel tige à quel tige vous voulez faire ce
                    transfert. Construisez vous une chaîne de caractères représentant cette opération. Par
                    exemple, <IC>5 A-&gt;B</IC>, ce qui voudrait dire: déplacer 5 anneaux de la tige A à la tige B.
                    Pour trouver le nom d'une tige, vous pouvez la passer en paramètre à la
                    fonction <IC>GetNomTige</IC>.
                </li>
                <li>
                    Utilisez la chaîne de caractère représentant l'opération comme clé dans le dictionnaire
                    précédemment créé. Si la clé n'existe pas dans le dictionnaire, vous l'ajoutez avec une valeur de
                    1. Si la clé existe déjà dans le dictionnaire, vous incrémentez la valeur. Ainsi, à la fin du
                    programme le dictionnaire contiendra le nombre d'exécution totale par opération.
                </li>
            </ol>
            <p>
                N'oubliez pas d'afficher vos résultats dans la console à la fin du programme pour pouvoir les voir et
                les comparer.
            </p>
        </section>

        <section>
            <h2>Distribué et solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ distribue } name="distribué.zip"></DownloadBlock.File>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
