import React from 'react';
import IC from '../component/InlineCode';
import DownloadBlock from '../component/DownloadBlock';
import Video from '../component/Video';

import distribue from '../resources/laboratoire-index-distribué.zip';
import solution from '../resources/laboratoire-index-solution.zip';

export default function LabIndex() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                Dans une base de données, l'utilisation d'index permet généralement d'accélérer la recherche des
                données sur certains champs. Un index de base de donnée fonctionne d'une façon très similaire à un
                dictionnaire. En effet, un index est simplement une structure clé/valeur ayant le champs sur lequel
                on veut accélérer la recherche comme clé et un pointeur indiquant où en mémoire se trouve les données
                comme valeur.
            </p>
            <p>
                Dans ce laboratoire, nous créerons des index pour un tableau de données de films. Il n'y aura donc pas
                de base de données, mais nous utiliserons des concepts similaires. Le laboratoire chronomètre les
                performances des recherches pour que vous puissiez voir à quel point la recherche dans un dictionnaire
                est efficace.
            </p>
            <p>
                Compléter le projet distribué ci-dessous:
            </p>
            <ol>
                <li>Télécharger le fichier <IC>distribué.zip</IC>.</li>
                <li>Dézipper le fichier <IC>distribué.zip</IC>.</li>
                <li>
                    Programmer la méthode <IC>ConstruireIndex</IC> dans le fichier <IC>Index.cs</IC>.
                    <ul>
                        <li>
                            Cette fonction doit remplir le dictionnaire <IC>indexID</IC> avec chaque film se trouvant dans le
                            tableau <IC>films</IC>. La clé est l'ID du film et la valeur est le film lui-même.
                        </li>
                        <li>
                            Cette fonction doit aussi remplir le dictionnaire <IC>indexTitre</IC> avec chaque film se trouvant
                            dans le tableau <IC>films</IC>. Il faut faire attention puisque plusieurs films possèdent le même
                            titre. Donc la clé est le titre du film et la valeur est un vecteur contenant tous les films ayant
                            le même titre.
                        </li>
                    </ul>
                </li>
                <li>
                    Programmer la méthode <IC>RechercheParIdTableau</IC> dans le fichier <IC>Index.cs</IC>.
                    <ul>
                        <li>
                            Cette fonction doit trouver le film ayant le <IC>ID</IC> spécifié en paramètre en utilisant
                            uniquement le tableau de <IC>films</IC>. Aucun dictionnaire ne peut être utilisé ici.
                        </li>
                    </ul>
                </li>
                <li>
                    Programmer la méthode <IC>RechercheParIdIndex</IC> dans le fichier <IC>Index.cs</IC>.
                    <ul>
                        <li>
                            Cette fonction doit trouver le film ayant le <IC>ID</IC> spécifié en paramètre en utilisant
                            uniquement le dictionnaire <IC>indexID</IC>. Il ne faut pas utilisé la tableau
                            de <IC>films</IC> ici.
                        </li>
                    </ul>
                </li>
                <li>
                    Programmer la méthode <IC>TitrePlusUtilise</IC> dans le fichier <IC>Index.cs</IC>.
                    <ul>
                        <li>
                            Cette fonction doit trouver et retourner le titre de film qui est utilisé pour le plus de
                            films. Je vous suggère fortement d'utiliser le dictionnaire <IC>indexTitre</IC> pour
                            trouver votre réponse le plus efficacement possible.
                        </li>
                    </ul>
                </li>
            </ol>
        </section>

        <section>
            <h2>Vidéo</h2>
            <Video title="Laboratoire - Indexation des données" src="https://www.youtube.com/embed/ywNjt5xhT8w" />
        </section>

        <section>
            <h2>Distribué et solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ distribue } name="distribué.zip"></DownloadBlock.File>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
