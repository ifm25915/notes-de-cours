import React from 'react';
import IC from '../component/InlineCode';
import DownloadBlock from '../component/DownloadBlock';

import distribue from '../resources/laboratoire-burgersimulator-distribué.zip';
import solution from '../resources/laboratoire-burgersimulator-solution.zip';

/*
    TODO: Peut-être mettre du pseudo-code ici pour aider.
*/

export default function LabBurgerSimulator() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                Nous faisons la simulation simple d'un casse-croûte qui vend des hamburger. Il y a un seul employé
                dans le casse-croûte et il doit préparer les commandes qu'il reçoit. Chaque commande prends un certain
                temps à compléter et il peut recevoir d'autres commandes pendant la préparation d'une autre. S'il
                reçoit plusieurs commandes, celles-ci sont placées dans une file d'attente premier arrivé, premier
                servi.
            </p>
            <p>
                Compléter le projet distribué ci-dessous:
            </p>
            <ol>
                <li>Télécharger le fichier <IC>distribué.zip</IC>.</li>
                <li>Dézipper le fichier <IC>distribué.zip</IC>.</li>
                <li>
                    Écrire le code de la méthode <IC>TickSimulation</IC> dans le fichier <IC>BurgerSimulator.cs</IC>.
                    Cette méthode exécute une itération de la simulation du casse-croûte de hamburger. Par défaut,
                    cette méthode s'exécute à chaque 500 millisecondes. La fonction doit exécuter les étapes
                    ci-dessous dans l'ordre:
                    <ol>
                        <li>
                            8% du temps, une nouvelle commande aléatoire arrive dans la file d'attente. Générer un
                            nombre aléatoire pour qu'à 8% des exécution, cette méthode ajoute une commande dans la
                            file d'attente. Pour créer une commande, vous pouvez simplement faire
                            une <IC>new Commande()</IC> puisque le constructeur génère automatiquement la commande
                            aléatoirement.
                        </li>
                        <li>
                            Si la commande courante (<IC>commandeCourante</IC>) est fini, on la mets à <IC>null</IC>.
                            Pour vérifier si une commande est fini, exécutez sa fonction <IC>EstFini</IC> avec le
                            temps courant de la simulation. Assurez-vous qu'il y a une commande courante avant de
                            faire cette étape.
                        </li>
                        <li>
                            S'il n'y a pas une commande courante, prenez la prochaine dans la file d'attente et
                            mettez la dans la <IC>commandeCourante</IC>. Débuter la préparation de cette commande en
                            exécutant la méthode <IC>DebuterPreparation</IC> avec le temps courant de la simulation.
                            Assurez-vous qu'il y a une commande dans la file avant de faire cette étape.
                        </li>
                        <li>
                            Afficher la simulation en exécutant la méthode <IC>AffichageSimulation</IC>.
                        </li>
                    </ol>
                </li>
            </ol>
        </section>

        <section>
            <h2>Distribué et solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ distribue } name="distribué.zip"></DownloadBlock.File>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
