import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

import tableaumulti from '../resources/tableaumulti.png';

const tableau2D = 
`// Tableau multidimensionnel
int[,] tab2D = new int[3, 5] {
    {  1,  2,  3,  4,  5 },
    {  6,  7,  8,  9, 10 },
    { 11, 12, 13, 14, 15 }
};

int[,,] tab3D = new int[2, 4, 3];`;

const parcourir =
`// La fonction GetLength permet d'aller chercher la
// taille d'une dimension spécifique
for (int i = 0; i < tab2D.GetLength(0); i++){
    for (int j = 0; j < tab2D.GetLength(1); j++){
        Console.Write(tab2D[i, j] + ",");
    }
    Console.WriteLine();
}`;

const parcourirIterateur =
`foreach(int valeur in tab2D){
    Console.Write(valeur + ",");
}`;


export default function FTTableauMulti() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                Le tableau multidimensionnel existe par le fait qu'on peut généralement mettre un tableau dans un
                autre tableau. Puisqu'il fonctionne avec des tableaux il partage donc leurs points forts et leurs
                faiblesses. Un tableau multidimensionnel peut être à 2 dimensions, à 3 dimensions ou même plus! Il
                est toutefois très rare de voir un tableau de plus de 3 dimensions, entre autre puisqu'ils sont
                difficile à représenter graphiquement. 
            </p>
            <p>
                On représentera graphiquement un tableau 2D de la façon suivante:
            </p>
            <img src={ tableaumulti } alt="Représentation d'un tableau" />
            <p>
                En C#, on créera un tableau multidimensionnel de la façon suivante:
            </p>
            <CodeBlock language="csharp">{ tableau2D }</CodeBlock>
        </section>

        <section>
            <h2>Avantages</h2>
            <ul>
                <li>Simple à utiliser et ne nécessite généralement pas d'importation de librairie</li>
                <li>Très performant pour l'accès aléatoire</li>
            </ul>
        </section>

        <section>
            <h2>Inconvénients</h2>
            <ul>
                <li>La taille est fixe, on ne peut donc pas ajouté ou enlever de l'espace dedans</li>
                <li>Pour faire des recherches, on doit généralement parcourir toutes les données</li>
                <li>
                    Il peut être difficile de créer des tableaux de taille gigantesque puisque les éléments du tableau
                    sont placé un à côté de l'autre dans la mémoire et qu'il que l'ordinateur ne pourra pas
                    nécessairement trouver ce genre d'espace mémoire facilement
                </li>
            </ul>
        </section>

        <section>
            <h2>Opérations</h2>
            <dl>
                <dt>Parcourir le tableau multidimensionnel</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(n)</IC>
                    </p>
                    <p>
                        Les opérations et leur complexité algorithmique sont les même sur un tableau multidimensionnel
                        que sur un tableau régulier. La façon dont on va parcourir le tableau multidimensionnel est
                        toutefois un peu différente. En effet, il sera généralement nécessaire d'avoir une boucle pour
                        chaque dimension du tableau multidimensionnel.
                    </p>
                    <CodeBlock language="csharp">{ parcourir }</CodeBlock>
                    <p>
                        Si vous utilisez un itérateur, il est toutefois possible de parcourir la tableau à l'aide
                        d'une seule boucle.
                    </p>
                    <CodeBlock language="csharp">{ parcourirIterateur }</CodeBlock>
                </dd>
            </dl>
        </section>
    </>
}
