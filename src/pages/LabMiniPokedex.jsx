import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';
import DownloadBlock from '../component/DownloadBlock';

import distribue from '../resources/laboratoire-pokedex-distribué.zip';
import solution from '../resources/laboratoire-pokedex-solution.zip';

const rencontreAleatoire =
`// Sélectionner un emplacement aléatoire
// parmi les 4 emplacements du jeu
int indexEmplacement = MiniPokedex.randomiseur.Next(Emplacements.liste.Count);
Emplacement emplacement = Emplacements.liste[indexEmplacement];

// On génère une rencontre aléatoire dans l'emplacement pour 
// utilisé les mêmes chances de rencontre que si l'on visite 
// un emplacement
Pokemon pokemon = emplacement.RencontreAleatoire();`;

export default function LabMiniPokedex() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                Un Pokédex est un élément important de l'univers fictif de Pokémon. Voici un petit cours rapide sur
                les Pokémons si vous ne les connaissez pas bien.
            </p>
            <p>
                Les Pokémons sont des créatures fictives ressemblant un peu à des animaux. Des dresseurs capturent les
                Pokémons dans le but de les dresser, de les collectionner ou encore pour participer à des combats de
                Pokémons. Le Pokédex, quant à lui, est un encyclopédie électronique des Pokémons qu'un dresseur a
                rencontré jusqu'à présent. Les collectionneurs de Pokémons essaies donc de remplir leur Pokédex dans
                le but voir tous les Pokémon.
            </p>
            <p>
                Dans ce laboratoire, nous nous concentrerons sur cette partie de collection des Pokémons avec un
                Pokédex. Le laboratoire vous donnera un projet de base d'un petit jeu où le but est de compléter un
                Pokédex réduit (s'il fallait trouver les 893 différents Pokémons, ce serait trop long). Vous devrez
                écrire la partie du code du Pokédex puisque celui-ci est un bon exemple d'utilisation d'un ensemble.
            </p>
            <p>
                Compléter le projet distribué ci-dessous:
            </p>
            <ol>
                <li>Télécharger le fichier <IC>distribué.zip</IC>.</li>
                <li>Dézipper le fichier <IC>distribué.zip</IC>.</li>
                <li>
                    Écrire le code de la fonction <IC>GetNombrePokemon</IC> dans le fichier <IC>Pokedex.cs</IC>.
                    Cette fonction doit retourner le nombre de Pokémons enregistré dans le <IC>pokedex</IC> jusqu'à
                    présent.
                </li>
                <li>
                    Écrire le code de la fonction <IC>EstComplet</IC> dans le fichier <IC>Pokedex.cs</IC>.
                    Cette fonction doit retourner une valeur booléene indiquant si le <IC>pokedex</IC> est complet.
                    On considère que le Pokédex est complet s'il contient les 26 différents Pokémons de ce jeu. La
                    constante <IC>NB_POKEDEX_POKEMON</IC> peut être utilisé pour représenter le nombre de Pokémons
                    différents dans ce jeu.
                </li>
                <li>
                    Écrire le code de la fonction <IC>AjouterSiNonContenu</IC> dans le fichier <IC>Pokedex.cs</IC>.
                    Cette fonction doit ajouter le Pokémon passé en paramètre dans le Pokédex si celui-ci n'est pas
                    déjà dedans. Cette fonction doit retourner une valeur booléenne indiquant si le Pokémon passé en
                    paramètre était déjà dans le Pokédex ou non.
                </li>
                <li>
                    Écrire le code de la fonction <IC>GenererEnsemblePokemon</IC> dans le fichier <IC>Pokedex.cs</IC>.
                    Cette fonction doit faire les processus suivants:
                    <ol>
                        <li>
                            Créer un nouvel ensemble de Pokémon vide.
                        </li>
                        <li>
                            Générer aléatoirement entre 0 et 5 Pokémons dans cet ensemble. Pour générer les Pokémons,
                            vous pouvez utiliser la fonction <IC>RencontreAleatoire</IC> des <IC>Emplacement</IC> de
                            la façon suivante:
                            <CodeBlock language="csharp">{rencontreAleatoire}</CodeBlock>
                            <ColoredBox heading="Attention">
                                Un Pokémon retourné par cette méthode peut être <IC>null</IC>. Assurez-vous de ne pas
                                ajouter un Pokémon <IC>null</IC>, sinon cela va causer des erreurs dans le jeu.
                            </ColoredBox>
                        </li>
                        <li>
                            Faite un union de cet ensemble avec le <IC>pokedex</IC>. Ainsi, tous les pokémons généré
                            dans cet ensemble seront ajouté le Pokédex du joueur. Les Pokémons déjà existant dans
                            le Pokédex du joueur seront ignoré.
                        </li>
                        <li>
                            Retourner l'ensemble de Pokémon généré pour que le programme principal puisse les afficher
                            dans la console.
                        </li>
                    </ol>
                </li>
            </ol>
        </section>

        <section>
            <h2>Distribué et solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ distribue } name="distribué.zip"></DownloadBlock.File>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
