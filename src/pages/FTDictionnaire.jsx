import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';
import Video from '../component/Video';

import dictionnaire from '../resources/dictionnaire.png';

const creation = 
`// Dictionnaire avec une clé en chaine 
// de caractère et une valeur entière
Dictionary<string, int> dictionnaire = new Dictionary<string, int>();

// Dictionnaire avec une clé entière et 
// une valeur en objet d'une classe 
// quelconque nommée "Carte"
Dictionary<int, Carte> dictionnaire2 = new Dictionary<int, Carte>();

// Dictionnaire avec une taille de départ 
// prédéfinie
Dictionary<string, string> dictionnaire3 = new Dictionary<string, string>(30);`;

const add =
`// Dictionnaire 
Dictionary<string, string> dictionnaire = new Dictionary<string, string>();

// Ajoute des valeurs dans le dictionnaire
dictionnaire.Add(
    "programmeur", 
    "Personne chargée de la préparation, de l'écriture et de la mise au point d'un programme pour ordinateur."
);
dictionnaire.Add(
    "ordinateur", 
    "Machine automatique de traitement de l'information, obéissant à des programmes formés par des suites d'opérations arithmétiques et logiques."
);

// Ajoute avec l'opérateur d'indexation
dictionnaire["donnée"] = "Représentation conventionnelle d'une information en vue de son traitement informatique.";

// Ne fonctionne pas et lance une exception  
// puisque la clé existe déjà
dictionnaire.Add(
    "programmeur", 
    "Quelqu'un comme Jonathan."
);`;

const remove =
`// Dictionnaire 
Dictionary<string, string> dictionnaire = new Dictionary<string, string>();
dictionnaire.Add("2266282395", "Le Seigneur des Anneaux - Tome 1 La freternité de l'anneau");
dictionnaire.Add("2070584623", "Harry Potter à l'école des sorciers");
dictionnaire.Add("8822800346", "Le Petit Prince - Avec les dessins de l'auteur");
dictionnaire.Add("0804139024", "The Martian: A Novel");
dictionnaire.Add("2207303403", "Le guide du voyageur galactique");

// Retire des valeurs dans le dictionnaire
dictionnaire.Remove("2266282395");
dictionnaire.Remove("0804139024");
Console.WriteLine(dictionnaire.Count);

// Retourne "false"
bool reussi = dictionnaire.Remove("test");
Console.WriteLine(reussi);`;

const modify =
`// Dictionnaire 
Dictionary<string, string> dictionnaire = new Dictionary<string, string>();
dictionnaire.Add("2266282395", "Le Seigneur des Anneaux - Tome 1 La freternité de l'anneau");
dictionnaire.Add("2070584623", "Harry Potter à l'école des sorciers");
dictionnaire.Add("8822800346", "Le Petit Prince - Avec les dessins de l'auteur");
dictionnaire.Add("0804139024", "The Martian: A Novel");
dictionnaire.Add("2207303403", "Le guide du voyageur galactique");

// Modifie la valeur dans le dictionnaire
dictionnaire["2070584623"] = "H4rrY P0tt3r";

// La valeur n'existe pas, elle est donc ajouté
dictionnaire["2012248403"] = "Monsieur Rêve";`;

const recherche =
`// Dictionnaire 
Dictionary<string, string> dictionnaire = new Dictionary<string, string>();
dictionnaire.Add("2266282395", "Le Seigneur des Anneaux - Tome 1 La freternité de l'anneau");
dictionnaire.Add("2070584623", "Harry Potter à l'école des sorciers");
dictionnaire.Add("8822800346", "Le Petit Prince - Avec les dessins de l'auteur");
dictionnaire.Add("0804139024", "The Martian: A Novel");
dictionnaire.Add("2207303403", "Le guide du voyageur galactique");

// Recherche du livre avec le ISBN 0804139024
string livre = dictionnaire["0804139024"];
Console.WriteLine(livre);

// La recherche d'un livre dont le ISBN n'est
// pas dans le dictionnaire lance une 
// exception
string livre2 = dictionnaire["0123456789"];
Console.WriteLine(livre2);

// Si vous n'êtes pas certain si la clé 
// existe ou non, vous pouvez utiliser la 
// méthode "ContainsKey" pour vous assurer
// qu'elle existe avant de faire une recherche
if(dictionnaire.ContainsKey("0123456789"))
{
    Console.WriteLine(dictionnaire["0123456789"]);
}`;

const parcourir =
`// Dictionnaire 
Dictionary<string, string> dictionnaire = new Dictionary<string, string>();
dictionnaire.Add("2266282395", "Le Seigneur des Anneaux - Tome 1 La freternité de l'anneau");
dictionnaire.Add("2070584623", "Harry Potter à l'école des sorciers");
dictionnaire.Add("8822800346", "Le Petit Prince - Avec les dessins de l'auteur");
dictionnaire.Add("0804139024", "The Martian: A Novel");
dictionnaire.Add("2207303403", "Le guide du voyageur galactique");

// Parcourir
foreach(KeyValuePair<string, string> livre in dictionnaire)
{
    // Afficher la clé
    Console.Write(livre.Key);
    Console.Write(" - ");

    // Afficher la valeur
    Console.WriteLine(livre.Value);
}`;

export default function FTDictionnaire() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                Le dictionnaire est une structure de données similaire à l'ensemble. Elle permet la recherche rapide
                d'éléments. Toutefois, contrairement à l'ensemble, elle ne stocke pas uniquement un élément, mais
                plutôt une paire d'élément que l'on nommera la clé et la valeur.
            </p>
            <p>
                Dans un dictionnaire, la clé est l'élément qui est utilisé pour faire la recherche. La valeur, quant à
                elle, est l'élément retourné. Un peu comme un vrai dictionnaire papier où la clé est le mot que l'on
                cherche et la valeur est la définition du mot. Un autre exemple pourrait être les éléments d'une base
                de données où la clé est la clé primaire de l'élément (son identifiant) et la valeur est l'ensemble de
                l'élément. La clé et la valeur peuvent être de n'importe quel type, même d'un objet créé par
                vous-même.
            </p>
            <p>
                On peut représenter graphiquement le dictionnaire de la façon suivante:
            </p>
            <img src={dictionnaire} alt="Représentation d'un dictionnaire" />
            <p>
                En C#, un dictionnaire de base s'utilise avec la classe <IC>Dictionary</IC>. En anglais, vous
                entendrez aussi souvent le terme <IC>Map</IC> qui est aussi un dictionnaire, mais le langage C#
                n'utilise pas ce terme. En C#, le dictionnaire par défaut utilise le concept du hachage pour accélérer
                la recherche et stocker ses données, mais il existe d'autre façon d'implémenter un dictionnaire. Vous
                pouvez créer un <IC>Dictionary</IC> de la façon suivante:
            </p>
            <CodeBlock language="csharp">{ creation }</CodeBlock>
        </section>

        <section>
            <h2>Avantages</h2>
            <ul>
                <li>Les opérations d'ajout et de retrait sont très efficace.</li>
                <li>La recherche d'un élément dans un dictionnaire est très efficace.</li>
            </ul>
        </section>

        <section>
            <h2>Inconvénients</h2>
            <ul>
                <li>Pas d'accès aléatoires.</li>
                <li>Pas de répétition de clés.</li>
                <li>L'ordre des données n'est pas connu.</li>
            </ul>
        </section>

        <section>
            <h2>Opérations</h2>
            <dl>
                <dt>Accès aléatoire</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>N/A</IC>
                    </p>
                    <p>
                        On ne peut pas faire d'accès aléatoire dans un dictionnaire. En fait, il n'y a pas de concept
                        de position ou d'ordre dans le dictionnaire par défaut.
                    </p>
                </dd>

                <dt>Ajouter</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(1)</IC>
                    </p>
                    <p>
                        Permet d'ajouter un élément dans le dictionnaire. L'ajout dans un ensemble est toujours très efficace.
                    </p>
                    <p>
                        Contrairement aux autres structures de données vues jusqu'à présent, la méthode <IC>Add</IC> du <IC>Dictionary</IC> prends
                        2 paramètres. Le premier paramètre est la clé et le deuxième est la valeur.
                    </p>
                    <p>
                        Une autre façon d'ajouter une valeur dans un dictionnaire est d'utiliser l'opérateur
                        d'indexation <IC>{'[ ... ]'}</IC>. Vous pouvez spécifier la clé entre les accolades carrées et
                        la valeur après le <IC>=</IC>.
                    </p>
                    <p>
                        Si vous essayez d'ajouter une paire de clé/valeur dont la clé existe déjà dans le dictionnaire avec la méthode <IC>Add</IC>,
                        ceci causera une erreur dans votre programme. En effet, les clés dans un dictionnaire doivent
                        être unique.
                    </p>
                    <CodeBlock language="csharp">{add}</CodeBlock>
                </dd>

                <dt>Retirer</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(1)</IC>
                    </p>
                    <p>
                        Permet de retirer un élément dans le dictionnaire. Le retrait dans un dictionnaire est toujours très efficace.
                    </p>
                    <p>
                        La méthode <IC>Remove</IC> prends en paramètre la clé de la paire clé/valeur à supprimerdu dictionnaire.
                    </p>
                    <p>
                        La méthode <IC>Remove</IC> va retourner une valeur booléenne indiquant si la valeur que l'on essait
                        de retirer existe dans le dictionnaire ou non. Si la valeur n'existe pas dans le dictionnaire, la
                        méthode <IC>Remove</IC> va retourner <IC>false</IC> et ne retirera rien dans le dictionnaire. Si la valeur
                        existe dans le dictionnaire, elle sera retiré et la méthode va retourner <IC>true</IC>.
                    </p>
                    <CodeBlock language="csharp">{remove}</CodeBlock>
                </dd>

                <dt>Modifier</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(1)</IC>
                    </p>
                    <p>
                        Permet de modifier la valeur associé à une clé dans un dictionnaire. La modification d'une valeur dans un dictionnaire est toujours très efficace.
                    </p>
                    <p>
                        La modification utilise l'opérateur d'indexation <IC>{'[ ... ]'}</IC> pour sélectionner la valeur à modifier.
                        Vous devez mettre la clé de la valeur que vous recherchez entre les accolades carrées.
                    </p>
                    <p>
                        La méthode <IC>Remove</IC> va retourner une valeur booléenne indiquant si la valeur que l'on essait
                        de retirer existe dans le dictionnaire ou non. Si la valeur n'existe pas dans le dictionnaire, la
                        méthode <IC>Remove</IC> va retourner <IC>false</IC> et ne retirera rien dans le dictionnaire. Si la valeur
                        existe dans le dictionnaire, elle sera retiré et la méthode va retourner <IC>true</IC>.
                    </p>
                    <p>
                        Si la clé n'existe pas dans le dictionnaire, cette opération va ajouter la paire de clé/valeur
                        dans le dictionnaire.
                    </p>
                    <CodeBlock language="csharp">{modify}</CodeBlock>
                </dd>

                <dt>Recherche</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(1)</IC>
                    </p>
                    <p>
                        Dans un dictionnaire, la recherche correspond à trouver une valeur en fonction de sa clé. La
                        recherche dans un dicitonnaire est toujours très efficace.
                    </p>
                    <p>
                        La recherche utilise l'opérateur d'indexation <IC>{'[ ... ]'}</IC> pour faire sa recherche.
                        Vous devez mettre la clé que vous recherchez entre les accolades carrées.
                    </p>
                    <p>
                        Si la clé n'existe pas dans le dictionnaire, cette opération lance une erreur. Si vous n'êtes
                        pas certain de l'existance d'une clé dans le dictionnaire, vous pouvez utiliser la
                        méthode <IC>ContainsKey</IC> avant une recherche pour éviter l'erreur.
                    </p>
                    <CodeBlock language="csharp">{recherche}</CodeBlock>
                </dd>

                <dt>Parcourir</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(n)</IC>
                    </p>
                    <p>
                        Puisqu'il n'y a pas d'accès aléatoire dans un dictionnaire, on peut uniquement le parcourir à l'aide
                        d'une boucle à itérateur. La boucle <IC>foreach</IC> est donc importante ici.
                    </p>
                    <p>
                        Lorsque l'on parcourt un dictionnaire, on parcours sa liste de paires clés/valeurs. Nous
                        utiliserons donc un object de la classe <IC>KeyValuePair&lt;TKey, TValue&gt;</IC> dans notre
                        boucle <IC>foreach</IC>. Assurez vous de mettre les mêmes types de clé et de valeurs que votre
                        dictionnaire. L'objet <IC>KeyValuePair</IC> permet d'aller chercher la clé
                        avec l'attribut <IC>Key</IC> et la valeur avec l'attribut <IC>Value</IC>.
                    </p>
                    <CodeBlock language="csharp">{parcourir}</CodeBlock>
                    <ColoredBox heading="Attention">
                        Une des particularités du parcour d'un dictionnaire de base est qu'il ne préserve pas un ordre des
                        éléments. Bien que l'exécution du code ci-dessus semble montré que les éléments reste dans le
                        même ordre, l'ajout ou le retrait de beaucoup de données variées peut changer l'ordre sans
                        qu'il soit facile de le prédire.
                    </ColoredBox>
                </dd>
            </dl>
        </section>

        <section>
            <h2>Documentation</h2>
            <p>
                Si vous voulez voir plus de fonctionnalité et de possibités avec le dictionnaire par hachage du langage C#,
                je vous recommande fortement d'aller visiter sa page de documentation. Cela vous permettra de voir
                toutes les méthodes disponibles et d'apprendre à mieux les utiliser. Vous pourrez y trouver de
                nombreuses fonctionnalités que nous n'avons pas couvert.
            </p>
            <p>
                <a target="_blank" rel="noopener noreferrer" href="https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.dictionary-2?view=net-5.0">
                    C# Dictionary&lt;TKey, TValue&gt; Class
                </a>
            </p>
        </section>

        <section>
            <h2>Vidéo</h2>
            <Video title="Le dictionnaire" src="https://www.youtube.com/embed/rPWdWH8o6BU" />
        </section>
    </>
}
