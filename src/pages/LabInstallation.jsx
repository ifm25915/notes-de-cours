import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

export default function LabInstallation() {
    return <>
        <section>
            <h2>Instruction</h2>
            <p>
                Pour le cours, cous aurez besoin de Visual Studio Community ou de Visual Studio Code. Choisissez celui 
                que vous préférez ou essayez les deux. Vous pourrez ainsi décider lequel vous préférez.
            </p>
        </section>

        <section>
            <h2>Visual Studio Community</h2>
            <ol>
                <li>
                    Télécharger <a target="_blank" rel="noopener noreferrer" href="https://visualstudio.microsoft.com/vs/community/">
                        Visual Studio Community
                    </a>
                </li>
                <li>
                    Installer Visual Studio Community
                </li>
                <li>
                    Durant l'installation, sélectionner le module <IC>.NET desktop development</IC>
                </li>
            </ol>
        </section>

        <section>
            <h2>Visual Studio Code</h2>
            <ol>
                <li>
                    Télécharger <a target="_blank" rel="noopener noreferrer" href="https://code.visualstudio.com/">
                        Visual Studio Code
                    </a>
                </li>
                <li>
                    Installer Visual Studio Code
                </li>
                <li>
                    Installer la plateforme <a target="_blank" rel="noopener noreferrer" href="https://dotnet.microsoft.com/download">
                        .NET
                    </a>
                </li>
                <li>
                    Télécharger l'extension C# dans Visual Studio Code
                </li>
            </ol>
            <p>
                Avec Visual Studio Code, les opérations sur un projet se font dans la console. Voici quelques
                commandes utiles:
            </p>
            <dl>
                <dt>Créer un projet console vide:</dt>
                <dd><CodeBlock language="shell">$ dotnet new console</CodeBlock></dd>
                <dt>Compiler le projet courant:</dt>
                <dd><CodeBlock language="shell">$ dotnet build</CodeBlock></dd>
                <dt>Compiler et lancer le projet courant:</dt>
                <dd><CodeBlock language="shell">$ dotnet run</CodeBlock></dd>
            </dl>
        </section>
    </>
}
