import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

import euclideExemple from '../resources/euclide.png';

const factorielle = 
`static int Factorielle(int n)
{
    if(n == 0)
    {
        // Condition d'arrêt de récursivité
        return 1;
    }
    else 
    {
        // Appel récursif
        return n * Factorielle(n - 1);
    }
}`;

const factorielleLoop = 
`static int Factorielle(int n)
{
    int resultat = 1;
    for(int i = 2 ; i <= n ; i++)
    {
        resultat *= i;
    }

    return resultat;
}`;

const euclide = 
`static int PGCD(int n, int m)
{
    if(n == m)
    {
        // Condition d'arrêt de récursivité
        return n;
    }
    else if(n < m)
    {
        // Appel récursif
        return PGCD(m - n, n);
    }
    else
    {
        // Appel récursif
        return PGCD(m, n - m);
    }
}`;

const euclideLoop = 
`static int PGCD(int n, int m)
{
    while(n != m)
    {
        if(n < m)
        {
            m -= n;
        }
        else
        {
            n -= m;
        }
    }

    return n;
}`;

export default function RecursiveIntro() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                En mathématique, une fonction récursive est une fonction qui fait appel à elle-même.
            </p>
            <div style={{padding: '.5rem', margin: '0 0 1rem', fontFamily: 'monospace', border: '2px solid var(--border-interactive-color)'}}>
                Factorielle:
                <div style={{display: 'flex', margin: '0 0 1rem'}}>
                    <div>
                        <div>
                            F(n) = n * F(n-1)
                        </div>
                        <div>
                            F(n) = 1
                        </div>
                    </div>
                    <div style={{marginLeft: '2rem'}}>
                        <div>
                            Si n &gt; 0
                        </div>
                        <div>
                            Si n = 0
                        </div>
                    </div>
                </div>
                
                Fibonacci:
                <div style={{display: 'flex'}}>
                    <div>
                        <div>
                            F(n) = F(n-1) + F(n-2)
                        </div>
                        <div>
                            F(n) = n
                        </div>
                    </div>
                    <div style={{marginLeft: '2rem'}}>
                        <div>
                            Si n &gt; 1
                        </div>
                        <div>
                            Si n = 0 ou n = 1
                        </div>
                    </div>
                </div>
            </div>
            <p>
                Une solution récursive fait un pas en direction de la solution, puis soumet le même problème, 
                maintenant plus près de cette solution. Par exemple, si on vous demande le chemin pour se rendre à un 
                endroit E, on pourrait répondre: "Suivez cette route sur 2km, tournez à droite au feu de circulation 
                et demandez à quelqu'un comment vous rendre à E".
            </p>
            <p>
                La plupart des langages de programmation moderne permettent d'écrire des routines récursive. C'est 
                aujourd'hui une fonctionnalité de base qu'un langage de programmation doit supporté. Les langages 
                comme le C#, C++, Java, Javascript et Python supportent donc les fonctions récursive sans problèmes. 
                En fait, la récursivité est super importante en programmation puisque certains problèmes peuvent 
                uniquement être résolue efficacement avec des récursions. Toutefois, elle n'est pas essentielle 
                puisque toute solution récursive peut être exprimée sans récursivité.
            </p>
            <p>
                La récursivité est plus facilement assimilable en étudiant des exemples de son exploitation. Dans ce 
                document, nous explorerons donc certains exemples classiques de récursivité.
            </p>
        </section>

        <section>
            <h2>Régles</h2>
            <p>
                Pour bien fonctionner sans problème, la récursivité doit suivre certaines règles:
            </p>
            <ol>
                <li>La fonction doit contenir au moins une invocation à elle-même</li>
                <li>La fonction doit avoir une condition qui assure l'arrêt éventuel des appels récursifs</li>
            </ol>
            <p>
                Si ces 2 règles ne sont pas supporté, il est possible que votre code génère des erreurs de mémoire ou 
                qu'il ne soit tout simplement pas récursif.
            </p>
        </section>
        
        <section>
            <h2>Exemple - La factorielle</h2>
            <p>
                La factorielle d'un nombre est la multiplication de tous les nombres de 1 jusqu'au nombre. Par 
                exemple, la factorielle du nombre 6 est 
                1&nbsp;&times;&nbsp;2&nbsp;&times;&nbsp;3&nbsp;&times;&nbsp;4&nbsp;&times;&nbsp;5&nbsp;&times;&nbsp;6&nbsp;=&nbsp;720.
                La définition mathématique de la factorielle est la suivante:
            </p>
            <div style={{display: 'flex', alignItems: 'center', padding: '.5rem', margin: '0 0 1rem', fontFamily: 'monospace', border: '2px solid var(--border-interactive-color)'}}>
                <div>
                    n! = 
                </div>
                <div style={{marginLeft: '2rem'}}>
                    <div>
                        1
                    </div>
                    <div>
                        n(n-1)!
                    </div>
                </div>
                <div style={{marginLeft: '2rem'}}>
                    <div>
                        Si n = 0
                    </div>
                    <div>
                        Si n &gt; 0
                    </div>
                </div>
            </div>
            <p>
                Cette définition est récursive puisque dans la définition de la factorielle, nous utilisons la 
                factorielle. En effet, si <IC>n&nbsp;&gt;&nbsp;0</IC>, nous multiplions <IC>n</IC> par la factorielle 
                de <IC>n&nbsp;-&nbsp;1</IC>.
            </p>
            <p>
                En C#, nous pouvons implémenter la factorielle de la façon récursive suivante:
            </p>
            <CodeBlock language="csharp">{factorielle}</CodeBlock>
            <p>
                Comme mentionné plus haut, il existe toujours une solution non récursive à un problème. Dans le cas 
                de la factorielle, nous pourrions faire ceci:
            </p>
            <CodeBlock language="csharp">{factorielleLoop}</CodeBlock>
        </section>

        <section>
            <h2>Exemple - Algorithme d'Euclide</h2>
            <p>
                L'algorithme d'Euclide permet de calculer le plus grand commun diviseur de 2 nombres entiers positifs.
                Cet algorithme consiste à soustraire plusieurs fois le plus petit nombre <IC>n</IC> du plus grand 
                nombre <IC>m</IC> jusqu'à ce que la différence obtenue <IC>d</IC> soit inférieure au plus petit 
                nombre <IC>n</IC>. On remplace ensuite <IC>n</IC> par <IC>d</IC> et <IC>m</IC> par <IC>n</IC>, et on 
                recommence jusqu'à ce que <IC>n</IC> et <IC>m</IC> soient égaux.
            </p>
            <p>
                Par exemple, pour trouver le plus grand commun diviseur entre 494 et 130, on ferait les étapes 
                suivantes:
            </p>
            <img src={euclideExemple} alt="Algorithme d'Euclide exécuté sur les nombres 494 et 130" />
            <p>
                En C#, nous pouvons implémenter l'algorithme d'Euclide récursif de la façon suivante:
            </p>
            <CodeBlock language="csharp">{euclide}</CodeBlock>
            <p>
                La solution non récursive, quant à elle, peut ressembler à ceci:
            </p>
            <CodeBlock language="csharp">{euclideLoop}</CodeBlock>
        </section>
    </>
}
