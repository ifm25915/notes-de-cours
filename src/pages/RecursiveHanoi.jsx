import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

import hanoi from '../resources/hanoi.png';
import hanoi2 from '../resources/hanoi2.png';
import hanoi3 from '../resources/hanoi3.png';

const pseudoHanoi = 
`Hanoï(n, TigeSource, TigeTemp, TigeDestination):
    si (n = 1) alors
        déplacer l'anneau de TigeSource à TigeDestination
    sinon
        Hanoï(n-1, TigeSource, TigeDestination, TigeTemp)
        déplacer l’anneau de TigeSource à TigeDestination
        Hanoï(n-1, TigeTemp, TigeSource, TigeDestination)`;

export default function RecursiveHanoi() {
    return <>
        <section>
            <h2>Un jeu récursif</h2>
            <p>
                La tour de Hanoï est un problème récursif très simple. À la base du jeu, nous avons 3 tiges avec un 
                ensemble d'anneaux de taille différentes empilées sur la première tige en ordre croissant de diamètre.
            </p>
            <img src={hanoi} alt="La tour de Hanoï" />
            <p>
                L'objectif du jeu est de déplacer la tour d'anneaux de la tige A à la tige C. Cela semble facile, mais 
                certaines règles du jeu compliqueront un peu notre tâche. Voici ces règles:
            </p>
            <ol>
                <li>On ne déplace qu'un seul anneau à la fois</li>
                <li>On peut utiliser la tige B</li>
                <li>Un anneau ne peut pas être déposé sur un anneau de diamètre inférieur</li>
            </ol>
        </section>

        <section>
            <h2>Stratégie</h2>
            <p>
                Dans le diagramme ci-dessus, si on ignore l’anneau au bas de la pile, on obtient alors le même 
                problème, mais avec 3 anneaux au lieu de 4. Ceci est possible puisqu'on peut mettre n’importe quel des 
                3 anneaux sur celui au bas de la pile. De plus, on sait comment résoudre le problème avec un seul 
                anneau. Il suffit simplement de déplacer l’anneau de la tige A à la tige C. Si cette stratégie vous 
                semble récursive, vous avez tout à fait raison!
            </p>
            <p>
                Pour résoudre avec deux anneaux, on déplace l’anneau 1 de la tige A à B, 2 de la tige A à C, puis 1 
                de B à C. On résout donc le problème de taille 1 deux fois (A vers B, B vers C).
            </p>
            <img src={hanoi2} alt="Stratégie de la tour de Hanoï pour 2 anneaux" />
            <p>
                Pour résoudre avec trois anneaux, on déplace les anneaux 1 et 2 de la tige A à B, l’anneau 3 de la 
                tige A à C, puis les anneaux 1 et 2 de B à C. On résout donc le problème de taille 2 deux fois 
                (A vers B, B vers C).
            </p>
            <img src={hanoi3} alt="Stratégie de la tour de Hanoï pour 3 anneaux" />
            <p>
                Pour résoudre avec <IC>n</IC> anneaux, on déplace les anneaux 1 à <IC>n - 1</IC> de la tige A à B, 
                l’anneau <IC>n</IC> de la tige A à C, puis les anneaux 1 à <IC>n - 1</IC> de B à C. On résout donc le 
                problème de taille <IC>n - 1</IC> deux fois (A vers B, B vers C).
            </p>
        </section>

        <section>
            <h2>Code</h2>
            <p>
                Voici le pseudo-code de l'algorithme récursif de la tour de Hanoï:
            </p>
            <CodeBlock language="pseudo">{pseudoHanoi}</CodeBlock>
        </section>
    </>
}
