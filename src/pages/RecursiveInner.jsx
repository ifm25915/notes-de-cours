import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

import factorielleExec from '../resources/factorielleexec.png';
import pileAppel from '../resources/pileappel.png';

const factorielle = 
`static int Factorielle(int n)
{
    int a = 0;
    if(n == 0)
    {
        // Condition d'arrêt de récursivité
        a = 1;
    }
    else 
    {
        // Appel récursif
        a = n * Factorielle(n - 1);
    }

    return a;
}`;

const factorielleBig = 
`using System.Numerics;

// ...

static BigInteger Factorielle(int n)
{
    if(n == 0)
    {
        return 1;
    }
    else 
    {
        return n * Factorielle(n - 1);
    }
}`;

export default function RecursiveInner() {
    return <>
        <section>
            <h2>Appel de fonction</h2>
            <p>
                La récursivité ne demande aucun traitement spécial de la part du compilateur puisqu'elle est basée 
                sur le concept de variables locales. Souvenez-vous que :
            </p>
            <ol>
                <li>Les variables locales existent seulement durant l’exécution de la fonction</li>
                <li>Les paramètres sont des variables locales initialisées aux valeurs passées en appel</li>
            </ol>
            <p>
                Dans une fonction récursive, on peut imaginer qu’une copie de la fonction est créée lors de l’appel 
                récursif. Par exemple, si nous utilisons un algorithme de factorielle avec une variable locale comme 
                ci-dessous, nous aurions une exécution qui ressemblerait à ceci:
            </p>
            <CodeBlock language="csharp">{factorielle}</CodeBlock>
            <img src={factorielleExec} alt="Exécution imagée de la fonction factorielle." />
            <p>
                Pour comprendre comment les valeurs de <IC>n</IC> et <IC>a</IC> sont conservées entre les différents 
                appels de la fonction <IC>Factorielle</IC>, nous devons comprendre comment la pile d'appels est 
                exploitée par le programme exécutable pour gérer les invocations de fonctions.
            </p>
        </section>

        <section>
            <h2>La pile d'appels</h2>
            <p>
                Lorsqu’une fonction <IC>A()</IC> invoque une fonction <IC>B()</IC>, elle exploite une pile pour 
                stocker des informations à échanger entre les deux fonctions. La pile est utilisé de la façon 
                suivante: 
            </p>
            <ol>
                <li>
                    La fonction <IC>A()</IC> utilise la pile d’appels pour stocker ses variables locales ainsi que les arguments à fournir à la fonction <IC>B()</IC>.
                </li>
                <li>
                    La fonction <IC>B()</IC> récupère les arguments de la pile d’appels pour initialiser ses paramètres, puis stocke dans la pile d’appels sa valeur de retour.
                </li>
                <li>
                    La fonction <IC>A()</IC> récupère de la pile d’appels la valeur de retour de <IC>B()</IC>.
                </li>
            </ol>
            <p>
                Le principe de la pile fonctionne aussi si nous devons appeler d'autres fonctions. Si la 
                fonction <IC>B()</IC> doit invoquer une troisième fonction <IC>C()</IC>, les variables et arguments 
                de <IC>A()</IC> et <IC>B()</IC> ne sont pas perdus lors de l’exécution de <IC>C()</IC> puisque ces 
                informations demeurent dans la pile d’appels.
            </p>
            <p>
                Le principe est identique lors des appels récursifs. En effet, les variables et paramètres de la 
                fonction sont successivement stockés dans la pile d’appels lors des invocations récursives. Au retour 
                d’un appel récursif, la fonction a récupéré ses valeurs de variables et de paramètres qu’elle avait 
                avant l’appel récursif. Voici un exemple graphique de la pile d'appel avec la 
                fonction <IC>Factorielle</IC> ci-dessus:
            </p>
            <img src={pileAppel} alt="Présentation visuelle de la pile d'appel pour la fonction factorielle." />
            <p>
                Donc, avant un appel récursif, les valeurs des variables locales (incluant les paramètres) sont 
                emmagasinées sur la pile. Elles sont ensuite restaurées (dépilées) au retour de l’appel récursif.
                Ce processus est appliqué à tous les appels de fonctions, récursifs ou non.
            </p>
        </section>

        <section>
            <h2>Problèmes de la récursivité sur la pile d'appel</h2>
            <p>
                Comme présenté ci-dessus, chaque appel de fonction dans une fonction empile de nouvelles données sur 
                la pile d'appel. Les appels récursifs de grande taille demande donc beaucoup d'insertions sur la pile, 
                ce qui peut être problématique. C'est pourquoi, dans plusieurs langages, comme le C#, il est 
                préférable d'utiliser une version non récursive, si possible.
            </p>
            <p>
                Par exemple, si nous modifions légèrement la fonction <IC>Factorielle</IC> pour qu'elle accepte les 
                très grands nombres et que nous lançons <IC>Factorielle(1000)</IC>, il y aurais 1000 empilement sur la 
                pile, ce qui coûterait beaucoup de mémoire à notre programme.
            </p>
            <CodeBlock language="csharp">{factorielleBig}</CodeBlock>
            <p>
                En fait, la pile d'appel possède une taille maximale. Si nous faisons assez d'appel récursif, votre 
                programme va tout simplement vous afficher une erreur indiquant qu'il manque de mémoire. Si vous 
                voulez tester, lancez la fonction <IC>Factorielle(10000)</IC> pour voir une erreur de 
                type <IC>Stack overflow</IC>.
            </p>
        </section>
    </>
}
