import React from 'react';
import IC from '../component/InlineCode';

import tripetit from '../resources/tricomppetit.png';
import trigrand from '../resources/tricompgrand.png';

export default function TriClasse() {
    return <>
        <section>
            <h2>Méthodes de classement</h2>
            <p>
                Comme vous pouvez le constater, il existe de nombreux algorithmes de tri. On pourrait d'ailleurs
                presque écrire un livre juste sur les tris inefficaces! Il existe donc plusieurs façons de classifier
                les algorithmes de tri. En voici quelques unes:
            </p>
            <dl>
                <dt>Complexité algorithmique</dt>
                <dd>
                    On utilise la notation Grand O pour indiquer la performance d'un algorithme du point de vu du
                    nombre d'opérations qu'il doit faire. En général, on va définir l'algorithme en pire, meilleur et
                    moyen cas. Les algorithmes de <IC>O(n<sup>2</sup>)</IC> ou pire sont considérés comme lent alors
                    que les algorithmes de <IC>O(n log n)</IC> sont considéré comme rapide.
                </dd>

                <dt>Utilisation de mémoire</dt>
                <dd>
                    Les algorithmes de tri vu jusqu'à présent font le triage directement dans le tableau à trié. Ce
                    n'est toutefois pas le cas de tous les algorithmes de tri. Certains nécessitent des tableaux
                    supplémentaires pour copier ou transférer des données. Ils nécessitent donc de la mémoire
                    supplémentaire.
                </dd>

                <dt>Récursion</dt>
                <dd>
                    Certains algorithmes sont récursifs. Il faut donc considérer qu'ils utiliseront un certaine
                    quantité d'espace sur la pile d'appel et qu'ils feront plusieurs appels à des fonctions, ce qui
                    peut être un peu plus lent.
                </dd>
            </dl>
        </section>

        <section>
            <h2>Comparaison des algorithmes</h2>
            <p>
                La rapidité d'exécution d'un algorithme de tri est classifié en fonction du nombre d'éléments à trier
                ainsi que du nombre de comparaisons et d'inversions d'éléments effectuées lors du tri.
            </p>
            <p>
                Plutôt que d'évaluer la performance d'un algorithme de tri en fonction du temps requis pour trier les
                éléments (dépendant de la vitesse de l'ordinateur), on l'évalue en fonction du nombre de comparaisons
                et d'inversions (ou échanges) d'éléments dans le conteneur.
            </p>
            <p>
                Certains algorithmes rapides sont parfois plus lents que les algorithmes lents lorsque le nombre
                d'éléments à trier est petit. Ceci est généralement dû à:
            </p>
            <ul>
                <li>
                    À cause du travail préparatoire considérable effectué par certains algorithmes rapides avant de
                    commencer à trier.
                </li>
                <li>
                    Au fait que les algorithmes rapides sont souvent récursifs et que la gestion de la pile d'appels
                    requiert un temps d'exécution supplémentaire dont sont épargnés les algorithmes non récursif.
                </li>
                <li>
                    Au fait que la majorité des algorithmes lents sont non récursifs.
                </li>
            </ul>
            <p>
                Voici des diagrammes de comparaison du nombre de comparaisons et d'inversions du tri par insertion, du
                tri Shell et du tri rapide. Nous verrons ces 3 tris plus tard dans le cours.
            </p>
            <img src={tripetit} alt="Comparaison de tri pour un petit nombre d'éléments" />
            <img src={trigrand} alt="Comparaison de tri pour un grand nombre d'éléments" />
            <p>
                Comme vous pouvez le constater, le tri par insertion, considéré comme un tri lent, est plus efficace
                pour moins de 16 éléments que le tri rapide, considéré comme un tri efficace. Cela nous mènera donc à 
                créer des tris hybrides, comme le Timsort ou le Introsort que nous verrons aussi plus tard.
            </p>
        </section>
    </>;
}