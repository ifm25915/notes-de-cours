import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

import liste from '../resources/liste.png';
import liste2 from '../resources/liste2.png';

const creation = 
`// Liste d'entiers
LinkedList<int> liste1 = new LinkedList<int>();

// Liste de chaînes de caractères
LinkedList<string> liste2 = new LinkedList<string>();

// Liste à partir de données existantes
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
LinkedList<int> liste3 = new LinkedList<int>(tab);`;

const accesAleatoire = 
`static int? AccesAleatoire(LinkedList<int> liste, int indexATrouver)
{
    // On boucle dans le tableau jusqu'à ce qu'on trouve
    // notre index et on retourne sa valeur
    int index = 0;
    foreach(int valeur in liste){
        if(index == indexATrouver){
            return valeur;
        }

        // On incrémente nous-même l'index à chaque 
        // valeur qu'on parcourt
        index++;
    }

    // On retourne null si l'index n'est pas dans la 
    // liste
    return null;
}`;

const recherche =
`// Liste
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 0, 5, 4 };
LinkedList<int> liste = new LinkedList<int>(tab);

// Trouver un noeud à partir du début de la liste
LinkedListNode<int> noeudDebut = liste.Find(0);
Console.WriteLine(noeudDebut.Value);

// Trouver un noeud à partir de la fin de la liste
LinkedListNode<int> noeudFin = liste.FindLast(0);
Console.WriteLine(noeudFin.Value);`;

const ajouter =
`// Liste
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
LinkedList<int> liste = new LinkedList<int>(tab);

// Ajouter au début
liste.AddFirst(42);

// Ajouter à la fin
liste.AddLast(88);`;

const inserer =
`// Liste
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
LinkedList<int> liste = new LinkedList<int>(tab);

// On va trouver un noeud
LinkedListNode<int> noeud = liste.Find(7);

// Ajouter après un noeud
liste.AddAfter(noeud, 63);

// Ajouter avant un noeud
liste.AddBefore(noeud, 11);`;

const retirer =
`// Liste
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
LinkedList<int> liste = new LinkedList<int>(tab);

// Supprimer au début
liste.RemoveFirst();

// Supprimer à la fin
liste.RemoveLast();

// Noeud à supprimer
LinkedListNode<int> noeud = liste.Find(7);

// Ajouter après un noeud
liste.Remove(noeud);`;

const parcourir =
`// Liste
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
LinkedList<int> liste = new LinkedList<int>(tab);

foreach(int valeur in liste)
{
    Console.Write(valeur + ", ");
}

for (LinkedListNode<int> noeud = liste.First; noeud != null; noeud = noeud.Next)
{
    Console.Write(noeud.Value + ", ");
}`;

export default function FTListe() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                La liste, qu'on appèle souvent la liste chaînée, est une structure qui utilise de nombreux tableaux
                pour stocker ses données. Contrairement au vecteur qui utilise 1 seul tableau, la liste utilisera un
                tableau par valeur stocké. Nous appèlerons ces petits tableau des noeuds. Chaque noeud de la liste
                chaîné contient une valeur, mais aussi un pointeur ou une référence vers le prochain noeud ou le noeud
                précédant.
            </p>
            <p>
                On le représentera souvent graphiquement de la façon suivante:
            </p>
            <img src={ liste } alt="Représentation d'un tableau" />
            <p>
                La façon particulière dont la liste est construite nous permet d'ajouter des noeuds, donc des valeurs,
                très efficacement dans la liste.
            </p>
            <img src={ liste2 } alt="Représentation d'un tableau" />
            <p>
                En C#, un vecteur se crée avec la classe <IC>LinkedList</IC> et utilisera la
                classe <IC>LinkedListNode</IC> pour ses noeuds.
            </p>
            <CodeBlock language="csharp">{ creation }</CodeBlock>
        </section>

        <section>
            <h2>Avantages</h2>
            <ul>
                <li>Permet l'ajout et le retrait de données partout dans la liste de façon efficace</li>
                <li>Très performant pour la lecture séquentielle des données (la lecture dans l'ordre)</li>
                <li>Puisque la liste est segmenté en plusieurs noeuds, elle est très efficace en mémoire</li>
            </ul>
        </section>

        <section>
            <h2>Inconvénients</h2>
            <ul>
                <li>L'accès aléatoire n'est pas efficace</li>
                <li>Pour faire des recherches, on doit généralement parcourir toutes les données</li>
            </ul>
        </section>

        <section>
            <h2>Opérations</h2>
            <dl>
                <dt>Accès aléatoire</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(n)</IC>
                    </p>
                    <p>
                        Dans une liste, lorsqu'on veut accéder à un index particulier, nous devons absolument
                        parcourir chaque noeud à partir du début ou de la fin. Cette opération n'est donc vraiment pas
                        efficace. En C#, la seule façon de faire un accès aléaoire est de le faire manuellement d'une
                        façon similaire à ceci:
                    </p>
                    <CodeBlock language="csharp">{accesAleatoire}</CodeBlock>
                    <ColoredBox heading="Attention">
                        La liste chaînée n'est vraiment pas faite pour l'accès aléatoire. Si vous devez en faire, la
                        liste n'est probablement pas un choix de structure judicieux pour vos besoins.
                    </ColoredBox>
                </dd>

                <dt>Recherche de valeurs</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(n)</IC>
                    </p>
                    <p>
                        Pour trouver une valeur dans la liste, le code doit parcourir toutes les valeurs une par une
                        pour y rétourner le noeud de la valeur que l'on cherche. Heureusement, dans la liste en C#,
                        il existe la méthode <IC>Find</IC> et <IC>FindLast</IC> pour nous aider:
                    </p>
                    <CodeBlock language="csharp">{recherche}</CodeBlock>
                    <p>
                        La méthode <IC>Find</IC> et <IC>FindLast</IC> retourne le noeud. À première vu, ceci semble un
                        peu inutile, mais les noeuds nous seront très pratique pour les prochaines opérations.
                    </p>
                </dd>

                <dt>Ajouter des valeurs</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(1)</IC>
                    </p>
                    <p>
                        La liste chaînée est vraiment optimisé pour l'ajout et le retrait d'élément. En effet, ajouter
                        un noeud, peu importe l'endroit dans notre liste, se fait en <IC>O(1)</IC>. En C#, nous
                        pouvons ajouter des valeurs de la façon suivante:
                    </p>
                    <CodeBlock language="csharp">{ajouter}</CodeBlock>
                    <p>
                        Il y a toutefois un petit inconvénient: Si vous voulez insérer un noeud en plein milieu de la
                        liste, vous devez avoir accès au noeud précédant ou au noeud suivant. C'est un problème
                        puisque trouver un noeud se fait généralement en <IC>O(n)</IC>. Voici un exemple en C#:
                    </p>
                    <CodeBlock language="csharp">{inserer}</CodeBlock>
                    <p>
                        La liste est donc très efficace pour l'insertion de données au début et à la fin. Si nous
                        devons insérer au milieu, elle peut être efficace aussi, mais seulement si on insère toujours
                        avant ou après le même noeud, donc si on réutilise le noeud.
                    </p>
                </dd>

                <dt>Supprimer des valeurs</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(1)</IC>
                    </p>
                    <p>
                        La suppression de valeurs fonctionne de façon similaire à l'ajout de valeur dans la liste.
                        Lorsque la suppression se fait en début ou en fin de liste, elle est très efficace. Si vous
                        voulez retirer des données en milieu de tableau, vous devez d'abords trouver le noeud, ce
                        qui n'est pas toujours très efficace.
                    </p>
                    <CodeBlock language="csharp">{retirer}</CodeBlock>
                </dd>

                <dt>Parcourir</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(n)</IC>
                    </p>
                    <p>
                        Si vous devez parcourir vous-même une liste, vous pouvez facilement le faire à l'aide d'une
                        boucle à itérateur, comme si c'était un tableau. Il est possible de le faire avec une boucle
                        régulière, mais il faut travailler un peu avec les noeuds.
                    </p>
                    <CodeBlock language="csharp">{parcourir}</CodeBlock>
                    <ColoredBox heading="Attention">
                        Vous noterez que nous n'utilisons pas <IC>.Length</IC> pour avoir la taille de la liste. En
                        effet, dans une liste, c'est la propriété <IC>.Count</IC> qui nous permet de savoir combien
                        de valeurs se retrouvent dans notre structure.
                    </ColoredBox>
                </dd>
            </dl>
        </section>

        <section>
            <h2>Documentation</h2>
            <p>
                Si vous voulez voir plus de fonctionnalité et de possibités avec la liste chaînée du langage C#, je
                vous recommande fortement d'aller visiter sa page de documentation. Cela vous permettra de voir toutes
                les méthodes disponibles et d'apprendre à mieux les utiliser.
            </p>
            <p>
                <a target="_blank" rel="noopener noreferrer" href="https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.linkedlist-1?view=net-5.0">
                    C# LinkedList&lt;T&gt; Class
                </a>
            </p>
            <p>
                <a target="_blank" rel="noopener noreferrer" href="https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.linkedlistnode-1?view=net-5.0">
                    C# LinkedListNode&lt;T&gt; Class
                </a>
            </p>
        </section>
    </>
}
