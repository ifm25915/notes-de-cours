import React from 'react';
import CodeBlock from '../component/CodeBlock';
import DownloadBlock from '../component/DownloadBlock';

import solution from '../resources/laboratoire-tableau2d-solution.zip';

const donnees =
`[  1,  2,  3,  4,  5,  6 ]
[  7,  8,  9, 10, 11, 12 ]
[ 13, 14, 15, 16, 17, 18 ]
[ 19, 20, 21, 22, 23, 24 ]
[ 25, 26, 27, 28, 29, 30 ]
[ 31, 32, 33, 34, 35, 36 ]`;

export default function LabTableau2D() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                Pour réussir ce laboratoire, créer un petit programme qui résout chaque situation ci-dessous.
            </p>
        </section>

        <section>
            <h2>Problèmes</h2>
            <p>
                <ol>
                    <li>
                        Déclarer un tableau à 2 dimensions de 6 par 6 (36 cases).
                    </li>
                    <li>
                        Remplir le tableau automatiquement avec les données suivantes:
                        <CodeBlock language="shell">{donnees}</CodeBlock>
                    </li>
                    <li>
                        Afficher la matrice (le tableau 2D) à la console.
                    </li>
                    <li>
                        Calculer et afficher la somme du tableau.
                    </li>
                    <li>
                        Calculer et afficher la somme des 2 diagonales.
                    </li>
                </ol>
            </p>
        </section>

        <section>
            <h2>Solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
