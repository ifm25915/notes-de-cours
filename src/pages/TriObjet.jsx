import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const carte =
`class Carte
{
    public enum Sortes { COEUR, CARREAU, TREFLE, PIQUE };

    public Sortes Sorte { get; set; }
    public int Valeur { get; set; }

    public Carte(Sortes sorte, int valeur)
    {
        this.Sorte = sorte;
        this.Valeur = valeur;
    }

    public override string ToString()
    {
        string valeur = this.Valeur switch
        {
            1 => "A",
            11 => "J",
            12 => "Q",
            13 => "K",
            _ => this.Valeur.ToString()
        };

        string sorte = this.Sorte switch
        {
            Sortes.COEUR => "♥",
            Sortes.CARREAU => "♦",
            Sortes.TREFLE => "♣",
            _ => "♠",
        };

        return valeur + sorte;
    }
}`;

const inter =
`class Carte : IComparable<Carte>
{
    // ...
}`;

const compareTo =
`public int CompareTo(Carte autreCarte)
{
    if(this.Sorte < autreCarte.Sorte)
    {
        return -1;
    }
    else if(this.Sorte > autreCarte.Sorte)
    {
        return 1;
    }
    else
    {
        return this.Valeur.CompareTo(autreCarte.Valeur);
    }
}`;

const binary =
`// Tableau trié de carte
Carte[] cartes = new Carte[]{
    new Carte(Carte.Sortes.COEUR, 1),
    new Carte(Carte.Sortes.COEUR, 6),
    new Carte(Carte.Sortes.COEUR, 7),
    new Carte(Carte.Sortes.CARREAU, 10),
    new Carte(Carte.Sortes.TREFLE, 11),
    new Carte(Carte.Sortes.TREFLE, 13),
    new Carte(Carte.Sortes.PIQUE, 5),
    new Carte(Carte.Sortes.PIQUE, 12)
};

// Recherche d'une carte
Carte carteARechercher = new Carte(Carte.Sortes.TREFLE, 11);
int index = Array.BinarySearch(cartes, carteARechercher);

// Affiche 4
Console.WriteLine(index);`;

export default function TriObjet() {
    return <>
        <section>
            <h2>Classe de carte à jouer</h2>
            <p>
                Si vous voulez trier des objets complexes, donc des objets instanciés de classe que vous avez créé,
                vous devrez définir vous-même la façon de les comparer directement dans la classe. Pour vous donner un
                bon exemple, nous utiliserons la classe <IC>Carte</IC> suivante:
            </p>
            <CodeBlock language="csharp">{carte}</CodeBlock>
        </section>

        <section>
            <h2>Ajout de comparaison</h2>
            <p>
                Si nous avons un tableau ou un vecteur de <IC>Carte</IC> et que nous essayons de le trier, nous aurons
                une erreur dans la console. Ceci est dû au fait que C# ne sait pas comment trier les <IC>Carte</IC>.
                Il va donc falloir lui indiquer. Pour ce faire, nous utiliserons l'interface <IC>IComparable</IC>.
                Cette interface indique que notre classe peut être comparé avec elle-même. Vous ajoutez l'interface de
                la façon suivante:
            </p>
            <CodeBlock language="csharp">{inter}</CodeBlock>
            <p>
                L'ajout de l'interface nous ajoute une erreur à notre classe. Pour enlever l'erreur, nous devons
                programmer une méthode <IC>CompareTo</IC> à notre classe. Cette méthode doit comparer la valeur de
                l'objet courant <IC>this</IC> avec un autre élément reçu en paramètre. Comme pour les fonctions de
                comparaison, la méthode <IC>CompareTo</IC> retourne -1, 0 ou 1 pour indiquer la comparaison des 2
                valeurs. Dans le cas de nos cartes à jouer, nous pouvons déclarer cette méthode de la façon suivante:
            </p>
            <CodeBlock language="csharp">{compareTo}</CodeBlock>
            <p>
                Dans le cas ci-dessus, on tri d'abords les cartes par leur sorte. On utilise les valeurs numérique des
                <IC>enum</IC> pour les comparer. Si la sorte de nos 2 éléments est la même, on va alors comparer par
                la valeur en utilisant la méthode <IC>CompareTo</IC> des entiers.
            </p>
        </section>

        <section>
            <h2>Recherche dichotomique</h2>
            <p>
                En mettant l'interface <IC>IComparable</IC> et en programmant la méthode <IC>CompareTo</IC> à une
                classe, on indique que les objets de cette classe peuvent être comparés entre eux. Nous pouvons donc
                utiliser plusieurs algorithmes avec cette classe, dont la recherche dichotomique.
            </p>
            <p>
                Bref, si vous avez un tableau trié d'objets, vous pourrez utiliser la recherche binaire pour trouver
                l'index d'un élément dans le tableau. Bien entendu, cela fonctionne aussi avec les vecteurs.
            </p>
            <CodeBlock language="csharp">{binary}</CodeBlock>
        </section>
    </>
}
