import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

import pile from '../resources/pile.png';

const creation = 
`// Pile d'entiers
Stack<int> pile = new Stack<int>();

// Pile de chaînes de caractères
Stack<string> pile2 = new Stack<string>();

// Pile avec une taille de départ prédéfinie
Stack<int> pile3 = new Stack<int>(20);

// Pile à partir de données existante
// Les données sont empilées dans l'ordre du tableau
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
Stack<int> pile4 = new Stack<int>(tab);`;

const push =
`// Pile
Stack<int> pile = new Stack<int>();

// Empiler des données
pile.Push(1);
pile.Push(3);
pile.Push(3);
pile.Push(7);`;

const pop =
`// Pile
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
Stack<int> pile = new Stack<int>(tab);

// Dépiler des données
// Enlève l'entier 4
pile.Pop();                 

// Enlève l'entier 5, mais mets sa valeur dans la variable
int valeur = pile.Pop();
Console.WriteLine(valeur);`;

const peek =
`// Pile
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
Stack<int> pile = new Stack<int>(tab);

// Regarde l'élément sur le dessus de la pile
// Retourne l'entier 4
int valeur = pile.Peek();
Console.WriteLine(valeur);`;

const parcourir =
`// Pile
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };
Stack<int> pile = new Stack<int>(tab);

// Parcourir la pile
foreach(int valeur in pile)
{
    Console.Write(valeur + ", ");
}`;

export default function FTPile() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                La pile est une structure de données similaire au vecteur, mais avec certaines restrictions. On ne
                peut insérer et retirer que sur le dessus (la fin) de la pile. Bref, si vous voulez retirer un
                élément en plein milieur de la pile, vous devez retirer tous les éléments au dessus (après) cet
                élément. C'est une structure pratique pour représenter certains concepts réels ou pour créer des
                algorithmes plus complexe.
            </p>
            <p>
                On représentera graphiquement la pile de la façon suivante:
            </p>
            <img src={pile} alt="Représentation d'un tableau" />
            <p>
                Dans une pile, on dira que le premier à y entrer est le dernier à en sortir puisque si on empile
                d'autres éléments sur le premier élément, il faudra enlever tous ces éléments avant de retirer notre
                premier élément. Vous verrez donc souvent l'accronyme <IC>F.I.L.O.</IC> en ligne. Il
                signifie <IC>First In Last Out</IC>, donc premier entré, dernier sorti.
            </p>
            <p>
                En C#, une pile se crée avec la classe <IC>Stack</IC> de la façon suivante:
            </p>
            <CodeBlock language="csharp">{ creation }</CodeBlock>
        </section>

        <section>
            <h2>Avantages</h2>
            <ul>
                <li>Les opérations d'ajout et de retrait sont très efficace.</li>
                <li>Très pratique pour les situations nécessitant un ordre <IC>F.I.L.O.</IC> d'entrée et sortie des données</li>
            </ul>
        </section>

        <section>
            <h2>Inconvénients</h2>
            <ul>
                <li>Pas d'accès aléatoires</li>
                <li>Inutile pour les situations n'ayant pas besoin d'un ordre <IC>F.I.L.O.</IC></li>
            </ul>
        </section>

        <section>
            <h2>Opérations</h2>
            <dl>
                <dt>Accès aléatoire</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>N/A</IC>
                    </p>
                    <p>
                        On ne peut pas faire d'accès aléatoire dans une pile. Il faut la convertir dans une autre
                        structure de données pour pouvoir le faire, ce qui n'est pas efficace.
                    </p>
                </dd>

                <dt>Empiler - Push</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(1)</IC>
                    </p>
                    <p>
                        Permet d'empiler un élément sur la pile. Essentiellement, un élément sera ajouté à la fin de
                        la pile. L'ajout dans une pile est toujours très efficace.
                    </p>
                    <CodeBlock language="csharp">{push}</CodeBlock>
                </dd>

                <dt>Dépiler - Pop</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(1)</IC>
                    </p>
                    <p>
                        Permet de dépiler l'élément sur le dessus de la pile. Essentiellement, l'élément à la fin de
                        la pile est retiré. Le retrait dans une pile est toujours très efficace.
                    </p>
                    <p>
                        Lorsqu'on dépile un élément de la pile, cet élément est retourné pour qu'on puisse l'utiliser
                        dans notre traitement.
                    </p>
                    <CodeBlock language="csharp">{pop}</CodeBlock>
                </dd>

                <dt>Voir le haut de la pile - Peek</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(1)</IC>
                    </p>
                    <p>
                        Le seul élément que l'on peut accéder aléatoirement dans une pile est celui sur le dessus,
                        donc à la fin. L'avantage de cette opération est qu'elle nous permet de voir ce qu'il y a sur
                        le dessus de la pile, mais sans avoir à dépiler cet élément.
                    </p>
                    <CodeBlock language="csharp">{peek}</CodeBlock>
                </dd>

                <dt>Parcourir</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(n)</IC>
                    </p>
                    <p>
                        Puisqu'il n'y a pas d'accès aléatoire dans une pile, on peut uniquement la parcourir à l'aide
                        d'une boucle à itérateur. La boucle <IC>foreach</IC> est donc notre ami ici:
                    </p>
                    <CodeBlock language="csharp">{parcourir}</CodeBlock>
                    <ColoredBox heading="Attention">
                        Si vous testez le parcours d'une file, vous remarquerez que les éléments sont parcouru en
                        ordre inverse, soit de l'élément sur le dessus de la pile vers l'élément vers le dessous de la
                        pile. Cela peut être un peu bizarre puisque c'est l'ordre inverse d'insertion dans la pile,
                        donc faites attention!
                    </ColoredBox>
                </dd>
            </dl>
        </section>

        <section>
            <h2>Documentation</h2>
            <p>
                Si vous voulez voir plus de fonctionnalité et de possibités avec la pile du langage C#, je
                vous recommande fortement d'aller visiter sa page de documentation. Cela vous permettra de voir toutes
                les méthodes disponibles et d'apprendre à mieux les utiliser. Vous pourrez y trouver de nombreuses
                fonctionnalités que nous n'avons pas encore couvert.
            </p>
            <p>
                <a target="_blank" rel="noopener noreferrer" href="https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.stack-1?view=net-5.0">
                    C# Stack&lt;T&gt; Class
                </a>
            </p>
        </section>
    </>
}
