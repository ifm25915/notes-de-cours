import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import DownloadBlock from '../component/DownloadBlock';

import distribue from '../resources/laboratoire-hybride-distribué.zip';
import solution from '../resources/laboratoire-hybride-solution.zip';

const insertion =
`triInsertion(tab, debut, fin)
    pour i = debut+1 jusqu'à fin
        elementCle = tab[i]
        j = i
        tant que j > debut et tab[j-1] > elementCle
            tab[j] = tab[j-1]
            j--
            
        tab[j] = elementCle`;

export default function LabSortCompare() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                Dans ce laboratoire, nous créerons une version simplifié du Introsort. Essentiellement, nous créerons
                le Introsort, mais sans regarder la profondeur et sans utiliser le tri par tas puisque nous ne
                l'avons pas vu. Nous avons donc uniquement besoin de regarder si les sous-tableaux du tri rapide sont
                plus petit qu'une certaine valeur, et si c,est le cas, on utilise plutôt le tri par insertion.
            </p>
            <p>
                Compléter le projet distribué ci-dessous:
            </p>
            <ol>
                <li>Télécharger le fichier <IC>distribué.zip</IC>.</li>
                <li>Dézipper le fichier <IC>distribué.zip</IC>.</li>
                <li>
                    Programmer l'algorithme du tri rapide pour des tableaux de nombres entiers.
                </li>
                <li>
                    Programmer l'algorithme de tri par insertion pour des tableaux de nombres entiers. Cette vesion du
                    tri par insertion doit permettre de trier uniquement une partie d'un tableau, donc un
                    sous-tableau. Pour un défi, essayez de le programmer par vous-même. Toutefois, si vous avez des
                    difficultés, fiez-vous au pseudo-code ci-dessous.
                </li>
                <li>
                    Modifier le tri rapide pour que lors son exécution de la fonction auxilière, si la taille du
                    sous-tableau est plus petite que 16, on lance un tri par insertion à la place de faire les
                    opérations normales du tri rapide (choix du pivot, partitionnement et triage des partitions).
                </li>
            </ol>
        </section>

        <section>
            <h2>Tri par insertion de sous-tableaux</h2>
            <p>
                Voici le pseudo-code du tri par insertion permettant de trier une section d'un tableau:
            </p>
            <CodeBlock language="pseudo">{insertion}</CodeBlock>
        </section>

        <section>
            <h2>Distribué et solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ distribue } name="distribué.zip"></DownloadBlock.File>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}