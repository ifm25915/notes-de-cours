import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const puissanceLoop = 
`static double Puissance(double x, int n)
{
    if(n == 0)
    {
        return 1;
    }
    else
    {
        double resultat = x;
        for(int i = 1 ; i < n ; i++)
        {
            resultat *= x;
        }

        return resultat;
    }
}`;

const puissance = 
`static double Puissance(double x, int n)
{
    if(n == 0)
    {
        return 1;
    }
    else if(n % 2 == 0)
    {
        // Pair
        // (x^2)^(n/2)
        return Puissance(x * x, n / 2);
    }
    else
    {
        // Impair
        // (x^2)^((n-1)/2) * x
        return Puissance(x * x, (n - 1) / 2) * x;
    }
}`;

const puissanceInfini = 
`static double Puissance(double x, int n)
{
    if(n == 0)
    {
        return 1;
    }
    else if(n % 2 == 0)
    {
        // Pair
        // (x^2)^(n/2)
        return Puissance(Puissance(x, 2), n / 2);
    }
    else
    {
        // Impair
        // (x^2)^((n-1)/2) * x
        return Puissance(Puissance(x, 2), (n - 1) / 2) * x;
    }
}`;

const puissanceInefficace = 
`static double Puissance(double x, int n)
{
    if(n == 0)
    {
        return 1;
    }
    else if(n % 2 == 0)
    {
        // Pair
        // x^(n/2) * x^(n/2)
        return Puissance(x, n / 2) * Puissance(x, n / 2);
    }
    else
    {
        // Impair
        // x^((n-1)/2) * x^((n-1)/2) * x
        return Puissance(x, (n - 1) / 2) * Puissance(x, (n - 1) / 2) * x;
    }
}`;

export default function RecursiveHanoi() {
    return <>
        <section>
            <h2>Calcul de l'exposant</h2>
            <p>
                L'exposant, ou la puissance est une opération mathématique permettant de multiplier un nombre par 
                lui-même un certain nombre de fois. On l'écrit <IC>x<sup>n</sup></IC>. La création d'une fonction pour 
                calculer l'exposant entier <IC>n</IC> d'un nombre <IC>x</IC> est très simple.
            </p>
            <CodeBlock language="csharp">{puissanceLoop}</CodeBlock>
            <p>
                Dans cette solution, nous ferons un total de <IC>n - 1</IC> multiplications, où <IC>n</IC> est la 
                valeur de l'exposant. Bref, 2<sup>8</sup> fait 7 multiplications.
            </p>
        </section>

        <section>
            <h2>Réduire le nombre de multiplication</h2>
            <p>
                On peut réduire le nombre de multiplication de l'algorithme précédant en utilisant la propriété 
                suivante:
            </p>
            <div style={{display: 'flex', padding: '.5rem', margin: '0 0 1rem', fontFamily: 'monospace', border: '2px solid var(--border-interactive-color)'}}>
                <div>
                    <div>Si n est pair</div>
                    <div>Si n est impair</div>
                </div>
                <div style={{marginLeft: '2rem'}}>
                    <div>x<sup>n</sup> = x<sup>n/2</sup> * x<sup>n/2</sup></div>
                    <div>x<sup>n</sup> = x<sup>(n-1)/2</sup> * x<sup>(n-1)/2</sup> * x</div>
                </div>
            </div>
            <p>
                Voici un exemple pour un nombre pair et un nombre impair:
            </p>
            <div style={{padding: '.5rem', margin: '0 0 1rem', fontFamily: 'monospace', border: '2px solid var(--border-interactive-color)'}}>
                <p>
                    2<sup>6</sup> = 2<sup>6/2</sup> * 2<sup>6/2</sup> = 2<sup>3</sup> * 2<sup>3</sup> = 8 * 8 = 64
                </p>
                <div>
                    2<sup>7</sup> = 2<sup>(7-1)/2</sup> * 2<sup>(7-1)/2</sup> * 2 = 2<sup>3</sup> * 2<sup>3</sup> * 2 = 8 * 8 * 2 = 128
                </div>
            </div>
            <p>
                Nous pouvons aussi utiliser certaines propriétés mathématiques de l'exposant pour encore continuer de 
                réduire nos multiplications:
            </p>
            <div style={{padding: '.5rem', margin: '0 0 1rem', fontFamily: 'monospace', border: '2px solid var(--border-interactive-color)'}}>
                x<sup>a</sup> * x<sup>a</sup> = (x<sup>a</sup>)<sup>2</sup> = (x<sup>2</sup>)<sup>a</sup>
            </div>
            <p>
                Avec cette propriété, nous pouvons donc réduire nos 2 énoncé ci-dessus:
            </p>
            <div style={{display: 'flex', padding: '.5rem', margin: '0 0 1rem', fontFamily: 'monospace', border: '2px solid var(--border-interactive-color)'}}>
                <div>
                    <div>Si n est pair</div>
                    <div>Si n est impair</div>
                </div>
                <div style={{marginLeft: '2rem'}}>
                    <div>x<sup>n/2</sup> * x<sup>n/2</sup> = (x<sup>n/2</sup>)<sup>2</sup> = (x<sup>2</sup>)<sup>n/2</sup></div>
                    <div>x<sup>(n-1)/2</sup> * x<sup>(n-1)/2</sup> * x = (x<sup>(n-1)/2</sup>)<sup>2</sup> * x = (x<sup>2</sup>)<sup>(n-1)/2</sup> * x</div>
                </div>
            </div>
            <p>
                En utilisant ces propriétés et la récursivité, on arrive donc à l'algorithme suivant:
            </p>
            <CodeBlock language="csharp">{puissance}</CodeBlock>
            <p>
                L'algorithme récursif réduit considérablement le nombre de multiplication. Par exemple, 
                2<sup>62</sup> exécute 61 multiplication avec l'algorithme qui utilise une boucle. L'algorithme 
                récursif, quant à lui, en fait simplement 9.
            </p>
            <p>
                Cette réduction de multiplication semble être superbe, mais généralement, tout gain de performance 
                vient avec un inconvénient. Dans ce cas-ci, l'algorithme récursif nécessite beaucoup plus de mémoire 
                à cause de la pile d'appel que l'algorithme utilisant une boucle.
            </p>
        </section>

        <section>
            <h2>Implantation incorrectes ou inefficaces</h2>
            <p>
                Dans l'exemple ci-dessous, nous remplaçons les <IC>x * x</IC> par des appels récursifs à notre 
                fonction. Toutefois, ces appels récursifs n'auront jamais de condition de sortie et remplirons la 
                pile d'appel avec une récursivité infini. Nous aurions le même résultat si nous échangeons les 2 
                exposants de place, tel que d.montré avec la propriété mathématique ci-dessus.
            </p>
            <CodeBlock language="csharp">{puissanceInfini}</CodeBlock>
            <p>
                Dans ce second exemple, nous utilisons le même nombre de multiplication, mais on ajoute un appel 
                récursif de plus à chaque exécution de la fonction, ce qui n'est vraiment pas efficace en mémoire.
            </p>
            <CodeBlock language="csharp">{puissanceInefficace}</CodeBlock>
            <p>
                Bref, exploitez la récursivité avec prudence!
            </p>
        </section>
    </>
}
