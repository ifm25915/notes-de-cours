import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import Video from '../component/Video';

import dictionnaireOrdre from '../resources/dictionnaireordre.png';
import dictionnaireArbre from '../resources/dictionnairearbre.png';

const creation = 
`// Dictionnaire ordonné avec une clé en  
// chaine de caractère et une valeur entière
SortedDictionary<string, int> dictionnaire = new SortedDictionary<string, int>();

// Dictionnaire ordonné avec une clé entière  
// et une valeur en objet d'une classe 
// quelconque nommée "Carte"
SortedDictionary<int, Carte> dictionnaire2 = new SortedDictionary<int, Carte>());`;

const add =
`// Dictionnaire ordonné
SortedDictionary<string, string> dictionnaire = new SortedDictionary<string, string>();

// Ajoute des valeurs dans le dictionnaire
dictionnaire.Add(
    "programmeur", 
    "Personne chargée de la préparation, de l'écriture et de la mise au point d'un programme pour ordinateur."
);
dictionnaire.Add(
    "ordinateur", 
    "Machine automatique de traitement de l'information, obéissant à des programmes formés par des suites d'opérations arithmétiques et logiques."
);

// Ajoute avec l'opérateur d'indexation
dictionnaire["donnée"] = "Représentation conventionnelle d'une information en vue de son traitement informatique.";

// Ne fonctionne pas et lance une exception  
// puisque la clé existe déjà
dictionnaire.Add(
    "programmeur", 
    "Quelqu'un comme Jonathan."
);`;

const remove =
`// Dictionnaire ordonné
SortedDictionary<string, string> dictionnaire = new SortedDictionary<string, string>();
dictionnaire.Add("2266282395", "Le Seigneur des Anneaux - Tome 1 La freternité de l'anneau");
dictionnaire.Add("2070584623", "Harry Potter à l'école des sorciers");
dictionnaire.Add("8822800346", "Le Petit Prince - Avec les dessins de l'auteur");
dictionnaire.Add("0804139024", "The Martian: A Novel");
dictionnaire.Add("2207303403", "Le guide du voyageur galactique");

// Retire des valeurs dans le dictionnaire
dictionnaire.Remove("2266282395");
dictionnaire.Remove("0804139024");
Console.WriteLine(dictionnaire.Count);

// Retourne "false"
bool reussi = dictionnaire.Remove("test");
Console.WriteLine(reussi);`;

const modify =
`// Dictionnaire ordonné
SortedDictionary<string, string> dictionnaire = new SortedDictionary<string, string>();
dictionnaire.Add("2266282395", "Le Seigneur des Anneaux - Tome 1 La freternité de l'anneau");
dictionnaire.Add("2070584623", "Harry Potter à l'école des sorciers");
dictionnaire.Add("8822800346", "Le Petit Prince - Avec les dessins de l'auteur");
dictionnaire.Add("0804139024", "The Martian: A Novel");
dictionnaire.Add("2207303403", "Le guide du voyageur galactique");

// Modifie la valeur dans le dictionnaire
dictionnaire["2070584623"] = "H4rrY P0tt3r";

// La valeur n'existe pas, elle est donc ajouté
dictionnaire["2012248403"] = "Monsieur Rêve";`;

const recherche =
`// Dictionnaire ordonné
SortedDictionary<string, string> dictionnaire = new SortedDictionary<string, string>();
dictionnaire.Add("2266282395", "Le Seigneur des Anneaux - Tome 1 La freternité de l'anneau");
dictionnaire.Add("2070584623", "Harry Potter à l'école des sorciers");
dictionnaire.Add("8822800346", "Le Petit Prince - Avec les dessins de l'auteur");
dictionnaire.Add("0804139024", "The Martian: A Novel");
dictionnaire.Add("2207303403", "Le guide du voyageur galactique");

// Recherche du livre avec le ISBN 0804139024
string livre = dictionnaire["0804139024"];
Console.WriteLine(livre);

// La recherche d'un livre dont le ISBN n'est
// pas dans le dictionnaire lance une 
// exception
string livre2 = dictionnaire["0123456789"];
Console.WriteLine(livre2);

// Si vous n'êtes pas certain si la clé 
// existe ou non, vous pouvez utiliser la 
// méthode "ContainsKey" pour vous assurer
// qu'elle existe avant de faire une recherche
if(dictionnaire.ContainsKey("0123456789"))
{
    Console.WriteLine(dictionnaire["0123456789"]);
}`;

const parcourir =
`// Dictionnaire ordonné
SortedDictionary<string, string> dictionnaire = new SortedDictionary<string, string>();
dictionnaire.Add("2266282395", "Le Seigneur des Anneaux - Tome 1 La freternité de l'anneau");
dictionnaire.Add("2070584623", "Harry Potter à l'école des sorciers");
dictionnaire.Add("8822800346", "Le Petit Prince - Avec les dessins de l'auteur");
dictionnaire.Add("0804139024", "The Martian: A Novel");
dictionnaire.Add("2207303403", "Le guide du voyageur galactique");

// Parcourir
foreach(KeyValuePair<string, string> livre in dictionnaire)
{
    // Afficher la clé
    Console.Write(livre.Key);
    Console.Write(" - ");

    // Afficher la valeur
    Console.WriteLine(livre.Value);
}`;

export default function FTDictionnaireOrdre() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                Le dictionnaire ordonné est une structure de données basé sur le dictionnaire, mais qui garde un ordre
                pour nos données. Ce concept peut être pratique pour certaines applications, mais vient
                malheureusement avec quelques pertes de performances.
            </p>
            <p>
                On peut représenter graphiquement le dictionnaire ordonné de la façon suivante:
            </p>
            <img src={dictionnaireOrdre} alt="Représentation d'un dictionnaire" />
            <p>
                Le dictionnaire ordonné, dans la mémoire de votre ordinateur, sera sauvegardé sous la forme d'un arbre
                pour être capable de rester trié. C'est pourquoi vous pouvez aussi représenter un dictionnaire ordonné
                de la façon ci-dessous. Dans ce cours, nous ne verrons pas comment l'ordinateur fait pour manipuler
                cet arbre en mémoire.
            </p>
            <img src={dictionnaireArbre} alt="Représentation d'un dictionnaire sous la forme d'un arbre" />
            <p>
                Les opérations comme l'ajout, la modification, la suppression et la recherche de données fonctionnent
                de la même façon dans un dictionnaire ordonné que dans un dictionnaire de base. Ces fonctionnalités
                seront toutefois moins efficace que dans un dictionnaire de base.
            </p>
            <p>
                En C#, un dictionnaire ordonné s'utilise avec la classe <IC>SortedDictionary</IC>. L'ordre des données dépends du
                type de la clé. Les clés en nombres sont en ordre numérique et les clés en chaînes de caractères sont en ordre alphabétique
                unicode. Si le type des clés n'est pas comparable, il faudra leur indiquer une façon de les comparer, ce
                que nous verrons plus tard. Vous pouvez créer un <IC>SortedDictionary</IC> de la façon suivante:
            </p>
            <CodeBlock language="csharp">{ creation }</CodeBlock>
        </section>

        <section>
            <h2>Avantages</h2>
            <ul>
                <li>Les opérations d'ajout et de retrait sont efficace, mais pas autant que dans le dictionnaire de base.</li>
                <li>La recherche d'un élément est efficace, mais pas autant que dans le dictionnaire de base.</li>
                <li>Les données garde un ordre défini par le type de la clé.</li>
            </ul>
        </section>

        <section>
            <h2>Inconvénients</h2>
            <ul>
                <li>Pas d'accès aléatoires.</li>
                <li>Pas de répétition de clés.</li>
            </ul>
        </section>

        <section>
            <h2>Opérations</h2>
            <dl>
                <dt>Accès aléatoire</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>N/A</IC>
                    </p>
                    <p>
                        On ne peut pas faire d'accès aléatoire dans un dictionnaire. Malgré le concept d'ordre, il
                        n'est pas possible de faire un accès aléatoire.
                    </p>
                </dd>

                <dt>Ajouter</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(log n)</IC>
                    </p>
                    <p>
                        Permet d'ajouter une paire de clé/valeur dans le dictionnaire ordonné. L'ajout dans un
                        dictionnaire ordonné est toujours assez efficace.
                    </p>
                    <p>
                        La méthode <IC>Add</IC> fonctionne de la même façon qu'avec le dictionnaire de base. Il est
                        aussi possible d'utiliser l'opérateur d'indexation pour ajouter des clés et valeurs. L'ajout
                        dans le dictionnaire ordonné est moins efficace que celle de le dictionnaire de base.
                    </p>
                    <CodeBlock language="csharp">{add}</CodeBlock>
                </dd>

                <dt>Retirer</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(log n)</IC>
                    </p>
                    <p>
                        Permet de retirer une paire de clé/valeur dans le dictionnaire ordonné. Le retrait dans un
                        dictionnaire ordonné est toujours assez efficace.
                    </p>
                    <p>
                        La méthode <IC>Remove</IC> fonctionne de la même façon qu'avec le dictionnaire de base.
                        Cette opération est toutefois moins efficace que celle de le dictionnaire de base.
                    </p>
                    <CodeBlock language="csharp">{remove}</CodeBlock>
                </dd>

                <dt>Modifier</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(log n)</IC>
                    </p>
                    <p>
                        Permet de modifier une valeur en fonction de sa clé dans le dictionnaire ordonné. La
                        modification dans un dictionnaire ordonné est toujours assez efficace.
                    </p>
                    <p>
                        La modification utilise l'opérateur d'indexation <IC>{'[ ... ]'}</IC> de la même façon que
                        dans le dictionnaire de base. Cette opération est toutefois moins efficace que celle de le 
                        dictionnaire de base.
                    </p>
                    <CodeBlock language="csharp">{modify}</CodeBlock>
                </dd>

                <dt>Recherche</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(log n)</IC>
                    </p>
                    <p>
                        Dans un dictionnaire ordonné, la recherche correspond à trouver une valeur en fonction de sa
                        clé. La recherche dans un dicitonnaire ordonné est toujours assez efficace.
                    </p>
                    <p>
                        La recherche utilise l'opérateur d'indexation <IC>{'[ ... ]'}</IC> de la même façon que
                        dans le dictionnaire de base. Cette opération est toutefois moins efficace que celle de le 
                        dictionnaire de base. Il est aussi possible d'utiliser la méthode <IC>ContainsKey</IC>, mais
                        celle-ci est aussi affecté par la baisse d'efficacité des autres fonctionnalités.
                    </p>
                    <CodeBlock language="csharp">{recherche}</CodeBlock>
                </dd>

                <dt>Parcourir</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(n)</IC>
                    </p>
                    <p>
                        Puisqu'il n'y a pas d'accès aléatoire dans un dictionnaire ordonné, on peut uniquement le
                        parcourir à l'aide d'une boucle à itérateur. La boucle <IC>foreach</IC> est donc importante
                        ici. Elle fonctionne de la même façon qu'avec le dictionnaire de base.
                    </p>
                    <CodeBlock language="csharp">{parcourir}</CodeBlock>
                    <p>
                        À l'exécution du code ci-dessus, vous noterez que l'ordre de parcour est celui des clés en
                        ordre alphabétique unicode puisque les clés sont des chaînes de caractères et que le
                        dictionnaire reste toujours ordonné.
                    </p>
                </dd>
            </dl>
        </section>

        <section>
            <h2>Documentation</h2>
            <p>
                Si vous voulez voir plus de fonctionnalité et de possibités avec le dictionnaire ordonné du langage C#,
                je vous recommande fortement d'aller visiter sa page de documentation. Cela vous permettra de voir
                toutes les méthodes disponibles et d'apprendre à mieux les utiliser. Vous pourrez y trouver de
                nombreuses fonctionnalités que nous n'avons pas couvert.
            </p>
            <p>
                <a target="_blank" rel="noopener noreferrer" href="https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.sorteddictionary-2?view=net-5.0">
                    C# SortedDictionary&lt;TKey, TValue&gt; Class
                </a>
            </p>
        </section>

        <section>
            <h2>Vidéo</h2>
            <Video title="Le dictionnaire ordonné" src="https://www.youtube.com/embed/BXH1HV4b8o8" />
        </section>
    </>
}
