import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

export default function StructureRestrictive() {
    return <>
        <section>
            <h2>Restrictions</h2>
            <p>
                Les structures vues jusqu'à présent sont encore très générique. Elles servent à contenir des données,
                mais pas plus. On choisi parmi ces structures simplement en regardant leur performance en fonction des
                opération que l'on veut faire. Il existe toutefois des structures que nous choisirons pour leurs types
                d'opérations et de comportement.
            </p>
            <p>
                Prenez par exemple un jeu de carte. Les cartes sont brassées et empilées dans un paquet. En général,
                la seule carte accessible est celle sur le dessus du paquet. De la même façon, le seul endroit où nous
                pouvons rajouter des cartes est sur le dessus du paquet. Ce comportement défini ce qu'on appèle une
                pile. On peut seulement ajouter ou enlever des éléments sur le dessus de celle-ci.
            </p>
            <p>
                Un autre exemple pourrait être le service à l'auto d'un Tim Hortons. Les automobiles entrent dans la
                file d'un côté et vont sortir de l'autre côté une fois leur commande passée et ramassée. En
                programmation, ce comportement est celui d'une file. On entre d'un côté et on sort de l'autre. Le
                premier entré dans la file sera le premier à en sortir éventuellement.
            </p>
            <p>
                Ces 2 structures sont considérées restrictives. Elles imposent des restrictions à l'ajout de données et au
                retrait de données.
            </p>
        </section>
        <section>
            <h2>Pile et file en C#</h2>
            <p>
                Dans ce module, nous verrons comment utiliser la pile et la file. Ces 2 structures sont construites à
                l'aide de tableaux, comme le vecteur et la liste. Encore une fois, le but n'est pas de créer nous-même
                ces structures, mais plutôt de comprendre un peu comment elles fonctionnent et d'être capable de les
                utiliser lorsque nécessaire.
            </p>
            <p>
                En C#, la pile et la file se retrouve dans la même librairie de code que le vecteur et la liste.
                N'oubliez donc pas le <IC>using</IC> dans le haut de votre fichier pour pouvoir les utiliser.
            </p>
            <CodeBlock language="csharp">{'using System.Collections.Generic;'}</CodeBlock>
        </section>
    </>
}
