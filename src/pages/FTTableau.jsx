import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

import tableau from '../resources/tableau.png';

const accesAleatoire = 
`// Tableau
int[] tab = new int[10] { 9, 0, 8, 1, 7, 2, 6, 3, 5, 4 };

// Accès aléatoire
Console.WriteLine(tab[4]);
tab[0] = 10;`;

const recherche =
`/// <summary>
/// Cherche une valeur dans le tableau et retourne l'index
/// de la première occurence de cette valeur dans le tableau.
/// Si le tableau ne contient pas la valeur, l'index -1 est 
/// retourné.
/// </summary>
static int chercherValeur(int[] tableau, int valeurATrouver)
{
    // On boucle au travers du tableau
    for(int i = 0 ; i < tableau.Length ; i++)
    {
        // Si on trouve la valeur, on la retourne
        if(tableau[i] == valeurATrouver)
        {
            return i;
        }
    }

    // Si on ne trouve pas la valeur, on retourne -1
    return -1;
}`;

const ajouter =
`/// <summary>
/// Ajoute une valeur dans un tableau en créant un nouveau tableau
/// plus grand dans lequel on copie toutes les valeurs de l'ancien 
/// tableau et on ajoute la nouvelle valeur.
/// </summary>
static int[] ajouterValeur(int[] tableau, int valeurAAjouter)
{
    // On crée un nouveau tableau avec une taille un peu plus grande
    int[] nouveauTableau = new int[tableau.Length + 1];

    // On copie chaque valeur de l'ancien tableau dans le nouveau
    for (int i = 0; i < tableau.Length; i++){
        nouveauTableau[i] = tableau[i];
    }

    // On ajoute la valeur à rajouter à la fin du nouveau tableau
    nouveauTableau[nouveauTableau.Length - 1] = valeurAAjouter;

    // On retourne le nouveau tableau
    return nouveauTableau;
}`;

export default function FTTableau() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                Le tableau est la structure de données de base de la plupart des langages de programmation. C'est 
                un groupe de données, généralement du même type, qui vont être placé une à côté de l'autre dans la 
                mémoire de l'ordinateur. Mise à part certaines exception, le tableau a une taille fixe et ne peut pas 
                être aggrandi ou rapetissé.
            </p>
            <p>
                On le représentera souvent graphiquement de la façon suivante:
            </p>
            <img src={ tableau } alt="Représentation d'un tableau" />
        </section>

        <section>
            <h2>Avantages</h2>
            <ul>
                <li>Simple à utiliser et ne nécessite généralement pas d'importation de librairie</li>
                <li>Très performant pour l'accès aléatoire</li>
            </ul>
        </section>

        <section>
            <h2>Inconvénients</h2>
            <ul>
                <li>La taille est fixe, on ne peut donc pas ajouté ou enlever de l'espace dedans</li>
                <li>Pour faire des recherches, on doit généralement parcourir toutes les données</li>
                <li>
                    Il peut être difficile de créer des tableaux de taille gigantesque puisque les éléments du tableau
                    sont placé un à côté de l'autre dans la mémoire et qu'il que l'ordinateur ne pourra pas
                    nécessairement trouver ce genre d'espace mémoire facilement
                </li>
            </ul>
        </section>

        <section>
            <h2>Opérations</h2>
            <dl>
                <dt>Accès aléatoire</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(1)</IC>
                    </p>
                    <p>
                        Un des avantages du tableau est que son accès aléatoire est très performante. Comme vous 
                        pouvez le constater par sa complexité algorithmique, l'accès aléatoire se fait en une seule 
                        opération dans un tableau et ce, peu importe sa taille. À ce point-ci, on peut toutefois se 
                        poser comme question: Qu'est ce que l'accès aléatoire?
                    </p>
                    <p>
                        L'accès aléatoire est en fait une opération que vous faites souvent sur un tableau. C'est le 
                        principe d'aller chercher une valeur à un certain index. Par exemple, si vous voulez chercher 
                        ou modifier la valeur à l'index 8 du tableau, c'est un accès aléatoire. En C#, on utilise les 
                        accolades carrées pour faire un accès aléatoire.
                    </p>
                    <CodeBlock language="csharp">{ accesAleatoire }</CodeBlock>
                </dd>

                <dt>Recherche de valeurs</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(n)</IC>
                    </p>
                    <p>
                        Si vous voulez chercher une valeur dans un tableau, ce sera généralement beaucoup moins
                        efficace que l'accès aléatoire. En effet, pour trouver la valeur, il faudra parcourir chaque
                        valeur du tableau et regarder si c'est la bonne valeur. Dans le meileur des cas, la valeur
                        qu'on recherche sera la première valeur, mais dans le pire des cas, il faudra parcourir toutes
                        les valeurs du tableau.
                    </p>
                    <CodeBlock language="csharp">{ recherche }</CodeBlock>
                </dd>

                <dt>Ajout ou suppression de valeurs</dt>
                <dd>
                    <p>
                        Complexité algorithmique: <IC>O(n)</IC>
                    </p>
                    <p>
                        Un tableau ne peut pas s'aggrandir ou se rapetisser. La seule façon de changer la taille d'un
                        tableau est d'en créer un nouveau plus grand ou plus petit et de copier toutes les valeurs
                        dans le nouveau tableau, ce qui n'est pas nécessairement très efficace sur les très gros
                        tableaux.
                    </p>
                    <p>
                        Bien que l'efficacité algorithmique soit simplement <IC>O(n)</IC>, du point de vue de la
                        mémoire, c'est une opération coûteuse puisque l'on doit toujours recréer et supprimer des
                        tableaux de taille différente.
                    </p>
                    <CodeBlock language="csharp">{ ajouter }</CodeBlock>
                </dd>
            </dl>
        </section>
    </>
}
