import React from 'react';
import CodeBlock from '../component/CodeBlock';
import DownloadBlock from '../component/DownloadBlock';

import solution from '../resources/laboratoire-tableau-solution.zip';

const io =
`// Affichage dans la console
Console.Write("Allo\n");
Console.WriteLine("Je suis dans la console");

// Lecture de la console
string chaineEntree = Console.ReadLine();

// Convertir une chaîne de caractères dans
// un autre type
int valeurConvertie;
bool reussi = int.TryParse(chaineEntree, out valeurConvertie);

// Formatage de chaîne de caractère
Console.Write(
    String.Format(
        "Valeur entière {0} dans une chaîne.",
        valeurConvertie
    )
);

// Attendre que l'utilisateur appuie
// sur une touche pour continuer
Console.WriteLine("Appuyez sur une touche pour continuer");
Console.ReadKey();`;

export default function LabTableau() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                Pour réussir ce laboratoire, créer un petit programme qui résout chaque situation ci-dessous. Pour les
                entrées et sortie dans la console, n'oubliez pas ces fonctions pratiques de C#:
            </p>
            <CodeBlock language="csharp">{io}</CodeBlock>
        </section>

        <section>
            <h2>Problèmes</h2>
            <p>
                <ol>
                    <li>
                        Écrire un programme qui demande à l'utilisateur d'entrer la taille d'un tableau. La taille
                        doit être au maximum de 50. Par la suite, le programme doit créer ce tableau et demander à
                        l'utilisateur d'entrer un nombre entier pour remplir chaque cellule du tableau.
                    </li>
                    <li>
                        Écrire une fonction qui affiche le contenu d'un tableau sous le format suivant:
                        <CodeBlock language="csharp">{'[ 4, 13, 1, 5, 9 ]'}</CodeBlock>
                    </li>
                    <li>
                        Écrire une fonction qui permet de calculer et d'afficher la somme d'un tableau d'entier passé
                        en paramètre.
                    </li>
                    <li>
                        Écrire 2 fonctions qui permettent de déterminer la plus grande et la plus petite valeur d'un
                        tableau d'entier passé en paramètre. Ces fonctions doivent aussi afficher la plus grande et la
                        plus petite valeur ainsi que sa position dans le tableau à la console.
                    </li>
                    <li>
                        Écrire un programme qui demande à l'utilisateur d'entrer un nombre entier. Le programme doit
                        rechercher cette valeur dans un tableau d'entier et afficher l'index de la valeur trouvée dans
                        la console. Si la valeur ne se retrouve pas dans le tableau, le programme doit l'indiquer.
                    </li>
                </ol>
            </p>
        </section>

        <section>
            <h2>Solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
