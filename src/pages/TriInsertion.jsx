import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

import triinsertion1 from '../resources/insertion1.png';
import triinsertion2 from '../resources/insertion2.png';
import triinsertion3 from '../resources/insertion3.png';

const insertion =
`triInsertion(tab)
    n = taille de tab
    pour i = 1 jusqu'à n-1
        elementCle = tab[i]
        j = i
        tant que j > 0 et tab[j-1] > elementCle
            tab[j] = tab[j-1]
            j--
            
        tab[j] = elementCle`;

export default function TriInsertion() {
    return <>
        <section>
            <h2>Utilité et fonctionnement</h2>
            <p>
                Le tri par insertion est considéré comme le tri le plus efficace pour de petites quantités de données.
                En général, si vous avez moins d'une quinzaine de données, c'est le meilleur tri pour vos besoins. Il
                est aussi très performant si les données sont déjà triées ou presque triées. Pour ces raisons, le tri
                par insertion est fréquement utilisé en combinaison avec d'autres méthodes de tri dans des algorithmes
                de tri hybride. Le tri par insertion s'exécute en <IC>O(n<sup>2</sup>)</IC>
            </p>
            <p>
                Le tri par insertion est un algorithme de triage classique dont le principe est très similaire à celui
                que la plupart des personnes utilisent naturellement pour trier une main de carte à jouer. Par
                exemple, prendre les cartes mélangées une à une sur la table, et former une main
                en <strong>insérant</strong> chaque carte à sa place.
            </p>
        </section>

        <section>
            <h2>Insertion en image</h2>
            <p>
                Le tri est plus facile à comprendre avec des images. Dans le tableau de données ci-dessous, nous
                sélectionnons un élément clé à trier. Au début de l'algorithme, cet élément est toujours l'élément à
                la position 1 de l'ensemble de données. On place ensuite cet élément à la bonne place parmi les
                éléments qui le précède. Dans le cas ci-dessous, nous déplaçons l'élément clé <IC>4</IC> avant
                l'élément 5 puisqu'il est plus petit.
            </p>
            <img src={triinsertion1} alt="1ère étape d'un tri par insertion" />
            <p>
                Le tri passe ensuite au prochain élément clé à trier. Cet élément est toujours le suivant du dernier
                élément clé. Comme pour l'étape d'avant, on va placer cet élément clé à la bonne place parmi les
                éléments qui sont avant celui-ci.
            </p>
            <img src={triinsertion2} alt="2ème étape d'un tri par insertion" />
            <p>
                On a ensuite à répéter ces étapes pour chaque élément clé un après l'autre, ce qui triera notre
                tableau. Comme vous pouvez le constater, les éléments avant l'élément clé sont toujours triés alors
                que les éléments après l'élément clé ne sont pas nécessairement encore triés.
            </p>
            <img src={triinsertion3} alt="Restant des étapes d'un tri par insertion" />
        </section>

        <section>
            <h2>Pseudo-code</h2>
            <p>
                Comme pour les tri à bulles et tri par sélection, le tri par insertion parcourt le conteneur du début
                à la fin. Au moment où il considère un élément clé à trier, les éléments qui le précèdent sont déjà
                triés alors que l'élément clé et ceux qui le suivent ne le sont pas encore.
            </p>
            <p>
                L'objectif d'une étape est donc d'insérer l'élément clé à sa place parmi ceux qui précèdent. Il faut
                pour cela trouver où l'élément doit être inséré en le comparant aux autres, puis décaler les éléments
                afin de pouvoir effectuer l'insertion. En pratique, ces deux actions sont fréquemment effectuées en
                une passe, qui consiste à faire « remonter » l'élément au fur et à mesure jusqu'à rencontrer un
                élément plus petit.
            </p>
            <CodeBlock language="pseudo">{insertion}</CodeBlock>
        </section>
    </>;
}
