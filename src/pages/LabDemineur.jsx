import React from 'react';
import IC from '../component/InlineCode';
import DownloadBlock from '../component/DownloadBlock';

import distribue from '../resources/laboratoire-demineur-distribué.zip';
import solution from '../resources/laboratoire-demineur-solution.zip';

export default function LabDemineur() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                Vous devez compléter un jeu simple de démineur. Le jeu consite à afficher une grille avec des mines
                dedans. À chaque tour, le joueur devra choisir des coordonnées à déminer. Si ces coordonnées
                correspondent à une mine, la mine sera déminée. Ce laboratoire vous aideras à vous exercer avec les
                listes chaînées. Pour réussir ce laboratoire, suivez les étapes suivantes:
            </p>
            <ol>
                <li>Télécharger le fichier <IC>distribué.zip</IC>.</li>
                <li>Dézipper le fichier <IC>distribué.zip</IC>.</li>
                <li>
                    Écrire le code de la fonction <IC>GenererMines</IC> dans le fichier <IC>Program.cs</IC>.
                    <ul>
                        <li>La fonction doit générer un nombre de mines défini dans le paramètre <IC>nombre</IC>.</li>
                        <li>Pour créer une mine, utiliser le constructeur de la classe <IC>Mine</IC> et passez lui des valeurs aléatoires de X et de Y.</li>
                        <li>Les valeurs aléatoire de X et de Y doivent être entre 0 et <IC>TAILLE_GRILLE - 1</IC>.</li>
                        <li>Les mines générées doivent être ajouté dans la liste de mines <IC>mines</IC>.</li>
                        <li>Si une mine générée a les mêmes coordonnées qu'un autre mine dans la liste, vous devez la regénérer.</li>
                    </ul>
                </li>
                <li>
                    Écrire le code de la fonction <IC>Deminer</IC> dans le fichier <IC>Program.cs</IC>
                    <ul>
                        <li>La fonction doit vérifier si les coordonnées <IC>x</IC> et <IC>y</IC> passées en paramètre correspondent à une mine dans la liste.</li>
                        <li>Si les coordonnées <IC>x</IC> et <IC>y</IC> correspondent à une mine, on retire la mine de la liste chaînée.</li>
                        <li>On doit ajouter les coordonnées du déminage dans la liste <IC>deminage</IC>. Vous devrez créer une nouvelle coordonnée et l'ajouter dans la liste.</li>
                    </ul>
                </li>
                <li>
                    Écrire le code de la fonction <IC>DeminerComplet</IC> dans le fichier <IC>Program.cs</IC>
                    <ul>
                        <li>La fonction doit vérifier si toutes les mines dans la liste sont déminées. Bref, regarder si la liste est vide ou non.</li>
                        <li>La fonction doit retourner <IC>true</IC> si la liste de mines est vide et <IC>false</IC> autrement.</li>
                    </ul>
                </li>
            </ol>
        </section>

        <section>
            <h2>Distribué et solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ distribue } name="distribué.zip"></DownloadBlock.File>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
