import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

const struct = 
`// Structure de données
int[] tab = new int[10] { 0, 1, 2, 3, 4, 5, 6,7, 8, 9 };`;

const whileLoop = 
`// Boucle while
int a = 0;
while(a < tab.Length)
{
    Console.Write(tab[a]);
    a++;
}`;

const dowhileLoop = 
`// Boucle do / while
int b = 0;
do
{
    Console.Write(tab[b]);
    b++;
} while(b < tab.Length);`;

const forLoop = 
`// Boucle for
for(int c = 0 ; c < tab.Length ; c++)
{
    Console.Write(tab[c]);
}`;

const foreachLoop = 
`// Boucle foreach
foreach(int valeur in tab){
    Console.Write(valeur);
}`;

export default function BoucleIterateur() {
    return <>
        <section>
            <h2>Boucles traditionnelles</h2>
            <p>
                Lorsqu'il est temps de parcourir les éléments d'une structure de données, nous utiliserons en général 
                une boucle. Le langage C# peut utiliser les 3 boucles traditionnelles, soit le <IC>while</IC>, 
                le <IC>do/while</IC> et le <IC>for</IC>. Pour le tableau suivant, les 3 programmes font exactement la 
                même chose:
            </p>
            <CodeBlock language="csharp">{ struct }</CodeBlock>
            <CodeBlock language="csharp">{ whileLoop }</CodeBlock>
            <CodeBlock language="csharp">{ dowhileLoop }</CodeBlock>
            <CodeBlock language="csharp">{ forLoop }</CodeBlock>
        </section>

        <section>
            <h2>Boucle à itérateurs</h2>
            <p>
                Il existe une autre façon de parcourir des structures de données, soit l'utilisation d'itérateurs. Un 
                itérateur est un objet particulier qui nous permet de naviguer dans une structure de données. Dans 
                certains langages, comme le C++, les itérateurs doivent être utilisés manuellement. Heureusement, en 
                C#, les itérateurs sont utilisable de façon invisible avec la boucle <IC>foreach</IC>.
            </p>
            <CodeBlock language="csharp">{ foreachLoop }</CodeBlock>
            <p>
                Comme vous pouvez le constater, la boucle <IC>foreach</IC> est très simple à utiliser. En fait, en 
                arrière-plan, elle crée un objet itérateur pour boucler sur la structure. Vous verrez plus tard que 
                certaines structures nécessitent absolument un itérateur pour parcourir ses éléments. Je vous 
                encourage donc à essayer la boucle <IC>foreach</IC> pour être à l'aise avec son utilisation.
            </p>
            <ColoredBox heading="Attention">
                Les itérateurs sont fait pour lire les données, mais pas les modifier. Si vous voulez modifier les
                données dans une structure de données, la boucle <IC>foreach</IC> n'est pas la solution.
            </ColoredBox>
        </section>
    </>
}
