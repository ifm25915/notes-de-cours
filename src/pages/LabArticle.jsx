import React from 'react';
import IC from '../component/InlineCode';
import DownloadBlock from '../component/DownloadBlock';

import solution from '../resources/laboratoire-article-solution.zip';

export default function LabArticle() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                Le but est de créer deux ensembles non ordonnés que vous devez remplir avec des valeurs à partir de
                d'une énumération nommée <IC>Article</IC>.
            </p>
            <p>
                Compléter le projet distribué ci-dessous:
            </p>
            <ol>
                <li>Créer un projet console vide nommé <IC>Article</IC>.</li>
                <li>
                    Dans votre programme, créer une énumération <IC>enum</IC> nommée <IC>Article</IC> d'articles contenant les valeurs
                    suivantes:
                    <div>
                        Camera, Cellulaire, Clavier, Ecran, Imprimante, Ordinateur, Radio, Scanner, Souris, Tablette,
                        TV
                    </div>
                </li>
                <li>
                    Créer 2 ensembles non triés d'article vide nommés <IC>E1</IC> et <IC>E2</IC>.
                </li>
                <li>
                    Demander à l'utilisateur de choisir un certain nombre article à mettre dans les
                    ensembles <IC>E1</IC> et <IC>E2</IC>. L'utilisateur peut choisir au maximum 10 article à mettre
                    dans chacun des ensembles.
                </li>
                <li>
                    Créer et afficher une liste chaînée contenant l'union triée des 2 ensembles.
                </li>
                <li>
                    Créer et afficher une pile contenant l'intersection triée des 2 ensembles.
                </li>
                <li>
                    Créer et afficher une file contenant la différence symétrique triée des 2 ensembles.
                </li>
            </ol>
        </section>
        
        <section>
            <h2>Exemple</h2>
            <dl>
                <dt>E1</dt>
                <dd>{'{'} Ordinateur, Clavier, TV, Cellulaire {'}'}</dd>

                <dt>E2</dt>
                <dd>{'{'} Scanner, Souris, Camera, Cellulaire, Écran,TV {'}'}</dd>

                <dt>Liste</dt>
                <dd>Camera → Cellulaire → Clavier → Ecran → Ordinateur → Scanner → Souris → TV</dd>

                <dt>Pile</dt>
                <dd>Cellulaire</dd>

                <dt>File</dt>
                <dd>Camera, Clavier, Ecran, Ordinateur, Scanner, Souris</dd>
            </dl>
        </section>

        <section>
            <h2>Distribué et solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
