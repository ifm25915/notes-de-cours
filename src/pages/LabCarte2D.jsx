import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import DownloadBlock from '../component/DownloadBlock';

import distribue from '../resources/laboratoire-carte2d-distribué.zip';
import solution from '../resources/laboratoire-carte2d-solution.zip';

const carteConsole =
`[┼] [~] [^] [^] [^] [~] [^] [▒] 
[┼] [~] [^] [┼] [~] [┼] [~] [^]
[┼] [~] [~] [▒] [▒] [┼] [~] [┼]
[^] [~] [~] [┼] [┼] [▒] [┼] [~]
[▒] [^] [~] [~] [^] [▒] [▒] [▒]
[▒] [┼] [┼] [~] [▒] [▒] [┼] [▒]`;

const carteConsoleIndex =
`    0   1   2   3   4   5   6   7   
0   [┼] [~] [^] [^] [^] [~] [^] [▒] 
1   [┼] [~] [^] [┼] [~] [┼] [~] [^]
2   [┼] [~] [~] [▒] [▒] [┼] [~] [┼]
3   [^] [~] [~] [┼] [┼] [▒] [┼] [~]
4   [▒] [^] [~] [~] [^] [▒] [▒] [▒]
5   [▒] [┼] [┼] [~] [▒] [▒] [┼] [▒]`;

export default function LabCarte2D() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                Vous faites un prototype de grille de jeu pour une idée de jeu vidéo futur. Votre prototype se fait
                dans la console. Complété le projet distribué ci-dessous:
            </p>
            <ol>
                <li>Télécharger le fichier <IC>distribué.zip</IC>.</li>
                <li>Dézipper le fichier <IC>distribué.zip</IC>.</li>
                <li>
                    Écrire le code de la fonction <IC>remplirCarte</IC> dans le fichier <IC>Program.cs</IC>.
                    <ul>
                        <li>La fonction doit remplir chaque cellule de la grille en paramètre avec un symbole aléatoirement.</li>
                        <li>Les symboles accepté sont le <IC>^</IC>, <IC>~</IC>, <IC>▒</IC> et <IC>┼</IC>.</li>
                        <li>Vous pouvez copier/coller les symboles directement dans l'éditeur de code.</li>
                        <li>Pour générer un nombre aléatoire, créer un randomisateur avec la classe <IC>Random</IC>.</li>
                    </ul>
                </li>
                <li>
                    Écrire le code de la fonction <IC>afficherCarte</IC> dans le fichier <IC>Program.cs</IC>
                    <ul>
                        <li>
                            La fonction doit afficher la grille de la façon similaire à celle ci-dessous:
                            <CodeBlock language="shell">{ carteConsole }</CodeBlock>
                        </li>
                        <li>
                            Assurez-vous que chaque colonne est bien aligné avec elle-même.
                        </li>
                        <li>
                            Les rangées et les colonnes doivent être affiché dans le bon sens, donc assurez-vous de
                            ne pas les inverser.
                        </li>
                        <li>
                            Vous pouvez utiliser <IC>String.Format()</IC> pour vous aider à afficher les cellules
                            dans la console.
                        </li>
                    </ul>
                </li>
            </ol>
        </section>

        <section>
            <h2>Bonus</h2>
            <p>
                Si vous avez fini trop vite et qu'il vous reste du temps, voici quelques petits détails que vous
                pouvez rajouter pour complexifier et améliorer votre programme:
            </p>
            <ul>
                <li>
                    Ajouter les index devant les rangées et colonne.
                    <CodeBlock language="shell">{ carteConsoleIndex }</CodeBlock>
                </li>
                <li>
                    Modifier la couleur des cellules dans la console dépendant du symbole affiché avec les
                    instructions <IC>Console.BackgroundColor</IC> et <IC>Console.ForegroundColor</IC>.
                </li>
                <li>
                    Ajouter au programme une boucle qui permet à l'utilisateur de choisir une cellule de la carte et
                    d'en modifier le symbole pour un autre des symboles acceptés. Notre application devient donc un
                    éditeur de carte!
                </li>
            </ul>
        </section>

        <section>
            <h2>Distribué et solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ distribue } name="distribué.zip"></DownloadBlock.File>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
