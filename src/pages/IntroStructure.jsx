import React from 'react';

export default function IntroStructure() {
    return <>
        <section>
            <h2>Les données</h2>
            <p>
                Les données, en informatique, sont le principal bloc de construction de nos programmes. En effet, tout
                programme va gérer et manipuler des données. Voici quelques exemples:
            </p>
            <ul>
                <li>
                    Un éditeur de texte permet de manipuler le texte, mais aussi son formattage, comme sa taille ou sa 
                    couleur.
                </li>
                <li>
                    Un explorateur de fichier nous permet de manipuler l'emplacement des fichiers sur votre 
                    ordinateur, mais aussi de changer leur nom et de les supprimer.
                </li>
                <li>
                    Un jeu vidéo modifie constamment plusieurs données, comme la vitesse d'un personnage, sa direction 
                    ou encore sa quantité de munition.
                </li>
            </ul>
            <p>
                Dans les langages de programmation, les données sont sauvegardé dans des variables. Pour de petites 
                quantités de données, avoir une variable par donnée est suffisant, mais lorsque nous avons beaucoup de 
                données, cela va être problématique. Imaginez devoir créer une variable pour chaque publication sur 
                Instagram ou pour chaque compte sur un jeu vidéo populaire. Une situation comme celle-ci deviendrait 
                rapidement ingérable!
            </p>
            <p>
                C'est ici que les structures de données nous seront utiles.
            </p>
        </section>

        <section>
            <h2>Structure de données</h2>
            <p>
                Une structure de données est une façon de regrouper plusieurs données ensemble. On va les utiliser 
                très souvent dans nos programmes pour mettre des données similaires ensembles. Voici quelques 
                exemples:
            </p>
            <ul>
                <li>
                    Regrouper tous les projectiles dans un jeu vidéo.
                </li>
                <li>
                    Regrouper toutes les rangées d'une table dans une base de données.
                </li>
                <li>
                    Regrouper toutes les images utilisés pour entraîner un intelligence artificiel.
                </li>
                <li>
                    Regrouper tous les mots du dictionnaire avec leurs descriptions.
                </li>
            </ul>
            <p>
                Vous connaissez déjà une structure de données de base: le tableau. 
            </p>
            <p>
                Le tableau n'est toutefois pas 
                une structure adaptée à toutes les situations. Dans ce cours, nous verrons donc de nombreuses autres 
                structures de données qui pourront nous être utile dans nos programmes. Nous apprendrons aussi à 
                choisir nos structures, puisqu'elles ont chacune leurs forces et leurs faiblesses.
            </p>
        </section>
    </>
}
