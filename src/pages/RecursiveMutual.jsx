import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

import recursemutual from '../resources/recursemutual.png';

const sin =
`static double Sinus(double x)
{
    if (x > -0.005 && x < 0.005)
    {
        return x - ((x * x * x) / 6);
    }
    else
    {
        return 2 * Sinus(x / 2) * Cosinus(x / 2);
    }
}`;

const cos =
`static double Cosinus(double x)
{
    if (x > -0.005 && x < 0.005)
    {
        return 1.0 - ((x * x)/2);
    }
    else
    {
        double s = Sinus(x / 2);
        return 1 - (2 * s * s);
    }
}`;

export default function RecursiveMutual() {
    return <>
        <section>
            <h2>Directe et indirecte</h2>
            <p>
                Lorsqu’une fonction s’invoque elle-même, on parle de <IC>récursivité directe</IC>. Toutefois,  il
                existe aussi une récursivité indirecte, quand une fonction invoque d’autre fonctions, qui invoquent
                d’autres fonctions, qui à leur tour finissent par invoquer la fonction initiale.
            </p>
            <img src={recursemutual} alt="Démonstration graphique de la récursivité directe et indirecte" />
        </section>
        <section>
            <h2>Exemple</h2>
            <p>
                Les fonctions <IC>Sinus()</IC> et <IC>Cosinus()</IC> sont de très bons exemple de la récursivité
                mutuelle. En effet, lorsqu'on calcul le sinus ou le cosinus d'un nombre près de zéro, on peut utiliser
                l'approximation par <IC>polinômes de Taylor</IC> de la façon suivante:
            </p>
            <div style={{padding: '.5rem', margin: '0 0 1rem', fontFamily: 'monospace', border: '2px solid var(--border-interactive-color)'}}>
                <div>
                    sin(x) ≈ x - x<sup>3</sup> / 6
                </div>
                <div>
                    cos(x) ≈ 1 - x<sup>2</sup> / 2
                </div>
            </div>
            <p>
                Pour de plus grande valeur de <IC>x</IC>, l'approximation par <IC>polinômes de Taylor</IC> ne
                fonctionne pas bien. Il est toutefois possible d'utiliser les identités mutuellement récursives
                suivantes:
            </p>
            <div style={{padding: '.5rem', margin: '0 0 1rem', fontFamily: 'monospace', border: '2px solid var(--border-interactive-color)'}}>
                <div>
                    sin(x) = 2 * sin(x / 2) * cos(x / 2)
                </div>
                <div>
                    cos(x) = 1 - 2 * (sin(x / 2))<sup>2</sup>
                </div>
            </div>
            <p>
                Si nous combinons toutes ces stratégies, nous arrivons donc au code C# suivant:
            </p>
            <CodeBlock language="csharp">{sin}</CodeBlock>
            <CodeBlock language="csharp">{cos}</CodeBlock>
        </section>
    </>
}
