// Syntax highlighter imports for styles
import vs from 'react-syntax-highlighter/dist/esm/styles/prism/vs';
import allyDark from 'react-syntax-highlighter/dist/esm/styles/prism/a11y-dark';

// Settings object
let settings = {
    naming: {
        title: 'Structures de données appliquées',
        sections: 'Module'
    },
    themes: {
        default: 'light',
        properties: {
            light: {
                bgColor: '#fff',
                bgAccentColor: '#369e46',
                bgAccentGradientColor: '#2d8039',
                bgInteractiveColor: '#636d62',
                bgInteractiveAccentColor: '#deffde',
                bgHighlight: '#d6d6d6',
                bgHighlightAccent: '#d5f5d9',
                borderInteractiveColor: '#369e46',
                borderSeparatorColor: '#c3c3c3',
                textColor: '#222',
                textInvertedColor: '#fff',
                textAsideColor: '#222',
                textSwitcherColor: '#24ab15',
                syntaxHighlightStyle: 'vs'
            },
            dark: {
                bgColor: '#36352e',
                bgAccentColor: '#0d9462',
                bgAccentGradientColor: '#07ad60',
                bgHighlight: '#00694c',
                bgHighlightAccent: '#0d9462',
                bgInteractiveColor: '#c3c3c3',
                bgInteractiveAccentColor: '#c3c3c3',
                borderInteractiveColor: '#0d9462',
                borderSeparatorColor: '#c3c3c3',
                textColor: '#fff',
                textInvertedColor: '#fff',
                textAsideColor: '#c0ffd4',
                textSwitcherColor: '#77e6b3',
                syntaxHighlightStyle: 'allyDark'
            }
        }
    },
    syntaxHiglight: {
        languages: {
            csharp: { name: 'C#', parser: 'csharp' },
            shell: { name: 'Terminal', parser: 'powershell' },
            pseudo: { name: 'Pseudocode' },
        },
        themes: {
            vs: vs,
            allyDark: allyDark
        }
    }
}

export default settings;
export let naming = settings.naming;
export let themes = settings.themes;
export let syntaxHiglight = settings.syntaxHiglight;