import React from 'react';
import useCheckOverflow from './useCheckOverflow';

import styles from './ToTop.module.css';

export default function ToTop() {    
    /**
     * State variable indicating whether the button to top should be visible or 
     * not.
     */
     const visible = useCheckOverflow(1.5);

    /**
     * Scroll the page to the top.
     */
    const scrollToTop = () => {
        window.scrollTo({ top: 0, behavior: 'smooth' });
    }

    return <button className={ styles.button + ' ' + (visible ? '' : styles.hidden) } onClick={ scrollToTop }>
        <i className={ styles.arrow }></i>
    </button>
}
