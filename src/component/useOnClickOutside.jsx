import { useEffect } from 'react';

export default function useOnClickOutside(elementList, listenerList) {
    /**
     * Refs to HTML element that are considered inside our element.
     */
    const elements = new Set(elementList && Array.isArray(elementList) ? elementList : [elementList]);

    /**
     * Listeners to executed when a click outside our element is done.
     */
    const listeners = new Set(listenerList && Array.isArray(listenerList) ? listenerList : [listenerList]);

    /**
     * Executed when there is a click on the page. If the click is outside the 
     * elements, the listeners are executed.
     * @param {Event} event The click event object
     */
    const onClick = (event) => {
        // If one of the elements contains the target, we dont execute the 
        // listeners
        for(let element of elements){
            if(element.current.contains(event.target)){
                return;
            }
        }

        // Execute all listeners
        for (let listener of listeners){
            listener(event);
        }
    }

    /**
     * Register and unregister the mouse click on mount and demount
     */
    useEffect(() => {
        document.addEventListener('mousedown', onClick);

        return () => {
            document.removeEventListener('mousedown', onClick);
        }
    });
}
