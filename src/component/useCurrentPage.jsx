import { useEffect, useState } from "react";
import { useLocation } from "react-router";
import data from '../data';


export default function useCurrentPage() {
    const location = useLocation();

    /**
     * Get the current section from a location URL.
     */
    const getCurrentSection = (location) => {
        // Find the current section from the current route "directory"
        let slashPosition = location.pathname.indexOf('/', 1);
        let directory = location.pathname.substring(1, slashPosition !== -1 ? slashPosition : undefined);
        for(let section of data.sections){
            if(directory === section.id){
                return section;
            }
        }

        return null;
    };

    /**
     * Get the current page from a location URL.
     */
    const getCurrentPage = (location) => {
        // We check if we are in a page in this section
        let slashPosition = location.pathname.indexOf('/', 1);
        if(slashPosition !== location.pathname.length - 1){
            return data.pages[location.pathname.substring(slashPosition + 1)];
        }

        return null;
    };

    const [section, setSection] = useState(getCurrentSection(location));
    const [page, setPage] = useState(getCurrentPage(location));

    // Update the current page and section object at the start and whenever 
    // the location URL changes.
    useEffect(() => {
        // Update current section
        let currentSection = getCurrentSection(location);
        if(section !== currentSection){
            setSection(currentSection);
        }

        // Update current page
        let currentPage = getCurrentPage(location);
        if(page !== currentPage){
            setPage(currentPage);
        }
    }, [location, section, page]);

    return [page, section];
}