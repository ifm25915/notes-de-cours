import React from 'react';

import styles from './Logo.module.css'

export default function Logo() {
    return <div className={ styles.logo }>
        <div className={ styles.title }>
            <div>Structures de données</div>
            <span>appliquées</span>
        </div>
    </div>
}
