import React from 'react';

import styles from './Situation.module.css';

export default function Situation(props) {
    return <p className={ styles.situation }>
        { props.children }
    </p>;
}