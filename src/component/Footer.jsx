import React from 'react';

import logoECite from '../resources/e-cite.svg';
import logoLaCite from '../resources/la-cite.png';
import styles from './Footer.module.css';

export default function Footer() {
    return <footer>
        <div className={ styles.links }>
            <a href="https://www.collegelacite.ca/" target="_blank" rel="noopener noreferrer"><img src={ logoLaCite } alt="Logo La Cité" /></a>
            <a href="https://ecite.lacitec.on.ca/" target="_blank" rel="noopener noreferrer"><img src={ logoECite } alt="Logo eCité" /></a>
        </div>
        <p className={ styles.copyright }>© Collège La Cité</p>
        <p className={ styles.credits }>Développé par Jonathan Wilkie | Design par Catherine Wilkie</p>
        <p className={ styles.credits }>Marco Lavoie et Khaled Saidani pour une partie des notes et laboratoires</p>
    </footer>
}
